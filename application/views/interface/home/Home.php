<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="WE SERVE AND PROTECT">
    <meta name="author" content="PNPR6">
    
    <title><?= $page_title ?> | <?= $system_title ?></title>

    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    
    <?php $this->load->view('interface/home/includes/Css'); ?>
    <style type="text/css">
        .navy-line{
            border-bottom-color: rgb(150,26,30) !important;
        }       
    </style>

    <script src="<?= base_url() ?>assets/js/jquery-3.1.1.min.js"></script>

</head>
<body id="page-top" class="landing-page no-skin-config">

   <?php $this->load->view('interface/home/includes/Header'); ?>

    <?php $this->load->view("interface/home/includes/Carousel"); ?>

    <?php $this->load->view("interface/home/includes/Navbar"); ?>    

    <div class="modal inmodal" id="myModal1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-line-chart modal-icon"></i>
                    <h4 class="modal-title">FLOW CHART OF PROCCESSING OF CLAIMS AND VOUCHER</h4>
                </div>
                <div class="modal-body">
                    <object width="100%" height="500px" data="<?= base_url() ?>assets/custom/doc/FLOW_CHART_2.pdf"></object>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="myModal2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-line-chart modal-icon"></i>
                    <h4 class="modal-title">Flow of PS Claims</h4>
                </div>
                <div class="modal-body">
                    <object width="100%" height="500px" data="<?= base_url() ?>assets/custom/doc/FLOW_OF_CLAIMS.pdf"></object>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="myModal3" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-line-chart modal-icon"></i>
                    <h4 class="modal-title">ORGANIZATIONAL CHART</h4>
                </div>
                <div class="modal-body">
                    <img src="<?= base_url() ?>assets/custom/images/3x4ft.jpg" style="width: 100%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="myModal4" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-eye modal-icon"></i>
                    <h4 class="modal-title">RCD CHARTER STATEMENT</h4>
                </div>
                <div class="modal-body">
                    <img src="<?= base_url() ?>assets/custom/images/3x4ftx.jpg" style="width: 100%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <section id="logos" class="container services" style="margin-top: 0px; padding-top: 60px; padding-bottom: 60px;">
       <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1 style="font-weight: 400;">LOGOS</h1>
                <!-- <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member wow fadeInLeft">
                    <img src="<?= base_url() ?>assets/custom/images/pnp_finance_service.jpeg" class="img-responsive img-circle" alt="" style="width: 150px;">
                    <h4><span class="navy">RFSO 13</span></h4>
                </div>
            </div>
            <div class="col-sm-4 wow zoomIn">
                <div class="team-member">
                   <img src="<?= base_url() ?>assets/custom/images/pnp_logo.png" alt="dashboard" class="img-responsive" style="margin: 0 auto; width: 150px;">
                    <h4><span class="navy">PHILIPPINE NATIONAL POLICE</span></h4>
                </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
                <div class="team-member">
                    <img src="<?= base_url() ?>assets/custom/images/itms.png" class="img-responsive img-circle" alt="" style="width: 150px;">
                    <h4><span class="navy">ITMS</span></h4>
                </div>
            </div>
        </div>
    </section>
    
    <!-- backgroud rgb(31,26,83) -->
    <section id="abouts" class="gray-section features" style="padding-top: 60px; padding-bottom: 60px; background: #0F2A55; color: #fff;">
        <div class="container">
            <div class="row m-b-lg">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1 style="color: #fff !important; font-weight: 400;">ABOUT<br/> <!-- <span class="navy"> with many custom components</span> --> </h1>
                    <p>KNOW US MORE. </p>
                </div>
            </div>
            <div class="row m-b-lg">
                <div class="col-md-3 text-center wow fadeInLeft">
                    <div>
                        <i class="fa fa-bullseye features-icon"></i>
                        <h2 style="color: #fff !important; font-weight: 400;">RCD MISSION</h2>
                        <p class="text-justify" style="text-indent: 50px; color:#fff;">The Regional Comptrollership Division (RCD) is principally concerned with the management of the financial resources of the Police Regional Office (PRO). It involved planning, objective setting, program costing and integration, budget harmonization, financial accounting, cost effectiveness evaluation, performance analysis and resource management improvement.</p>
                    </div>
                    <div class="m-t-lg">
                        <i class="fa fa-line-chart features-icon"></i>
                        <h2 style="color: #fff !important; font-weight: 400;">ORGANIZATIONAL CHART</h2>
                        <img src="<?= base_url() ?>assets/custom/images/3x4ft.jpg" style="width: 270px; cursor: pointer;" data-toggle="modal" data-target="#myModal3">
                        <span class="text-danger"><strong><i>Note: Click the image to show in large.</i></strong></span>
                    </div>
                </div>
                <div class="col-md-6 text-center wow zoomIn" style="padding-top: 50px;">

                    <img src="<?= base_url() ?>assets/custom/images/police_regional_office_13.png" class="img-responsive img-circle" style="margin: 0 auto;" alt="">

                    <div class="hr-line-dashed"></div>
                    <h2 style="color: #fff !important; font-weight: 400;">
                        FLOWCHART LIST
                    </h2>
                    <div class="hr-line-dashed"></div>

                    <a href="#myModal1" class="text-muted" data-toggle="modal" id="flow_chart_2"><h4>FLOW CHART OF PROCCESSING OF CLAIMS AND VOUCHER</h4></a>
                    <a href="#myModal2" class="text-muted" data-toggle="modal" id="flow_of_claim"><h4>FLOW OF PS CLAIMS</h4></a>

                    <div class="hr-line-dashed"></div>

                    <a href="<?= base_url() ?>login" class="btn btn-primary">View Claims</a>

                </div>
                <div class="col-md-3 text-center wow fadeInRight">
                    <div>
                        <i class="fa fa-eye features-icon"></i>
                        <h2 style="color: #fff !important; font-weight: 400;">RCD CHARTER STATEMENT</h2>
                        <img src="<?= base_url() ?>assets/custom/images/3x4ftx.jpg" style="width: 270px; cursor: pointer;" data-toggle="modal" data-target="#myModal4">
                        <span class="text-danger"><strong><i>Note: Click the image to show in large.</i></strong></span>
                    </div>
                    <div class="m-t-lg">
                        <i class="fa fa-cogs features-icon"></i>
                        <h2 style="color: #fff !important; font-weight: 400;">RCD FUNCTIONS</h2>
                        <ol style="height: 200px; overflow-y: auto;">
                            <li class="text-justify" style="padding: 0px 7px;">
                                Responsible for the preparation of the annual budget proposal of PRO, quarterly and annual Program Review and Analysis and the costing of the Operations Plan and Budget;
                            </li>
                            <li class="text-justify" style="padding: 0px 7px;">
                                Initiates project in the furtherance of management improvement programs of the PRO;
                            </li>
                            <li class="text-justify" style="padding: 0px 7px;">
                                Plans and supervises the implementation of policies and procedures pertaining to auditing and statistical reporting in management activities;
                            </li>
                            <li class="text-justify" style="padding: 0px 7px;">
                                Plans, implements and manages all policies and procedures concerning financial management; and
                            </li>
                            <li class="text-justify" style="padding: 0px 7px;">
                                Performs other official duties or functions as directed by the Regional Director and/or as higher authorities may direct from time to time.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="container contact" style="padding-bottom: 60px;">
        <div class="container">
            <div class="row m-b-lg">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1 style="font-weight: 400;">Contact Us</h1>
                    <!-- <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p> -->
                </div>
            </div>
            <div class="row m-b-lg">
                 <!-- col-lg-offset-3 -->
                <div class="col-lg-12 text-center">
                    <address>
                        <strong><span class="navy">PNP RCD PRO 13</span></strong><br/>
                        CAMP RAFAEL C. RODRIGUEZ<br/>
                        LIBERTAD BUTUAN CITY, 8600, PHILIPPINES<br/>
                        <abbr title="R6 Email hotline">Email: </abbr> rcdpro13@yahoo.com.ph
                        <br>
                        <abbr title="R6 hotline #">P:</abbr> 09074175843
                    </address>
                </div>
                <!-- <div class="col-lg-4">
                    <p class="text-color">
                        Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                    </p>
                </div> -->
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <a href="mailto:cdpro13@yahoo.com.ph" class="btn btn-primary">Send us mail</a>
                    <p class="m-t-sm">
                        Or follow us on social platform
                    </p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="team" class="gray-section team" style="margin-top: 0px;"> 
        <div class="container" style="padding-bottom: 60px;">
            <div class="row m-b-lg">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1 style="font-weight: 400;">Our Team</h1>
                    <!-- <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="team-member wow zoomIn">
                        <img src="<?= base_url() ?>assets/custom/images/sir_castro2.jpg" class="img-responsive img-circle" width="128px" alt="">
                        <h4><span class="navy">Eltimar Timoteo Castro Jr,</span> MIT</h4>
                        <p>Project Manager, Team Leader, Consultant, Programmer</p>
                        <ul class="list-inline social-icon">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 hr-line-dashed"></div>
                <div class="col-sm-4 wow fadeInLeft">
                    <div class="team-member">
                        <img src="<?= base_url() ?>assets/custom/images/edward3.jpg" class="img-responsive img-circle img-small" alt="">
                        <h4><span class="navy">Jean Edward</span> Goloran</h4>
                        <p>Graphic Artist</p>
                        <ul class="list-inline social-icon">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member wow zoomIn">
                        <img src="<?= base_url() ?>assets/custom/images/20180228103837_1.jpg" class="img-responsive img-circle" width="128px" alt="">
                        <h4><span class="navy">Joshua</span> Saubon</h4>
                        <p>Lead Programmer</p>
                        <ul class="list-inline social-icon">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInRight">
                    <div class="team-member">
                        <img src="<?= base_url() ?>assets/custom/images/klaven.jpg" class="img-responsive img-circle img-small" width="128px" alt="">
                        <h4><span class="navy">Klaven Rey</span> Matugas</h4>
                        <p>Programmer</p>
                        <ul class="list-inline social-icon">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 hr-line-dashed"></div>
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; <?= date("Y") ?> RCD PRO 13</strong><br/> WE SERVE AND PROTECT.</p>
            </div>
        </div>
    </section>
    
    <?php $this->load->view("interface/home/includes/Js"); ?>
    
</body>
</html>