 <!-- Bootstrap core CSS -->
<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

<!-- Animation CSS -->
<link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

<style type="text/css">
	
	::-webkit-scrollbar {
        width: 8px;
        height: 8px;
        background-color: #F5F5F5;
    }
	::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
	    border-radius: 10px;
    }
    ::-webkit-scrollbar-thumb {
    	-webkit-border-radius: 10px;
	    border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

    /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) {  } 

    /* Small devices (portrait tablets and large phones, 600px and up) */
    @media only screen and (min-width: 600px) {  } 

    /* Medium devices (landscape tablets, 768px and up) */
    @media only screen and (min-width: 768px) {  } 

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {  } 

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {  }

</style>