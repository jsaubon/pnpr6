<!-- footer -->
<footer id="footer">
	<div class="wrap">
		
		<a href="index.html"><strong>GIOSS</strong></a>
		
		<div class="col">
			<ul class="social">
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube"></i></a></li>
				<li><a href="#"><i class="fa fa-skype"></i></a></li>
			</ul>
		</div>
		
	</div>
	<a href="#" id="go-top"></a>
</footer>