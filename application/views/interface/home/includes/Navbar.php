<style type="text/css">
    
    .landing-page .navbar-default {
        background: #f3f3f4;
        border-bottom-color: #2f4050;
    }

    .landing-page .navbar.navbar-scroll .navbar-brand {
        padding: 5px;
    }

    @media (min-width: 768px) {
        .navbar-nav {
            float: none;
            text-align: center;
        }
        .navbar-nav>li {
            float: none; 
            /*text-align: center !important; */
            display: inline-block;
        }
    }
    .navbar-nav>li>a:hover {
        color:rgb(150,26,30) !important;
    }

    .landing-page .navbar-default .nav li a {
        color: #676a6c;
    }

    .affix {
        top: 0;
        width: 100%;
        z-index: 200 !important;
    }

    .landing-page .navbar-default.navbar-scroll {
        padding: 0px;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    }
    .landing-page .navbar-default .navbar-nav > .active > a, 
    .landing-page .navbar-default .navbar-nav > .active > a:hover{
        border-top-color: rgb(150,26,30) !important;
    }

    .landing-page .navbar-default .navbar-nav > .active > a, 
    .landing-page .navbar-default .navbar-nav > .active > a:hover,
    .landing-page .navbar-default .navbar-nav > .active > a:focus {
        color: rgb(150,26,30) !important;
    }
    .landing-page .navbar-default .nav li a {
        padding-bottom: 25px;
    }



    .landing-page .navbar-default .navbar-brand{
        padding: 0px;
        background: transparent;
    }

    .landing-page .navbar.navbar-scroll .navbar-brand {
        color: #fff;
        height: auto;
        display: block;
        font-size: 12px;
        background: transparent;
        padding: 0;
        border-radius: 0 0 5px 5px;
        font-weight: 700;
        transition: all 0.3s ease-in-out 0s;
        margin-top: 0px;
    }

    .landing-page .navbar.navbar-scroll .navbar-brand img {
        width: 100px !important;
        position: absolute;
        top: -16px;
    }

</style>
<nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="100"> 
    <div class="container">
        <!-- <div class="navbar-header page-scroll hidden">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="< ?= base_url() ?>"><img src="<?= base_url() ?>assets/home/img/logo-final.png" style="width: 50px;">sadasd</a>
        </div> -->
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="home active"><a class="page-scroll" href="#page-top">Home</a></li>
                <li class="about"><a class="page-scroll" href="#abouts">ABOUT</a></li>
                <li><a class="page-scroll" href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>