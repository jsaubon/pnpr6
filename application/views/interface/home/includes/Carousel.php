<style type="text/css">

    .landing-page .carousel, 
    .landing-page .carousel .item {
        height: 650px;
    }

    .landing-page .carousel .item .carousel-caption {
        background: rgba(150,26,30, 0.5) !important;
        padding-left: 25px;
        padding-right: 25px;
        top: 375px;
    }

    .landing-page .carousel-caption.blank {
        top: 375px;
    }

    .landing-page .carousel .item p.p_learn_more {
        margin-top: 50px;
        margin-bottom: 0px;
    }
    
    .landing-page .header-back.one {
        background: url(<?= base_url() ?>assets/custom/images/DSC_0079.JPG);
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        height: 650px;
    }
    .landing-page .header-back.two {
        background: url(<?= base_url() ?>assets/custom/images/DSC_0008%20%282%29.JPG); 
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        height: 650px;
    }
    .landing-page .header-back.three {
        background: url(<?= base_url() ?>assets/custom/images/DSC_0027.JPG); 
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        height: 650px;
    }
    .landing-page .header-back.four {
        background: url(<?= base_url() ?>assets/custom/images/DSC_0036.JPG);
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover; 
        height: 650px;
    }

     /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) { 
        .landing-page .carousel, 
        .landing-page .carousel .item {
            height: 325px;
        }
        .landing-page .carousel .item .carousel-caption {
            top: 187px;
        }
        .landing-page .carousel .item .carousel-caption h1 {
            font-size: 14px;
        }
        .landing-page .header-back.one,
        .landing-page .header-back.two,
        .landing-page .header-back.three,
        .landing-page .header-back.four {
            height: 325px;
        }
        .landing-page .carousel-caption.blank {
            top: 187px;
        }
    } 

    /* Small devices (portrait tablets and large phones, 600px and up) */
    @media only screen and (min-width: 600px) {  } 

    /* Medium devices (landscape tablets, 768px and up) */
    @media only screen and (min-width: 768px) {  } 

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {  } 

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {  }

</style>

<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
        <li data-target="#inSlider" data-slide-to="2"></li>
        <li data-target="#inSlider" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>
                        CAMP RAFAEL C. RODRIGUEZ
                        <br>
                        <small style="color: #fff;">LIBERTAD BUTUAN CITY</small>
                    </h1>
                    <!-- <p>Lorem Ipsum is simply dummy text of the printing.</p> -->
                    <p class="p_learn_more">
                        <!-- <a class="btn btn-lg btn-primary" href="#" role="button">READ MORE</a> -->
                        <!-- <a class="caption-link" href="#" role="button">Inspinia Theme</a> -->
                    </p>
                </div>
                <div class="carousel-image wow zoomIn">
                    <!-- <img src="< ?= base_url() ?>assets/img/landing/laptop.png" alt="laptop"/> -->
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>

        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>
                        CAMP RAFAEL C. RODRIGUEZ
                        <br>
                        <small style="color: #fff;">LIBERTAD BUTUAN CITY</small>
                    </h1>
                    <!-- <p class="p_learn_more"><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>
                        CAMP RAFAEL C. RODRIGUEZ
                        <br>
                        <small style="color: #fff;">LIBERTAD BUTUAN CITY</small>
                    </h1>
                    <!-- <p class="p_learn_more"><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back three"></div>
        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                   <h1>
                        CAMP RAFAEL C. RODRIGUEZ
                        <br>
                        <small style="color: #fff;">LIBERTAD BUTUAN CITY</small>
                    </h1>
                    <!-- <p class="p_learn_more"><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back four"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>