<style type="text/css">

	header {
        height: 50px;
        background: #0F2A55; /* rgb(31,26,83) */
        color:#fff;
    }
    header ul {
    	margin: 0 auto;
    	text-align: center;
    }
    header ul li {
        display: inline-block;
    }
    header ul li h2 {
        margin: 0px; 
        padding: 0px; 
        line-height: 50px;
    }
    header ul li:nth-child(2){
    	width: 120px;
    }
    header ul li img {
        width: 100px;
        position: absolute;
        top: 0px;
        left: 0;
        right: 0;
        z-index: 999999;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

     /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) { 
        header {
            height: 40px;
        }
        header ul li h2 {
            font-size: 12px;
            line-height: 40px;
        }
        header ul li img {
            width: 80px;
        }
        header ul li:nth-child(1) h2{
            padding-right: 20px;
        }
        header ul li:nth-child(2){
            width: 100px;
        }
        header ul li:nth-child(3) h2{
            text-align: left !important;
        }
    } 

    /* Small devices (portrait tablets and large phones, 600px and up) */
    @media only screen and (min-width: 600px) {  } 

    /* Medium devices (landscape tablets, 768px and up) */
    @media only screen and (min-width: 768px) {  } 

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {  } 

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {  }

</style>

<header class="row">
	<ul>
		<li class="text-right"><h2 class="font-bold text-right">WE SERVE</h2></li>
		<li class="text-center"><img src="<?= base_url() ?>assets/custom/images/pnp_pro13.png" alt="PNP logo" class=""></li>
		<li class="text-left"><h2 class="font-bold text-left">AND PROTECT</h2></li>
	</ul>
</header>