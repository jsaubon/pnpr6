<!-- orris -->
<link href="<?= base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

<style type="text/css">
        
    #ibox_dashboard > .ibox-content {
        min-height: 100px;
    }

    #modal_memo_and_update_preview .modal-body {
        min-height: 500px;
    }

</style>

<div class="row">
    <div class="col-sm-6">
        <div class="ibox float-e-margins" id="ibox_dashboard1">
            <div class="ibox-title">
                <h5>Memo and Updates <small></small></h5>
                <div class="ibox-tools"></div>
            </div>
            <div class="ibox-content sk-loading" style="min-height: 200px; max-height: 543px; overflow-y: auto;">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <div id="div_memo_and_updates" class="row"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="ibox float-e-margins" id="ibox_dashboard2">
            <div class="ibox-title">
                <h5>Claimed <small>Count</small></h5>
                <div class="ibox-tools"></div>
            </div>
            <div class="ibox-content sk-loading" style="min-height: 200px;">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <div id="morris-donut-chart"></div>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="modal_memo_and_update_preview" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-eye modal-icon"></i>
                <h4 class="modal-title">Memo and Update</h4>
                <small class="font-bold">Preview</small>
            </div>
            <div class="modal-body no-padding"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Morris -->
<script src="<?= base_url() ?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="<?= base_url() ?>assets/js/plugins/morris/morris.js"></script>


<script type="text/javascript">

    var tbl_memo_and_update, tbl_memo_and_update_data;
    
    $(function(){
        get_memo_and_updates();
        get_claim_counts();
    });

    function get_memo_and_updates(){
        $("#div_memo_and_updates").empty();
        $.post("<?= base_url('get_memo_and_updates') ?>",
            function(data){
                var d = JSON.parse(data);
                var count = 0;

                for (var i = 0; i < d.length; i++) {
                    count = d.length[i];
                }

                if (count != 0) {
                    $.each(d, function(k, v) {
                        $("#div_memo_and_updates").append(v.data_ibox);
                    });                    
                } else {
                    $("#div_memo_and_updates").append('<h1 class="text-center" style="line-height: 180px;">No memo or updates.</h1>');
                }

                $("#ibox_dashboard1").children(".ibox-content").toggleClass("sk-loading");
            }
        );
    }

    function get_memo_and_update_preview(value) {
        $("#modal_memo_and_update_preview .modal-body").empty();
        $.post("<?= base_url('dashboard/get_memo_and_update_preview') ?>",
            { value },
            function(data) {
                var d = JSON.parse(data);
                $("#modal_memo_and_update_preview .modal-body").html(d);
            }
        );
    }
    
    function get_claim_counts(){
        $.post("<?= base_url('dashboard/get_claim_counts') ?>",
            function(data) {
                var d = JSON.parse(data);
                if (d.count_cdp != 0 || d.count_cip != 0 || d.count_other != 0) {
                    Morris.Donut({
                        element: 'morris-donut-chart',
                        data: [{ label: "CDP Claimed", value: d.count_cdp },
                            { label: "CIP Claimed", value: d.count_cip },
                            { label: "Other Claimed", value: d.count_other } ],
                        resize: true,
                        colors: ['#87d6c6', '#54cdb4','#1ab394'],
                    });
                } else {
                    $("#ibox_dashboard2 .ibox-content").html('<h1 class="text-center" style="line-height: 180px;">NO CLAIMED</h1>');
                }
                $("#ibox_dashboard2").children(".ibox-content").toggleClass("sk-loading");
            }
        );
    }
    
</script>