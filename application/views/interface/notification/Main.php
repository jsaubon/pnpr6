<!-- <script type="text/javascript" src="< ?= base_url() ?>assets/Styles-admin/js/jquery-2.1.1.js"></script> -->

<script type="text/javascript">

    $(function() {
        setInterval(get_notifications, 45000);
        $("#notifications").prop("style", "overflow-y: auto; height: auto; width: 300px; padding: 0px;");
        get_notifications();
    });

    function show_notifications() {
        $.post("<?= base_url('notification/Notification/set_as_read') ?>", {}, function(data) { get_notifications(); } );
    }
    
    const rollSound = new Audio("<?php echo base_url() ?>/assets/custom/sound/CHIME1.WAV");
    function get_notifications() {
        $.post("<?= base_url('notification/Notification/get_notifications') ?>",
                {},
                function(data) {
                    var d = JSON.parse(data);
                    var notifs = '<li>\
                                    <div class="text-center link-block">\
                                        <a href="javascript:;">\
                                            <i class="fa fa-bell"></i> <strong>Notifications</strong>\
                                        </a>\
                                    </div>\
                                </li>\
                                <li class="divider" style="margin:0px;"></li>';
                    var count = 0;
                    var count_notif = '<span class="label label-warning">'+d.count_notif+'</span>';
                    if(d.count_notif <= 0) {
                        count_notif = "";
                    } else {
                        rollSound.play();
                    }

                    for(var x=0; x < d.notifs.length; x++) {
                        notifs += '<li style="padding:5px 5px 5px 5px; min-height:30px;">\
                                        <a href="#" style="padding-left:5px;padding-right:5px !important;">\
                                            <div>\
                                                <div class="pull-right text-muted small" style="word-wrap: break-word; width:60px !important;">'+d.notifs[x].date+'</div>\
                                                <div style="word-wrap: break-word; font-size: 11px; width:200px !important;">'+d.notifs[x].notif+'</div>\
                                            </div>\
                                        </a>\
                                    </li>';
                        if((x+1) != d.notifs.length) {
                            notifs += '<li class="divider" style="margin:0px;"></li>';
                        }
                        count++;
                    }

                    if(count <= 0) {
                        notifs = '<li>\
                                    <div class="text-center link-block">\
                                        <a href="javascript:;">\
                                            <i class="fa fa-bell"></i> <strong>Notifications</strong>\
                                        </a>\
                                    </div>\
                                </li>\
                                <li class="divider" style="margin:0px;"></li>\
                                <li>\
                                    <div class="text-center link-block">\
                                        <a href="javascript:;">\
                                            No Notifications\
                                        </a>\
                                    </div>\
                                </li>';
                        $("#notifications").prop("style", "overflow-y: auto; height: auto; width: 300px; padding: 0px;");
                    } else {
                        $("#notifications").prop("style", "overflow-y: auto; height: auto; max-height: 250px; width: 300px; padding: 0px;");
                    }

                    $("#notifications").html(notifs);
                    $("#count_notif").html(count_notif);
                }
            );
    }
</script>