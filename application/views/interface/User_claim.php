<div class="row">
    <div class="col-sm-4">
        <div class="ibox float-e-margins" id="ibox_other_claim">
            <div class="ibox-title">
                <h5>Other Claims <small>List</small></h5>
                <div class="ibox-tools">
                    <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
                </div>
            </div>
            <div class="ibox-content sk-loading table-responsive">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <table class="table" id="tbl_other_claim" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Type of Claim</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="ibox float-e-margins" id="ibox_cdp_claim">
            <div class="ibox-title">
                <h5>CDP Claims <small>List</small></h5>
                <div class="ibox-tools">
                    <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
                </div>
            </div>
            <div class="ibox-content sk-loading table-responsive">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <table class="table" id="tbl_cdp_claim" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Type of Claim</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="ibox float-e-margins" id="ibox_cip_claim">
            <div class="ibox-title">
                <h5>CIP Claims <small>List</small></h5>
                <div class="ibox-tools">
                    <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
                </div>
            </div>
            <div class="ibox-content sk-loading table-responsive">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <table class="table" id="tbl_cip_claim" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Type of Claim</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var tbl_other_claim, tbl_other_claim_data;
    var tbl_cdp_claim, tbl_cdp_claim_data;
    var tbl_cip_claim, tbl_cip_claim_data;

    $(function(){

        tbl_other_claim = $("#tbl_other_claim").DataTable({
            ajax: {
                url: "<?= base_url('get_user_other_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_other_claim_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        tbl_cdp_claim = $("#tbl_cdp_claim").DataTable({
            ajax: {
                url: "<?= base_url('get_user_cdp_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_cdp_claim_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        tbl_cip_claim = $("#tbl_cip_claim").DataTable({
            ajax: {
                url: "<?= base_url('get_user_cip_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_cip_claim_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

    });

</script>