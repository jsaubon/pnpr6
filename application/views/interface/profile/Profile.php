<div class="row">
    <div class="col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Profile Detail</h5>
                <?php 
                    $old_password = "admin";
                    echo $this->session->login_id;
                 ?>
            </div>
            <div>
                <div class="ibox-content no-padding border-left-right profile_image">
                    <img alt="image" class="img-responsive" src="img/profile_big.jpg">
                </div>
                <div class="ibox-content profile-content">
                    <h4><strong class="fullname"></strong></h4>
                    <p class="rank"></p>
                    <p class="unit"></p>
                    <button class="btn btn-sm btn-white btn-block m-t-lg" onclick="$('#basic_profile').slideToggle()";><i class="fa fa-edit"></i> Edit</button>
                    <div class="panel panel-info m-t-lg" id="basic_profile" style="display: none;">
                        <?= form_open(base_url('profile/update_profile'), 'id="form_basic_profile"'); ?>
                            <div class="panel-body" style="padding-bottom: 5px;">
                                <div class="form-group">
                                    <input type="hidden" name="alert" value="">
                                    <input type="password" class="form-control" name="old_password" placeholder="Enter old password"></div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="username"></div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="Enter new password"></div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="confirm_password" placeholder="Confirm password"></div>
                            </div>
                            <div class="panel-footer" style="padding-bottom: 5px;">
                                <button class="btn btn-sm btn-primary btn-block no-margin" id="profile_credential_btnSave">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    
    $(function(){

        var profile_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#profile_credential_btnSave").attr("disabled", true);
                $("#profile_credential_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    swal({
                        html:   true,
                        title:  "Success",
                        text:   d.msg,
                        type:   "success"
                    });
                    $("#profile_credential_btnSave").attr("disabled", false);
                    $("#profile_credential_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    get_info_profile();
                } else if (d.success == false && d.alert == 'ok') {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, update it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_basic_profile [name=alert]").val("ok");
                            $("#form_basic_profile").submit();
                        } else {
                            swal("Cancelled", "Your credential is not updated :)", "error");
                            $("#profile_credential_btnSave").attr("disabled", false);
                            $("#profile_credential_btnSave").html("Save");
                        }
                    });
                } else {
                    $("#profile_credential_btnSave").attr("disabled", false);
                    $("#profile_credential_btnSave").html("<span class=\"fa fa-repeat\"></span> Try Again");
                    swal({
                        html:   true,
                        title:  "Warning",
                        text:   d.msg,
                        type:   "error"
                    });
                }
            }
        };
        $("#form_basic_profile").ajaxForm(profile_form_options);
        
        get_info_profile();

    });

    function get_info_profile(){
        $.post("<?= base_url('profile/get_info_profile') ?>",
            function(data){
                var d = JSON.parse(data);
                $('.profile_image').html('<img src="'+( d.data_user_image_single.image != null ? d.data_user_image_single.image:'<?= base_url('assets/custom/images/default.png') ?>' )+'" class="img-responsive block" alt="profile" style="margin: 0 auto;">');
                $('.fullname').html(( d.data_user.firstname != null ? d.data_user.firstname:'' )+' '+( d.data_user.middlename != null ? d.data_user.middlename:'' )+' '+( d.data_user.lastname != null ? d.data_user.lastname:'' )+' '+( d.data_user.name_ext != null ? d.data_user.name_ext:'' ));
                $('.rank').html('<i class="fa fa-star-o"></i> '+( d.data_user.rank_name != null ? d.data_user.rank_name:'No Rank' ));
                $('.unit').html('<i class="fa fa-map-marker"></i> '+( d.data_user.unit != null ? d.data_user.unit:'No Unit' ));
                $('[name=username]').val(d.data_user.username);
            }
        ).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

</script>