<div class="ibox float-e-margins" id="ibox_log">
    <div class="ibox-title">
        <h5>Log <small>List</small></h5>
        <div class="ibox-tools">
            <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
        </div>
    </div>
    <div class="ibox-content sk-loading table-responsive">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <table class="table" id="tbl_log" style="width: 100%;">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Action</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    var tbl_log, tbl_log_data;
    
    $(function(){

        $("#log").addClass("logActive");

        tbl_log = $("#tbl_log").DataTable({
            ajax: {
                url: "<?= base_url('log/get_logs') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_log").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_log_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

    });
</script>