<div class="col-sm-12">
	<div class="ibox float-e-margins" id="ibox_cdpcip_claim">
		<div class="ibox-title">
            <h5>Claim <small>List</small></h5>
            <div class="ibox-tools">
                <button name="btn_advanced_filter" class="btn btn-xs btn-white" onclick="$('#form_advanced_filter').slideToggle()" style="margin-right:7px;"><span class="fa fa-filter"></span> <strong>Advanced Filter</strong></button>

                <button name="btn_add" href="#modal_credited_cdpcip_claim" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Spreadsheet Claim <span class="text-danger"><strong>( Credited to ATM )</strong></span></button>

                <button onclick="clear_modal('form_single_cdpcip_claim')" name="btn_add" href="#modal_single_cdpcip_claim" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Claim</button>

                <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-content table-responsive sk-loading">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form role="form" id="form_advanced_filter" style="display: none;">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="af_user_id">Fullname</label>
                                            <select name="af_user_id" id="af_user_id" class="form-control af_filter" style="width: 100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="af_status">Status</label>
                                            <select name="af_status" id="af_status" class="form-control af_filter">
                                                <option value="0">Request for FUND</option>
                                                <option value="1">Credited to ATM</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="af_date_paid">Date Paid</label>
                                            <input type="date" name="af_date_paid" class="form-control af_filter" id="af_date_paid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12" id="dv_claim_temp">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa fa-recycle text-danger"></i> Claims Temporary 
                                <span class="text-danger"><strong>Note: </strong><i>This temporary claim are not exist from user</i></span></h4>
                        </div>
                        <div class="panel-body">
                            <table class="table" id="tbl_claim_temp">
                                <thead>
                                    <th>Fullname</th>
                                    <th>Ranked</th>
                                    <th>Date Paid</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </thead>
                                <tbody style="max-height: 300px; overflow-y: auto;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <table class="table" id="tbl_claim" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Claim Type</th>
                                <th>Date Paid</th>
                                <th>Status</th>
                                <th><div class='text-center'>Action</div></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="modal inmodal" id="modal_single_cdpcip_claim" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_single_cdpcip_claim'), 'id="form_single_cdpcip_claim"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-check modal-icon"></i>
                    <h4 class="modal-title">
                    	Claim <small class="font-bold">Form</small>
						<!-- <br> -->
						<!-- <small class="font-bold">Single Add</small> -->
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_id">Fullname <span class="text-danger">*</span></label>
                        <input type="hidden" name="claim_id">
                        <input type="hidden" name="alert" value="">
                        <select name="user_id" class="form-control select2" id="user_id" required style="width: 100%;">
                    		<option></option>
                    	</select>
                    </div>    
                    <div class="form-group">
                        <label for="date_paid">Date Paid <span class="text-danger">*</span></label>
                        <input type="datetime-local" name="date_paid" class="form-control" id="date_paid" step="8">
                    </div>    
                    <div class="form-group">
                        <label for="amount">Amount <span class="text-danger">*</span></label>
                        <input type="number" name="amount" class="form-control" id="amount" step="any" min="0">
                    </div>    
                    <div class="form-group">
                        <label for="status">Status <span class="text-danger">*</span></label>
                        <select name="status" class="form-control" id="status" required>  
                    		<option value="0">Request for FUND</option>
                    		<option value="1">Credited to ATM</option>
                    	</select>
                    </div>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="single_cdpcip_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_credited_cdpcip_claim" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_credited_cdpcip_claim'), 'id="form_credited_cdpcip_claim"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Credited to ATM</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-upload"></i> Spreadsheet</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" required />
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="credited_cdpcip_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_claim_temp" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_claim_temp_to_user_and_claim'), 'id="form_claim_temp"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-user modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Temp</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-user-o"></i> Claim not exist from user</small>
                        <br>
                    </h4>
                    <p class="text-danger"><i><strong>Note:</strong> Click save if the information below is correct!</i></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="form-group">
                        <label for="firstname">Firstname</label>
                        <input type="hidden" name="claim_temp_id">
                        <input type="hidden" name="claim_code">
                        <input type="hidden" name="date_requested">
                        <input type="hidden" name="date_approved">
                        <input type="text" name="firstname" class="form-control" id="firstname"></div>
                    <div class="form-group">
                        <label for="middlename">Middlename</label>
                        <input type="text" name="middlename" class="form-control" id="middlename"></div>
                    <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input type="text" name="lastname" class="form-control" id="lastname"></div>
                    <div class="form-group">
                        <label for="name_ext">Name Extension</label>
                        <input type="text" name="name_ext" class="form-control" id="name_ext"></div>
                    <div class="form-group">
                        <label for="rank_name">Ranked</label>
                        <input type="text" name="rank_name" class="form-control" id="rank_name" readonly></div>
                    <div class="form-group">
                        <label for="date_paid">Date Paid</label>
                        <input type="text" name="date_paid" class="form-control" id="date_paid"></div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="text" name="amount" class="form-control" id="amount"></div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                            <option value="0">Request for FUND</option>
                            <option value="1">Credited to ATM</option>
                        </select></div>            
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="claim_temp_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
	
	var tbl_claim, tbl_claim_data;

	$(function(){

        $("[name=af_user_id]").select2({
             placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
        });

		$("[name=user_id]").select2({
			 placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
		});

        var single_cdpcip_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#single_cdpcip_claim_btnSave").attr("disabled", true);
                $("#single_cdpcip_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cdpcip_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#single_cdpcip_claim_btnSave").attr("disabled", false);
                    $("#single_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Inserted!", "Your data has been save.", "success");
                            $("#form_single_cdpcip_claim [name=alert]").val("ok");
                            $("#form_single_cdpcip_claim").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#single_cdpcip_claim_btnSave").attr("disabled", false);
                            $("#single_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#single_cdpcip_claim_btnSave").attr("disabled", false);
                    $("#single_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_single_cdpcip_claim").ajaxForm(single_cdpcip_claim_form_options);

        var credited_cdpcip_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#credited_cdpcip_claim_btnSave").attr("disabled", true);
                $("#credited_cdpcip_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cdpcip_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#credited_cdpcip_claim_btnSave").attr("disabled", false);
                    $("#credited_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    get_claim_temp_cdpcip_claims();
                    tbl_claim.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Inserted!", "Your data has been save.", "success");
                            $("#form_credited_cdpcip_claim [name=alert]").val("ok");
                            $("#form_credited_cdpcip_claim").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#credited_cdpcip_claim_btnSave").attr("disabled", false);
                            $("#credited_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#credited_cdpcip_claim_btnSave").attr("disabled", false);
                    $("#credited_cdpcip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_credited_cdpcip_claim").ajaxForm(credited_cdpcip_claim_form_options);

        var claim_temp_cdpcip_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#claim_temp_btnSave").attr("disabled", true);
                $("#claim_temp_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cdpcip_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#claim_temp_btnSave").attr("disabled", false);
                    $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    get_claim_temp_cdpcip_claims();
                    tbl_claim.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Inserted!", "Your data has been save.", "success");
                            $("#form_claim_temp [name=alert]").val("ok");
                            $("#form_claim_temp").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#claim_temp_btnSave").attr("disabled", false);
                            $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#claim_temp_btnSave").attr("disabled", false);
                    $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_claim_temp").ajaxForm(claim_temp_cdpcip_claim_form_options);

        $(".af_filter").on("change keyup", function() {
            tbl_claim_data = $("#form_advanced_filter").serializeArray();
            $("#ibox_cdpcip_claim").children(".ibox-content").toggleClass("sk-loading");
            tbl_claim.ajax.reload(function() {
                check_user_permission();
            });
        });
		tbl_claim = $("#tbl_claim").DataTable({
            pageLength: 100,
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ "10", "25", "50", "100", "ALL" ]
            ],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    },
                },
                {extend: 'csv',
                    title: 'Survey Report',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    },
                },
                {extend: 'excel', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2 ]
                        },
                },
                {extend: 'pdf', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2 ]
                        },
                },
                {extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    },
                    title: '<div class="text-center"><img alt="image" style="width: 100px;" src="<?= base_url() ?>assets/custom/images/Philippine_National_Police_seal.svg.png"/><br>PNPR6 Report</div>', // need to change this
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            ajax: {
                url: "<?= base_url('get_cdpcip_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_cdpcip_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_claim_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        get_claim_temp_cdpcip_claims();
	});

    function get_claim_temp_cdpcip_claims(){
        $("#dv_claim_temp").hide();
        $("#tbl_claim_temp tbody").empty();
        $.post("<?= base_url('get_claim_temp_cdpcip_claims') ?>",
            function(data){
                var d = JSON.parse(data);
                var count = 0;

                for (var i = 0; i < d.length; i++) {
                    count = d.length;
                }

                if (count != 0) {

                    $("#dv_claim_temp").show();
                    $.each(d, function(k, v){

                        $("#tbl_claim_temp tbody").append('<tr>\
                                                                <td>'+v.firstname+' '+v.middlename+' '+v.lastname+' '+v.name_ext+'</td>\
                                                                <td>'+v.rank_name+'</td>\
                                                                <td>'+( v.date_paid != null ? v.date_paid:'' )+'</td>\
                                                                <td>'+v.amount+'</td>\
                                                                <td>'+( v.status == 0 && v.status != null ? '<label class="label label-info block">Request for FUND</label>':'<label class="label label-info block">Credited to ATM</label>' )+'</td>\
                                                                <td class="text-center"><button class="btn btn-primary btn-circle" href="#modal_claim_temp" data-toggle="modal" onclick="get_info_claim_temp_cdpcip_claim('+v.claim_temp_id+',\''+v.firstname+'\',\''+v.middlename+'\',\''+v.lastname+'\',\''+v.name_ext+'\',\''+v.rank_name+'\',\''+v.date_paid+'\',\''+v.amount+'\',\''+v.date+'\',\''+v.date_requested+'\',\''+v.date_approved+'\',\''+v.status+'\',\''+v.claim_code+'\')" title="Add to user"><i class="fa fa-plus"></i></button></td>\
                                                            </tr>');

                    });
                    
                }
            }
        );

    }

    function get_info_claim_temp_cdpcip_claim(claim_temp_id, firstname, middlename, lastname, name_ext, rank_name, date_paid, amount, date, date_requested, date_approved, status, claim_code) {
        clear_modal("form_claim_temp");
        $("#form_claim_temp [name=claim_temp_id]").val(( claim_temp_id != "null" ? claim_temp_id:"" ));
        $("#form_claim_temp [name=firstname]").val(( firstname != "null" ? firstname:"" ));
        $("#form_claim_temp [name=middlename]").val(( middlename != "null" ? middlename:"" ));
        $("#form_claim_temp [name=lastname]").val(( lastname != "null" ? lastname:"" ));
        $("#form_claim_temp [name=name_ext]").val(( name_ext != "null" ? name_ext:"" ));
        $("#form_claim_temp [name=rank_name]").val(( rank_name != "null" ? rank_name:"" ));
        $("#form_claim_temp [name=date_paid]").val(( date_paid != "null" ? date_paid:"" ));
        $("#form_claim_temp [name=amount]").val(( amount != "null" ? amount:"" ));
        $("#form_claim_temp [name=date_requested]").val(( date_requested != "null" ? date_requested:"" ));
        $("#form_claim_temp [name=date_approved]").val(( date_approved != "null" ? date_approved:"" ));
        $("#form_claim_temp [name=status]").val(( status != "null" ? status:"" ));
        $("#form_claim_temp [name=claim_code]").val(( claim_code != "null" ? claim_code:"" ));
    }

</script>


<?php 


    public function get_claim_temp_cdpcip_claims()
    {
        $data = [];
        $where = [
            "created_by"    => $this->session->login_id,
            "is_added"      => 0
        ];
        foreach ($this->model_claim_cdp_cip_temp->select("*", $where) as $value) {
            $data[] = [
                "claim_cdp_cip_temp_id" => $value->claim_cdp_cip_temp_id,
                "firstname"             => $value->firstname,
                "middlename"            => $value->middlename,
                "lastname"              => $value->lastname,
                "name_ext"              => $value->name_ext,
                "rank_name"             => $value->rank_name,
                "date_paid"             => date("Y-m-d H:i:s", strtotime($value->date_paid)),
                "amount"                => $value->amount,
                "date_approved"         => date("Y-m-d H:i:s", strtotime($value->date_approved)),
                "status"                => $value->status,
            ];
        }
        echo json_encode($data);
    }

    public function get_info_cdpcip_claim() {
        $data = [];
        foreach ($this->model_claim_cdp_cip->select("*", ["claim_cdp_cip_id" => $this->input->post("value")]) as $key => $value) {
            $data = [
                "claim_cdp_cip_id"  => $value->claim_cdp_cip_id,
                "user_id"           => $value->user_id,
                "date_paid"         => date("Y-m-d", strtotime($value->date_paid)),
                "amount"            => $value->amount,
                "status"            => $value->status,
            ];
        }
        echo json_encode($data);
    }

    public function delete_cdpcip_claim() {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $this->user_log("Deleted claim cdp cip '".$this->get_user_id($this->input->post("value"))."'");
        
        if ($this->model_claim_cdp_cip->delete(["claim_cdp_cip_id" => $this->input->post("value")])) {

            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];          
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
        echo json_encode($ret);
    }

    public function insert_single_cdpcip_claim()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $process = "";

        $claim_cdp_cip_id   = $this->input->post("claim_cdp_cip_id");
        $alert      = $this->input->post("alert");
        $data = [
            "user_id"       => $this->input->post("user_id"),
            "date_paid"     => $this->input->post("date_paid"),
            "amount"        => $this->input->post("amount"),
            "status"        => $this->input->post("status")
        ];

        if ($this->input->post("status") == 0) {
            $data += [
                "date_requested" => $this->now()
            ];
        } else {
            $data += [
                "date_approved" => $this->now()
            ];
        }

        if($claim_cdp_cip_id == null) {
            if ($alert == "ok" && $alert != "") {
                $data += [
                    "created_by"    => $this->session->login_id,
                    "date_created"  => $this->now()
                ];
                $this->model_claim_cdp_cip->insert($data);
                $warning = "success";
            } else {
                $warning = "alert";
            }
            $process = "insert";                
        } else {
            $data += [
                "modified_by"   => $this->session->login_id,
                "date_modified" => $this->now()
            ];
            $this->model_claim_cdp_cip->update($data, ["claim_cdp_cip_id" => $claim_cdp_cip_id]);
            $process = "update";
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];
        }
        else {
            $this->db->trans_commit();
            if ($process == "insert") {
                if ($warning == "alert") {
                    $ret = [
                        "success"   => false,
                        "msg"       => "<span class='fa fa-warning'></span> You want to add this data?",
                        "alert"     => true
                    ];
                } else {
                    $this->user_log("Inserted claim cdp cip '".$this->full_name($this->input->post("user_id"))."'");
                    $ret = [
                        "success"   => true,
                        "msg"       => "<span class='fa fa-check'></span> Success"
                    ];                  
                }
            } else {
                $this->user_log("Updated claim cdp cip '".$this->full_name($this->input->post("user_id"))."'");
                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Updated"
                ];
            }
        }
        echo json_encode($ret);
    }

    public function insert_credited_cdpcip_claim()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $upload_alert = $this->input->post("alert");
        $upload_error = "";

        $count = 0;
        if (!empty($_FILES["file"]["name"])) {

            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];
            
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            $count = $object->setActiveSheetIndex(0)->getHighestRow() - 9;  

            if ($upload_alert == "ok" && $upload_alert != null) {

                try {
                    foreach ($object->getWorksheetIterator() as $worksheet) {
                        
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn(); 
                        $claim_type = $worksheet->getCellByColumnAndRow(5,8)->getValue();

                        $new_claim_type = "";

                        if ($claim_type == "Combat Duty Pay") {
                            $new_claim_type = "CDP";
                            for ($row=10; $row<=$highestRow; $row++) {

                                $no                 = $worksheet->getCellByColumnAndRow(0,$row)->getValue();       
                                $rank_name          = $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
                                $lastname           = $worksheet->getCellByColumnAndRow(2,$row)->getValue(); 
                                $firstname          = $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
                                $middlename         = $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
                                $name_ext           = $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
                                $amount             = $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
                                $date_paid          = $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 

                                $rank_id = "";
                                if ($this->check_rank_name($rank_name) != 0) {

                                    $rank_id = $this->get_rank_id($rank_name);

                                } else {

                                    $this->model_rank->insert([
                                        "rank_name"     => $rank_name,
                                        "created_by"    => $this->session->login_id,
                                        "date_created"  => $this->now()
                                    ]);
                                    $rank_id = $this->db->insert_id();

                                }

                                $fullname = ucfirst($firstname)." ".ucfirst($middlename)." ".ucfirst($lastname)." ".ucfirst($name_ext);

                                if ($this->check_user_exist(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext)) != 0) {

                                    $user_id = $this->get_user_exist_id(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext));

                                    if ($this->check_claim_cdp_cip($user_id, $date_paid) != 0) {

                                        $data = [
                                            "user_id"           => $user_id,
                                            "date_paid"         => $date_paid,
                                            "amount"            => $amount,
                                            "status"            => 1,
                                            "claim_type"        => $new_claim_type,
                                            "modified_by"       => $this->session->login_id,
                                            "date_modified"     => $this->now()
                                        ];
                                        $this->model_claim_cdp_cip->update($data, ["claim_cdp_cip_id" => $this->get_claim_cdp_cip_id($user_id, $date_paid)]);
                                        $this->user_log("Updated claim cdp cip '".$fullname."'");

                                    } else {

                                        $data = [
                                            "user_id"           => $user_id,
                                            "date_paid"         => $date_paid,
                                            "amount"            => $amount,
                                            "status"            => 1,
                                            "claim_type"        => $new_claim_type,
                                            "created_by"        => $this->session->login_id,
                                            "date_created"      => $this->now()
                                        ];
                                        $this->model_claim_cdp_cip->insert($data);
                                        $this->user_log("Inserted claim cdp cip '".$fullname."'");

                                    }

                                } else {

                                    $data = [
                                        "firstname"         => ucfirst($firstname),
                                        "middlename"        => ucfirst($middlename),
                                        "lastname"          => ucfirst($lastname),
                                        "name_ext"          => ucfirst($name_ext),
                                        "rank_name"         => $rank_name,
                                        "date_paid"         => $date_paid,
                                        "amount"            => $amount,
                                        "date_requested"    => $this->now(),
                                        "date_approved"     => $this->now(),
                                        "status"            => 1,
                                        "is_added"          => 0,
                                        "claim_type"        => $new_claim_type,
                                        "created_by"        => $this->session->login_id,
                                        "date_created"      => $this->now()
                                    ];
                                    $this->model_claim_cdp_cip_temp->insert($data);
                                    $this->user_log("Inserted claim in temp not exist from user '".$fullname."'");

                                }

                            }
                        } else if ($claim_type == "COMBAT INCENTIVE PAY") {
                            $new_claim_type = "CIP";

                            for ($row=10; $row<=$highestRow; $row++) {

                                $no                 = $worksheet->getCellByColumnAndRow(0,$row)->getValue();       
                                $rank_name          = $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
                                $lastname           = $worksheet->getCellByColumnAndRow(2,$row)->getValue(); 
                                $firstname          = $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
                                $middlename         = $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
                                $name_ext           = $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
                                $amount             = $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
                                $date_paid          = $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 

                                $rank_id = "";
                                if ($this->check_rank_name($rank_name) != 0) {

                                    $rank_id = $this->get_rank_id($rank_name);

                                } else {

                                    $this->model_rank->insert([
                                        "rank_name"     => $rank_name,
                                        "created_by"    => $this->session->login_id,
                                        "date_created"  => $this->now()
                                    ]);
                                    $rank_id = $this->db->insert_id();

                                }

                                $fullname = ucfirst($firstname)." ".ucfirst($middlename)." ".ucfirst($lastname)." ".ucfirst($name_ext);

                                if ($this->check_user_exist(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext)) != 0) {

                                    $user_id = $this->get_user_exist_id(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext));

                                    if ($this->check_claim_cdp_cip($user_id, $date_paid) != 0) {

                                        $data = [
                                            "user_id"           => $user_id,
                                            "date_paid"         => $date_paid,
                                            "amount"            => $amount,
                                            "status"            => 1,
                                            "claim_type"        => $new_claim_type,
                                            "modified_by"       => $this->session->login_id,
                                            "date_modified"     => $this->now()
                                        ];
                                        $this->model_claim_cdp_cip->update($data, ["claim_cdp_cip_id" => $this->get_claim_cdp_cip_id($user_id, $date_paid)]);
                                        $this->user_log("Updated claim cdp cip '".$fullname."'");

                                    } else {

                                        $data = [
                                            "user_id"           => $user_id,
                                            "date_paid"         => $date_paid,
                                            "amount"            => $amount,
                                            "status"            => 1,
                                            "claim_type"        => $new_claim_type,
                                            "created_by"        => $this->session->login_id,
                                            "date_created"      => $this->now()
                                        ];
                                        $this->model_claim_cdp_cip->insert($data);
                                        $this->user_log("Inserted claim cdp cip '".$fullname."'");

                                    }

                                } else {

                                    $data = [
                                        "firstname"         => ucfirst($firstname),
                                        "middlename"        => ucfirst($middlename),
                                        "lastname"          => ucfirst($lastname),
                                        "name_ext"          => ucfirst($name_ext),
                                        "rank_name"         => $rank_name,
                                        "date_paid"         => $date_paid,
                                        "amount"            => $amount,
                                        "date_requested"    => $this->now(),
                                        "date_approved"     => $this->now(),
                                        "status"            => 1,
                                        "is_added"          => 0,
                                        "claim_type"        => $new_claim_type,
                                        "created_by"        => $this->session->login_id,
                                        "date_created"      => $this->now()
                                    ];
                                    $this->model_claim_cdp_cip_temp->insert($data);
                                    $this->user_log("Inserted claim in temp not exist from user '".$fullname."'");

                                }

                            }
                        }
                        
                    }

                    $warning = "success";

                } catch (Exception $e) {

                    $warning = "catch_warning";
                    $upload_error = $e->getMessaage();

                }

            } else {

                $warning = "alert";

            }

        }

        if($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];

        }
        else {

            $this->db->trans_commit();

            if ($warning == "alert") {

                $ret = [
                    "success"   => false,
                    "msg"       => "You want to upload these <strong>$count</strong> data?",
                    "alert"     => true,
                    "count"     => "<strong>$count</strong>"
                ];

            } elseif ($warning == "catch_warning") {

                $ret = [
                    "success"   => false,
                    "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
                ];

            } else {

                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Success"
                ];  

            }

        }

        echo json_encode($ret);
    }

    public function insert_claim_temp_to_user_and_claim()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $process = "";

        $alert                  = $this->input->post("alert");

        $claim_cdp_cip_temp_id  = $this->input->post("claim_cdp_cip_temp_id");

        $date_requested = "";

        foreach ($this->model_claim_cdp_cip_temp->select("*", ["claim_cdp_cip_temp_id" => $claim_cdp_cip_temp_id]) as $key => $value) {
            $date_requested = $value->date_requested;
        }

        $user_id = "";

        $data = [
            "firstname"     => ucfirst($this->input->post("firstname")),
            "middlename"    => ucfirst($this->input->post("middlename")),
            "lastname"      => ucfirst($this->input->post("lastname")),
            "name_ext"      => ucfirst($this->input->post("name_ext")),
            "rank_id"       => $this->get_rank_id($this->input->post("rank_name")),
            "user_role_id"  => 3,
            "status"        => 1,
            "badge_number"  => ucfirst($firstname[0]).ucfirst($middlename[0]).ucfirst($lastname),
            "username"      => strtolower($firstname),
            "password"      => md5(strtolower($lastname)),
        ];

        if ($alert == "ok" && $alert != "") {

            $data += [
                "created_by"    => $this->session->login_id,
                "date_created"  => $this->now()
            ];
            $this->model_user->insert($data);
            $user_id = $this->db->insert_id();

            $data_claim_cdp_cip = [
                "user_id"           => $user_id,
                "amount"            => $this->input->post("amount"),
                "date_paid"         => $this->input->post("date_paid"),
                "date"              => $this->input->post("date"),
                "date_requested"    => $date_requested,
                "date_approved"     => $this->input->post("date_approved"),
                "status"            => $this->input->post("status")
            ];
            $this->model_claim_cdp_cip->insert($data_claim_cdp_cip);

            // $this->model_permission->insert([
            //  "user_id"       => $user_id,
            //  "mod_button_id" => 21,
            //  "status"        => 1
            // ]);

            // $this->model_permission->insert([
            //  "user_id"       => $user_id,
            //  "mod_button_id" => 22,
            //  "status"        => 1
            // ]);

            // $this->model_permission->insert([
            //  "user_id"       => $user_id,
            //  "mod_button_id" => 23,
            //  "status"        => 1
            // ]);

            // $this->model_permission->insert([
            //  "user_id"       => $user_id,
            //  "mod_button_id" => 24,
            //  "status"        => 1
            // ]);

            $this->model_claim_cdp_cip_temp->update(["is_added" => 1], ["claim_cdp_cip_temp_id" => $claim_cdp_cip_temp_id]);

            $warning = "success";

        } else {

            $warning = "alert";

        }

        if($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];

        }
        else {

            $this->db->trans_commit();

            if ($warning == "alert") {

                $ret = [
                    "success"   => false,
                    "msg"       => "<span class='fa fa-warning'></span> You want to add this data?",
                    "alert"     => true
                ];

            } else {

                $this->user_log("Inserted users '".$this->full_name($user_id)."'");
                $this->user_log("Inserted claim cdp cip '".$this->full_name($user_id)."'");
                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Success"
                ];  

            }

        }
        echo json_encode($ret);
    }

    // checking
    public function check_rank_name($rank_name)
    {
        $count = 0;
        $where = [
            "rank_name" => $rank_name
        ];
        foreach ($this->model_rank->select("COUNT(rank_name) AS count", $where) as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    public function get_rank_id($rank_name)
    {
        $rank_id = "";
        $where = [
            "rank_name" => $rank_name
        ];
        foreach ($this->model_rank->select("rank_id", $where) as $key => $value) {
            $rank_id = $value->rank_id;
        }
        return $rank_id;
    }

    public function check_user_exist($firstname, $middlename, $lastname, $name_ext)
    {
        $count = 0;
        $where = [
            "firstname"         => $firstname,
            "middlename LIKE"   => "%$middlename%",
            "lastname"          => $lastname,
            "name_ext"          => $name_ext
        ];
        foreach ($this->model_user->select("COUNT(*) AS count", $where) as $value) {
            $count = $value->count;
        }
        return $count;
    }
    public function get_user_exist_id($firstname, $middlename, $lastname, $name_ext)
    {
        $data = "";
        $where = [
            "firstname"     => $firstname,
            "middlename LIKE"   => "%$middlename%",
            "lastname"      => $lastname,
            "name_ext"  => $name_ext, 
        ];
        foreach ($this->model_user->select("*", $where) as $key => $value) {
            $data = $value->user_id;
        }
        return $data;
    }

    public function check_claim_cdp_cip($user_id, $date_paid)
    {
        $count = 0;
        $where = [
            "user_id" => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cdp_cip->select("COUNT(*) AS count", $where) as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    public function get_claim_cdp_cip_id($user_id, $date_paid)
    {
        $data = "";
        $where = [
            "user_id"   => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cdp_cip->select("*", $where) as $key => $value) {
            $data = $value->claim_cdp_cip_id;
        }
        return $data;
    }

    // others
    public function get_user_select()
    {
        $data = [];
        foreach ($this->model_user->select("*", [], [], ["lastname" => "asc"]) as $key => $value) {
            $data[] = [
                "id"    => $value->user_id,
                "text"  => $value->lastname.", ".$value->firstname." ".$value->middlename." ".$value->name_ext
            ];
        }
        return json_encode($data);
    }

    public function get_user_id($claim_cdp_cip_id)
    {
        $data = "";
        foreach ($this->model_claim_cdp_cip->select("*", ["claim_cdp_cip_id" => $claim_cdp_cip_id]) as $value) {
            $data = $this->full_name($value->user_id);
            break;
        }
        return $data;
    }


 ?>