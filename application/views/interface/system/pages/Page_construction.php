<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Under Construction | <?= $system_title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    <?php $this->load->view('interface/system/scripts/Css'); ?>
</head>
<body oncontextmenu="return false">
    <div id="wrapper">
        <?php $this->load->view('interface/system/layout/Side_menu'); ?>
        <div id="page-wrapper" class="gray-bg">
            <?php $this->load->view('interface/system/layout/Header'); ?>
            <div class="wrapper wrapper-content animated fadeInRight">
                <center>
                  <h3 class="font-bold">The page is under construction.</h3>
                  <div class="error-desc">
                      Please contact administrator for more information.
                      <br/>
                      <a href="<?= site_url() ?>" class="btn btn-primary m-t">Home</a>
                  </div>
                </center>
                <!--/. end of row -->
            </div>
            <!--/. end of wrapper -->
            <?php $this->load->view('interface/system/layout/Footer'); ?>
        </div>
        <!--/. end of page-wrapper -->
    </div>
    <?php $this->load->view('interface/system/scripts/Js'); ?>
</body>
</html>