<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $page_title ?> | <?= $system_title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    <?php $this->load->view('interface/system/scripts/Css'); ?>
</head>
<body class="lockscreen" oncontextmenu="return false">
	<div class="content-wrapper" style="margin:auto;">
		<section class="content">
			<div class="error-page">
				<div class="error-content">
					<center>
						<h2 style="margin-top:5em;">
							<i class="fa fa-warning text-yellow"></i> PAGE UNDER CONSTRUCTION
						</h2>
						<p>
							This page is under construction. Meanwhile, you may return to 
							<a href="<?= site_url() ?>"> <?= $system_title ?> </a>homepage. 
						</p>
						<img src ="<?= base_url() ?>assets/Images/500.png" width="50%">
					</center>
				</div>
			</div>
		</section>
      </div>
	</div>
</body>
</html>