<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <title><?= $page_title ?> | <?= $system_title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    <?php $this->load->view('interface/system/scripts/Css'); ?>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.1.1.js"></script>

</head>
<body class="fixed-sidebar fixed-nav md-skin">        <!-- oncontextmenu="return false" -->

    <div id="wrapper">
        <?php $this->load->view('interface/system/layout/Sidemenu'); ?>
        <div id="page-wrapper" class="gray-bg">
            <?php $this->load->view('interface/system/layout/Header'); ?>

            <!--CONTENT -->
            <div class="wrapper wrapper-content animated fadeInRight">
                <?php if ($content_title != ""): ?>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2><?= $content_title ?></h2>
                        </div>
                    </div>                            
                <?php endif ?>
                <?php
                    foreach ($content_data as $row) {
                        echo $row;
                    }
                ?>
            </div>
            <!-- END OF CONTENT -->

            <?php $this->load->view('interface/system/layout/Footer'); ?>
            <?php $this->load->view('interface/system/scripts/Js'); ?>
        </div>
    </div>
</body>
</html>
