<style type="text/css">
    .navbar.navbar-fixed-top{
        background: #fff !important; 
    }
    .navbar-minimalize.minimalize-styl-2.btn.btn-primary{
        background: #eaeff2 !important;   
        border-color: #1ab394 !important;
        color: #36607f !important;
    }
    .md-skin .nav.navbar-right li a:hover, a.alias:hover{
        color: #1ab394 !important;
    }
    /*.dropdown.open a#count-info{
        background: #fff !important;
        color: #1ab394 !important;
    }
    .dropdown-toggle#count-info{
        color: #36607f  !important;
    }*/
    .md-skin .nav.navbar-right > li > a {
        background: #fff !important;
        color: #36607f !important;
    }
    /*#alias{
        color: #36607f  !important;
    }*/
    .right-sidebar-toggle {
         color: #36607f  !important;
    }
    #li_view_notification .dropdown-menu > li > a {
        line-height: normal;
    }

    .md-skin .nav.navbar-right > li > a.logActive {
        color: #1ab394 !important;
    }
    .md-skin .navbar-top-links li.open > a {
        color: #1ab394 !important;
    }
</style>
<div class="row border-bottom">
    <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom:0;">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown" id="li_view_notification" name="view_notification" onclick="show_notifications()">
                <a id="count-info" class="dropdown-toggle count-info" data-toggle="dropdown" href="#" title="Notifications">
                    <i class="fa fa-bell"></i>  <span id="count_notif"></span>
                </a>
                <ul class="dropdown-menu dropdown-alerts animated fadeInRight" id="notifications">
                    <li>
                        <div class="text-center link-block">
                            <a href="javascript:;">
                                <i class="fa fa-bell"></i> <strong>Notifications</strong>
                            </a>
                        </div>
                    </li>
                    <li class="divider" style="margin:0px;"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="javascript:;">
                                No Notifications
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li><img src="<?= ( $images != null ? base_url().$images : base_url().'assets/custom/images/default.png' ) ?>" style="height: 35px; border-radius: 50%;" class="img-circle"></li>
            <li class="dropdown">
                <a id="alias" class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><?= $user_name ?><span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu animated fadeInRight m-t-xs">
                    <li>
                        <a role="button" href="<?= base_url() ?>profile" class="btn btn-primary" id="btn_logout" style="width: 95%;"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a role="button" href="<?= base_url() ?>request_logout" class="btn btn-primary" id="btn_logout" style="width: 95%;"><i class="fa fa-sign-out"></i> Logout</a>
                    </li>
                </ul>
            </li>
            <?php if ($this->session->login_role == 1): ?>
                <li>
                    <a id="log" href="<?= base_url() ?>log" name="view_logs" title="Logs">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
            <?php endif ?>
        </ul>
    </nav>
</div>
<?= $this->load->view('interface/notification/Main', [], true); ?>