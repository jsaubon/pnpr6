<style type="text/css">
    .nav > li > a {
        color: #36607f !important;
    }
    .nav > li.active > a {
        color: #1ab394 !important;
    }
</style>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header" style="text-align: center; padding: 10px;">
                <div class="dropdown profile-element">
                    <a href="<?= base_url() ?>">
                        <img alt="image" style="width: 100px;" src="<?= base_url() ?>assets/custom/images/Philippine_National_Police_seal.svg.png"/>
                    </a>
                </div>
                <div class="logo-element">PNP</div>
            </li>

            <li id="lnk_Dashboards" <?= ($this->uri->segment(1) == "dashboard" ? "class='active'" : "class=''") ?>>
                <a href="<?= base_url().'dashboard' ?>"><i class="fa fa-dashboard"></i> <span class="nav-label"> Dashboard</span></a>
            </li>
            
            <li id="lnk_AdminActivities" <?= ($this->uri->segment(1) == "admin" ? "class='active'" : "class=''") ?>>
                <a href=""><i class="fa fa-history"></i> <span class="nav-label"> Admin Activity</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li id="lnk_AdminClaims" class="<?= ($this->uri->segment(2) == "claim" ? "active show" : "show") ?>">
                        <a href="javascript:;"> Claims <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li id="lnk_AdminCDPCIPClaims" class="<?= ($this->uri->segment(3) == "cdpcip" ? "active show" : "show") ?>">
                                <a href="<?= site_url().'/admin/claim/cdpcip' ?>"> CDP CIP</a>
                            </li>
                            <li id="lnk_AdminOtherClaims" class="<?= ($this->uri->segment(3) == "otherclaim" ? "active show" : "show") ?>">
                                <a href="<?= site_url().'/admin/claim/otherclaim' ?>"> Other Claim</a>
                            </li>
                        </ul>
                    </li>

                    <li id="lnk_AdminMemoandUpdates" class="<?= ($this->uri->segment(2) == "memo_and_update" ? "active show" : "show") ?>">
                        <a href="<?= base_url() ?>admin/memo_and_update"> Memo and Updates</a>
                    </li>
                    <li id="lnk_AdminUsers" class="<?= ($this->uri->segment(2) == "user" ? "active show" : "show") ?>">
                        <a href="<?= base_url() ?>admin/user"> User</a>
                    </li>
                    <li id="lnk_AdminRanks" class="<?= ($this->uri->segment(2) == "rank" ? "active show" : "show") ?>">
                        <a href="<?= base_url() ?>admin/rank"> Ranked</a>
                    </li>
                    <li id="lnk_AdminTypeOfClaims" class="<?= ($this->uri->segment(2) == "type_of_claim" ? "active show" : "show") ?>">
                        <a href="<?= base_url() ?>admin/type_of_claim"> Type of Claim</a>
                    </li>
                </ul>
            </li>
            

            <li id="lnk_MOOES" class="<?= ($this->uri->segment(1) == "mooe" ? "active" : "") ?>">
                <a href="javascript:;"><i class="fa fa-angle-double-right"></i> Mooe<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li id="lnk_AssignPPO" class="<?= ($this->uri->segment(2) == "assign_ppo" ? "active show" : "show") ?>">
                        <a href="<?= site_url().'/mooe/assign_ppo' ?>"> Assign PPO</a>
                    </li>
                    <li id="lnk_AssignMPS" class="<?= ($this->uri->segment(2) == "assign_mps" ? "active show" : "show") ?>">
                        <a href="<?= site_url().'/mooe/assign_mps' ?>"> Assign MPS</a>
                    </li>
                    <li id="lnk_AssignRMFB" class="<?= ($this->uri->segment(2) == "assign_rmfb" ? "active show" : "show") ?>">
                        <a href="<?= site_url().'/mooe/assign_rmfb' ?>"> Assign RMFB</a>
                    </li>
                    <li id="lnk_NotifyPPO" class="<?= ($this->uri->segment(2) == "notif_ppo" ? "active show" : "show") ?>">
                        <a href="<?= site_url().'/mooe/notif_ppo' ?>"> Notify PPO</a>
                    </li>
                    <li id="lnk_NotifyMPS" class="<?= ($this->uri->segment(2) == "notif_mps" ? "active show" : "show") ?>">
                        <a href="<?= site_url().'/mooe/notif_mps' ?>"> Notify MPS</a>
                    </li>
                </ul>
            </li>

            <li id="lnk_UserClaims" class="<?= ($this->uri->segment(1) == "user_claim" ? "active" : "") ?>">
                <a href="<?= base_url() ?>user_claim"><i class="fa fa-hashtag"></i> <span class="nav-label"> Claim</span></a>
            </li>
            
        </ul>
    </div>
</nav>

