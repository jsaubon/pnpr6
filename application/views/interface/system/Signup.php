<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <title><?= $page_title ?> | <?= $system_title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    <?php $this->load->view('interface/system/scripts/Css'); ?>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.1.1.js"></script>

</head>
<body class="gray-bg">
	
	<div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name" style="position: relative; left: -110px;">PNPR6</h1>

            </div>
            <h3>Register to PNPR6</h3>
            <p>Create account to see it in action.</p>
            <form class="m-t" role="form" action="request_signup" method="POST" accept-charset="utf-8" id="form_signup">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <input type="password" name="con_password" class="form-control" placeholder="Confirm Password" required>
                </div>
                <div class="form-group">
                    <input type="text" name="badge_number" class="form-control" placeholder="Badge Number" required>
                </div>
                <div class="form-group">
                    <input type="text" name="firstname" class="form-control" placeholder="Firstname" required>
                </div>
                <div class="form-group">
                    <input type="text" name="lastname" class="form-control" placeholder="Lastname" required>
                </div>
                <div class="form-group">
                        <div class="checkbox i-checks">
                        	<label> <input type="checkbox" name="gender" value="Male"><i></i> Male </label>
                        	<label> <input type="checkbox" name="gender" value="Female"><i></i> Female </label>
                        </div>
                </div>
                <div class="form-group">
                       <textarea name="address" class="form-control" placeholder="Address" rows="5" style="resize: vertical;"></textarea>
                </div>
                <div class="form-group">
                    <select name="rank_id" class="form-control" placeholder="Ranked" required style="width: 100%;">
                    	<option></option>
                    </select>
                </div>
                <div class="form-group">
                	<div class="hr-line-solid"></div>
                </div>
                <div class="form-group">
                        <div class="checkbox i-checks"><label> <input type="checkbox" name="agree" required><i></i> Agree the terms and policy </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

                <!-- <p class="text-muted text-center"><small>Already have an account?</small></p> -->
                <!-- <a class="btn btn-sm btn-white btn-block" href="< ?=  ?>">Login</a> -->
            </form>
            <p class="m-t"> <small>PNPR6 &copy; <?= date("Y") ?></small> </p>
        </div>
    </div>


    <script>

        console.log("%c%s", "color: red; font-size: 50px;", "STOP!");
        console.log("%c%s", "color: #000; font-size: 25px;", "This is for entended developers!");

        $("body").on("contextmenu", function(){
            return false;
        });
    
        $(document).ready(function(){

        	$("[name=rank_id]").select2({
        		placeholder: "Search",
	            allowClear: true,
	            data: <?= $ranks; ?>
        	});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $("#form_signup").ajaxForm({
                clearForm: false,
                resetForm: false,
                beforeSubmit: function() {
                    $("#signup_btnSave").attr("disabled", true);
                    $("#signup_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
                },
                success: function(data) {
                    var d = JSON.parse(data);
                    if(d.success == true) {
                        swal({
                        	html:true,
                            position: 'center',
                            type: 'success',
                            title: '<div class="text-primary">'+d.msg+'</div>',
                            showConfirmButton: false,
                            timer: 1000,
                            customClass: 'animated bounceIn'
                        });

                        $("#signup_btnSave").attr("disabled", false);
                        $("#signup_btnSave").html("<span class=\"fa fa-check\"></span> Save");

                        location.replace("<?= base_url() ?>");
                    } else {
                        $("#signup_btnSave").attr("disabled", false);
                        $("#signup_btnSave").html("<span class=\"fa fa-warning\"></span> Try Again.");
                        swal({
                        	html:true,
                            title: '<div class="text-warning">'+d.msg+'</div>',
                            type: 'warning',
                            animation: false,
                            timer: 1000,
                            customClass: 'animated tada'
                        });
                    }
                }
            });

        });

    </script>

    <?php $this->load->view('interface/system/scripts/Js'); ?>
</body>
</html>