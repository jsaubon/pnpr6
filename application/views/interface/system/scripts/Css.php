<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/animate.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

<link href="<?= base_url() ?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

<link href="<?= base_url() ?>assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/fullcalendar/fullcalendar.min.css" rel='stylesheet' />
<link href="<?= base_url() ?>assets/custom/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print' />
<link href="<?= base_url() ?>assets/custom/css-custom/styles-common.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/css-custom/styles-theme.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/css-custom/styles-theme-responsive.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/dist/sweetalert.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/notiny/dist/notiny.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/dhtmlxcombo/codebase/dhtmlxcombo.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/craftpip/dist/jquery-confirm.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/custom/treegrid/css/jquery.treegrid.css" rel='stylesheet' />
<link href="<?= base_url() ?>assets/custom/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

<style type="text/css">
	[class^='select2'] { border-radius: 0px !important; border-color: #d9d9d9 !important; font-size: 14px;}
	.select2-close-mask{ z-index: 2099; }
	.select2-dropdown{ z-index: 3051; }
	.notiny-theme-inspinia { background-color: #1ab394; border-color: #fff200; color: #FFFFFF; }
	.notiny-theme-light { background-color: #ffffff; border-color: #fff200; color: #000000; }
	.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
	.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; font-size: 14px;}
	.autocomplete-selected { background: #F0F0F0; }
	.autocomplete-suggestions strong { font-weight: normal; color: #1ab394; font-size: 18px;}
	.autocomplete-group { padding: 2px 5px; }
	.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
	.dhxcombo_material { border: 1px solid #d9d9d9; z-index: 99999; width: 100% !important;}
	.dhxcombolist_material { z-index: 4000 !important; width: 300px !important;}
	.dhxcombo_input { width: 95% !important; }
</style>
<style type="text/css">
	.md-skin .ibox-title {
		border-bottom: 1px solid #e7eaec !important;
	}

	.b-r {
		border-color: #1ab394;
	}
	
	::-webkit-scrollbar {
        width: 8px;
        height: 8px;
        background-color: #F5F5F5;
    }
	::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
	    border-radius: 10px;
    }
    ::-webkit-scrollbar-thumb {
    	-webkit-border-radius: 10px;
	    border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }
    
	#btn_logout {
		color: #fff;
		text-align: center;
	}
	#btn_logout:hover {
		color: #333;
	}
	.profile-element a img {
		/*background: #1ab394;*/
		box-shadow: 0 0 25px -5px #9e9c9e;
	}
	.fieldset 
	{
	    border: 1px solid #ddd !important;
	    margin: 0;
	    xmin-width: 0;
	    padding: 10px;       
	    position: relative;
	    border-radius:4px;
	    padding-left:10px!important;
	}   
	.fieldset .legend
	{
		color: #fff;
	    font-size:14px;
	    font-weight:bold;
	    margin-bottom: 0px; 
	    width: 35%; 
	    border: 1px solid #ddd;
	    border-radius: 4px; 
	    padding: 5px 5px 5px 10px; 
	    background-color:#24437f;
	}

	.fieldset .legend-with-btn
	{
	    width: 35%;
	    border-radius: 4px; 
	    background-color:#f5f5f5;
	    margin-bottom: 0px;
	    font-weight:bold;
	    border: 1px solid #ddd;
	}

</style>