<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/inspinia.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pace/pace.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/dataTables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/dataTables/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/cropper/cropper.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/nestable/jquery.nestable.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/fullcalendar/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tinycon/tinycon.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/notiny/dist/notiny.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/jquery-autocomplete/dist/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/dhtmlxcombo/codebase/dhtmlxcombo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/craftpip/dist/jquery-confirm.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/bootstrap-fileinput-master/js/plugins/sortable.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/treegrid/js/jquery.treegrid.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/custom/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">

	<?php if ($this->session->login_alias != "Klaven Rey Matugas"): ?>
        // console.log("%c%s", "color: red; font-size: 50px; font-weight: 700; text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);", "STOP!");
        console.log("%c%s", "color: red; font-size: 50px; font-weight: bold; text-shadow: 1px 1px 0px black, 1px -1px 0px black, -1px 1px 0px black, -1px -1px 0px black;", "STOP!");
        console.log("%c%s", "color: #000; font-size: 25px;", "This is for entended developers! Your action is bieng recorded!");
        $("body").on("contextmenu", function(){
            return false;
        });        
    <?php endif ?>
	 
	$(function() {
		
		check_user_permission();

		$.notiny.addTheme('dark', {
        	notification_class: 'notiny-theme-inspinia notiny-default-vars'
    	});
    	// console.log("%cDuckie.TV", "color:transparent; font-size: 100px; line-height: 125px; padding:25px; padding-top:30px; padding-bottom:60px; background-image:url(http://duckietv.github.io/DuckieTV/img/icon128.png); background-repeat:no-repeat; ", "quack!\n\n\n\n\n\n");

		// console.log("%c%s", "color: red; font-size: 50px;", "STOP!");
		// console.log("%c%s", "color: #000; font-size: 25px;", "This is for entended developers!");

		// $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
		// 	function(json) {
		// 		console.log("My public IP address is: ", json.ip);
		// 	}
		// );
	});

	function check_user_permission() {
		var permission = <?= $permission ?>;
		if(permission != null) {
			for(var x = 0; x < permission[0].length; x++) {
				if(permission[0][x].button_code == "view_page") {
					if(permission[0][x].status == 0) {
						location.replace("<?= base_url('dashboard') ?>");
					}
				} else {
					if(permission[0][x].status == 0) {
						$('[name='+permission[0][x].button_code+']').remove();
					}
				}
			}
			for(var x = 0; x < permission[1].length; x++) {
				if(permission[1][x].status == 0) {
					$('#lnk_'+permission[1][x].module_name).remove();
				}
			}
		}

		if ($('#lnk_AdminActivities ul li.show').length == 0 || $('#lnk_AdminActivities ul li.show').length == 1) {
	        $("#lnk_AdminActivities").remove();
	    }
	    if ($('#lnk_AdminClaims ul li.show').length == 0) {
	        $("#lnk_AdminClaims").remove();
	    }
	    if ($('#lnk_MOOES ul li.show').length == 0) {
	        $("#lnk_MOOES").remove();
	    }
	}


	function clear_modal(form_id) {
		$("#"+form_id)[0].reset();
		$("#"+form_id).find("input[type='hidden']").each(function() {
			$(this).val("");
		});
		$("#"+form_id).find("input[type='checkbox']").each(function() {
			$(this).attr("checked", false);
		});
		$(".select2").val("").trigger("change");
	}

	function get_info(url, value, form_id) {
		$.post("<?= base_url() ?>" + url, 
			{ value: value }, 
			function(data) {
				var result = JSON.parse(data);
				$.each(result, function(k, v) {
					$("#"+form_id).each(function() {
						$("[name="+k+"]").val(v);
						$(".select2").trigger("change");
					});
				});
			}
		);
	}

	function delete_this(url, value, tbl) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Warning',
			content: 'Are you sure you want to delete this?',
			confirmButton: "<span class='fa fa-check'></span> Yes",
			cancelButton: "<span class='fa fa-remove'></span> No",
			confirmButtonClass: "btn btn-sm btn-default",
			cancelButtonClass: "btn btn-sm btn-primary",
			animation: 'rotateY',
    		closeAnimation: 'rotateY',
			confirm: function() {
				$.post("<?= base_url() ?>" + url,
					{ value: value }, 
					function(data) {
						var d = JSON.parse(data);
						if(d.success == true) {
							$(".ibox").children(".ibox-content").toggleClass("sk-loading");
						    $.notiny({ text: d.msg, position: "right-top" });
						    for(var x=0; x < tbl.length; x++) {
						    	tbl[x].ajax.reload();
						    }
						} else {
						    $.notiny({ text: d.msg, position: "right-top" });
						}
					}
				);
			},
			cancel: function() {}
		});
	}

	function number_format(n) {
		return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	}

	function numToWords(number) {
	    //Validates the number input and makes it a string
	    if (typeof number === 'string') {
	        number = parseInt(number, 10);
	    }
	    if (typeof number === 'number' && isFinite(number)) {
	        number = number.toString(10);
	    } else {
	        return 'This is not a valid number';
	    }

	    //Creates an array with the number's digits and
	    //adds the necessary amount of 0 to make it fully 
	    //divisible by 3
	    var digits = number.split('');
	    while (digits.length % 3 !== 0) {
	        digits.unshift('0');
	    }

	    //Groups the digits in groups of three
	    var digitsGroup = [];
	    var numberOfGroups = digits.length / 3;
	    for (var i = 0; i < numberOfGroups; i++) {
	        digitsGroup[i] = digits.splice(0, 3);
	    }

	    //Change the group's numerical values to text
	    var digitsGroupLen = digitsGroup.length;
	    var numTxt = [
	        [null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'], //hundreds
	        [null, 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'], //tens
	        [null, 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'] //ones
	        ];
	    var tenthsDifferent = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];

	    // j maps the groups in the digitsGroup
	    // k maps the element's position in the group to the numTxt equivalent
	    // k values: 0 = hundreds, 1 = tens, 2 = ones
	    for (var j = 0; j < digitsGroupLen; j++) {
	        for (var k = 0; k < 3; k++) {
	            var currentValue = digitsGroup[j][k];
	            digitsGroup[j][k] = numTxt[k][currentValue];
	            if (k === 0 && currentValue !== '0') { // !==0 avoids creating a string "null hundred"
	                digitsGroup[j][k] += ' Hundred ';
	            } else if (k === 1 && currentValue === '1') { //Changes the value in the tens place and erases the value in the ones place
	                digitsGroup[j][k] = tenthsDifferent[digitsGroup[j][2]];
	                digitsGroup[j][2] = 0; //Sets to null. Because it sets the next k to be evaluated, setting this to null doesn't work.
	            }
	        }
	    }

	    //Adds '-' for gramar, cleans all null values, joins the group's elements into a string
	    for (var l = 0; l < digitsGroupLen; l++) {
	        if (digitsGroup[l][1] && digitsGroup[l][2]) {
	            digitsGroup[l][1] += '-';
	        }
	        digitsGroup[l].filter(function (e) {return e !== null});
	        digitsGroup[l] = digitsGroup[l].join('');
	    }

	    //Adds thousand, millions, billion and etc to the respective string.
	    var posfix = [null, 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion'];
	    if (digitsGroupLen > 1) {
	        var posfixRange = posfix.splice(0, digitsGroupLen).reverse();
	        for (var m = 0; m < digitsGroupLen - 1; m++) { //'-1' prevents adding a null posfix to the last group
	            if (digitsGroup[m]) {
	                digitsGroup[m] += ' ' + posfixRange[m];
	            }
	        }
	    }
	    //Joins all the string into one and returns it
	    return digitsGroup.join(' ');
	}

	function convert_to_words(number) {
		number = number.replace(/,/g , "");
		var whole_numbers = numToWords(number);
		var decimals = number.split(".");

		var decimals_word = "";
		if(whole_numbers != "") {
			whole_numbers+=" Pesos";
		}
		
		if(decimals[1] != null) {
			if(numToWords(decimals[1]) != "") {
				decimals_word = " And "+numToWords(decimals[1])+" Centavos";
			}
		}
		return whole_numbers+decimals_word;
	}

	function date(){
		return new Date();
	}

	// For todays date;
	Date.prototype.today = function () { 
	    return this.getFullYear() +"/"+ (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+((this.getDate() < 10)?"0":"") + this.getDate();
	}

	// For the time now
	Date.prototype.timeNow = function () {
	     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
	}
</script>