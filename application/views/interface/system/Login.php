<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page_title ?> | <?= $system_title ?></title>
    <link rel="icon" type="image/png" href="<?= $system_logo ?>">
    <style type="text/css">
        #link:hover { text-decoration: underline; }
    </style>
    <?php $this->load->view('interface/system/scripts/Css'); ?>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
</head>
<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <h2 class="font-bold text-center text-uppercase">Welcome <br> <small>to</small> <br> PNP RCD PRO 13</h2>

                <h4 class="font-bold text-center">
                    <img src="<?= base_url() ?>assets/custom/images/police_regional_office_13.png" style="width: 60%; height: 60%;" class="img-circle">
                </h4>

            </div>
            <div class="col-md-6 m-t-xl">
                <div class="ibox-content m-t-sm">
                    <h3 class="text-center font-bold">Please log in!</h3>
                    <?php if ($this->input->get("login_attempt") == md5(0)): ?>
                            <?= "<h5 class='text-danger'>Invalid Username or Password. Please try again.</h5>" ?>
                        <?php elseif ($this->input->get("login_attempt") == md5(1)): ?>
                            <?= "<h5 class='text-danger'>Sorry. You are block in this website.</h5>" ?>
                        <!-- < ?php elseif ($this->input->get("login_attempt") == md5(2)): ?> -->
                             <!-- < ?= "<h5 class='text-danger'>User is already login.</h5>" ?> -->
                    <?php endif ?>
                    <form action="request_login" role="form" id="form" class="m-t" method="post" accept-charset="utf-8">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username" name="username" required autofocus />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password" required />
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b btn-outline">Login</button>
                    </form>
                    <p class="m-t">
                        <small><strong>NOTE: <i class="text-danger">If you forget your password or username. Ask the assigned personnel.</i></strong></small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright PNP RCD PRO 13 <small>© <?= date("Y") ?></small>
            </div>
            <div class="col-md-6 text-right">
               Powered by: <strong>TEAM: FSUU FIGHTING</strong>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">

        <?php if ($this->session->login_alias != "Klaven Rey Matugas"): ?>
            // console.log("%c%s", "color: red; font-size: 50px; font-weight: 700; text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);", "STOP!");
            console.log("%c%s", "color: red; font-size: 50px; font-weight: bold; text-shadow: 1px 1px 0px black, 1px -1px 0px black, -1px 1px 0px black, -1px -1px 0px black;", "STOP!");
            console.log("%c%s", "color: #000; font-size: 25px;", "This is for entended developers! Your action is bieng recorded!");
            $("body").on("contextmenu", function(){
                return false;
            });        
        <?php endif ?>
    </script>
</body>
</html>
