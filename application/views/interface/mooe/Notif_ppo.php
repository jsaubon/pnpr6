<style>
    .ppo_get_notifs:hover,
    .active_chat {
        color: skyblue !important;
    }
    .chat-discussion .chat-message.right .message {
        margin-right: 0px !important;
    }

    .input-group input[type="file"] {
    display: none;
    }
    .custom-file-upload {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
    }
</style>

<div class="ibox float-e-margins" id="ibox_cdp_claim">
    <div class="row"> 
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <!-- <small class="pull-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small> -->
                     PPO Notification List
                    <a class="pull-right text-primary btnBack hide"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9 ">
                            <div class="chat-discussion">
                                <br><br><br>
                                <h1 class="text-center">PPO Notification List <br><br>
                                    <img alt="image" style="width: 100px;" src="<?php echo base_url('assets/custom/images/Philippine_National_Police_seal.svg.png') ?>">
                                </h1>
                            </div>
                        </div>
                        <!-- || ($this->session->login_role == 3 || $this->session->login_ppo_id != 0) -->
                        <?php if ($this->session->login_role == 2 || $this->session->login_role == 4 || $this->session->login_role == 1){ ?>
                            <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list"> 
                                        <?php foreach ($ppo as $key => $value): ?>
                                            <div class="chat-user"> 
                                                <div class="chat-user-name">
                                                    <a class="ppo_get_notifs" id="<?php echo $value['ppo_code'] ?>"><?php echo $value['place'] ?></a>
                                                </div>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else {
                        ?>
                            <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list"> 
                                        <div class="text-center">
                                            <br><br><br><br><br>
                                            <h3 class="text-center">PPO Notification List <br><br>
                                            <img alt="image" style="width: 100px;" src="<?php echo base_url('assets/custom/images/Philippine_National_Police_seal.svg.png') ?>">
                                            </h3>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                        <?php
                        } ?>
                    </div>
                    <?php if ($this->session->login_role == 2 || $this->session->login_role == 4 || $this->session->login_role == 1): ?>
                    <div class="row send_row hide">
                        <div class="col-lg-12"> 
                            <!-- <input  type="file" />  -->
                            <form id="notif_form" method="POST" action="Notif_ppo/sendPPONotif" enctype="multipart/form-data">
                            <div class="input-group input-group-sm">
                                
                                    <textarea name="notif_message" id="ppo_notif_message" style="border-radius: 0px;font-size:14px;resize: none;"  class="form-control message-input" placeholder="Enter message text"></textarea> 
                                    <input type="hidden" name="ppo_code" id="ppo_code_form">
                                    <span class="input-group-btn">  
                                         <label for="file-upload" class="custom-file-upload" style="height: 90px;margin-bottom: 0px;padding-top: 0px;padding-bottom: 0px">
                                            <i class="fa fa-paperclip" style="font-size: 20px;line-height: 90px;"></i>
                                        </label>
                                        <input name="attachment"  id="file-upload" style="border-radius: 0px;width: 50px;height: 90px;font-size: 17px" type="file" /> 
                                        <button id="btnSendPpoNotif" style="border-radius: 0px;width: 70px;height: 90px;font-size: 17px;margin-top: -13px" class="btn btn-primary" type="button"><i class="fa fa-paper-plane"></i></button> 
                                    </span>
                                
                                
                            </div>
                            </form>
                        </div>
                    </div>
                    <?php endif ?>
                        
                </div>
            </div>
        </div>
             
    </div>
</div> 
<script> 
$(document).ready(function(){
    var ppo_code_clicked = '';
    $('.ppo_get_notifs').on('click',function(){
        $('.send_row').removeClass('hide');
        var ppo_code = $(this).attr('id');
        ppo_code_clicked = ppo_code;
        $('.ppo_get_notifs').removeClass('active_chat');
        $(this).addClass('active_chat');

        getPPONotifs(ppo_code);                                
    });

    var login_role = '<?php echo $this->session->login_role ?>';
    if (login_role == 3) {
        var login_ppo_code = '<?php echo $this->session->login_ppo_code ?>';
        ppo_code_clicked = login_ppo_code;
        getPPONotifs(login_ppo_code);
        
    }

    $('.btnBack').on('click',function(){
        location.reload();                
    });

    var mnp_id_clicked =' ';
    function getPPONotifs(ppo_code)
    {
        $('.btnBack').removeClass('hide');
        $('.chat-discussion').empty();
        $.post("<?php echo base_url('mooe/Notif_ppo/getPPONotifs')?>",
            {ppo_code},
        function(data){
            var data = JSON.parse(data); 
            if (data.length != 0) {
                var user_id = '<?php echo $this->session->login_id ?>';
                $.each(data,function(key,value){
                    var sender_id = value.sender_id;
                    var forwarded_to_all = value.forwarded_to_all;
                    var btnForward = '';
                    if (forwarded_to_all == 1) {
                        forwarded_to_all = '<a class="btnForward" value="'+value.mnp_id+'">Forwarded</a> <br>';
                    } else {
                        btnForward = '<a class="btnForward pull-left" value="'+value.mnp_id+'">Forward</a><br>';
                        forwarded_to_all = '<a class="btnForwardToAll" value="'+value.mnp_id+'">Forward to All</a> <br>';
                    }
                    var attachment = value.attachment;

                    if (attachment != null) {
                        var attachment_url = '<?php echo base_url() ?>'+'assets/attachments/'+attachment;
                        var attachment_arr = attachment.split('_');
                        attachment = '<a  download href="'+attachment_url+'">'+attachment_arr[1]+'</a>';
                    } else {
                        attachment = '';
                    }

                    var notif = '';
                        notif = '<div class="chat-message">\
                                    <div class="message">\
                                        <span class="message-date pull-right"><i"> \
                                            '+btnForward+' \
                                            '+forwarded_to_all+'\
                                            <a class="btnDeleteMNP" value="'+value.mnp_id+'">Delete</a> \
                                        </span>\
                                        <span class="message-date"> Sent by '+value.fullname+' '+moment(value.date_sent).format('LLL')+' </span> <br>\
                                        <span class="message-content">\
                                       '+value.notif_message+'\
                                       <br>'+attachment+' \
                                        </span>\
                                    </div>\
                                </div>';
                        $('.chat-discussion').append(notif); 
                });

                $('.btnDeleteMNP').on('click',function(){
                    mnp_id_clicked = $(this).attr('value'); 
                    $.confirm({
                        icon: 'fa fa-warning',
                        title: 'Warning',
                        content: 'Are you sure you want to delete this?',
                        confirmButton: "<span class='fa fa-check'></span> Yes",
                        cancelButton: "<span class='fa fa-remove'></span> No",
                        confirmButtonClass: "btn btn-sm btn-default",
                        cancelButtonClass: "btn btn-sm btn-primary",
                        animation: 'rotateY',
                        closeAnimation: 'rotateY',
                        confirm: function() {
                            deleteMNP(mnp_id_clicked);
                        },
                        cancel: function() {}
                    });
                    
                    function deleteMNP(mnp_id)
                    {
                        $.post("<?php echo base_url('mooe/Notif_ppo/deleteMNP')?>",
                            {mnp_id},
                        function(data){
                            console.log(data);
                            getPPONotifs(ppo_code_clicked);
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                            swal("Connection Error",'error');
                        });
                    }
                });

                $('.btnForwardToAll').on('click',function(){
                    mnp_id_clicked = $(this).attr('value'); 
                   forwardToAllMPS(mnp_id_clicked,ppo_code_clicked);
                   function forwardToAllMPS(mnp_id,ppo_code) {
                        $.post("<?php echo base_url('mooe/Notif_ppo/forwardToAllMPS')?>",
                            {mnp_id,ppo_code},
                            function(data){
                                // console.log(data);
                                getPPONotifs(ppo_code_clicked);
                                getMPSByPpoCode(ppo_code_clicked,mnp_id_clicked);
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                        });
                   }
                });

                $('.btnForward').on('click',function(){
                    mnp_id_clicked = $(this).attr('value');
                   getMPSByPpoCode(ppo_code_clicked,mnp_id_clicked);
                });

                function getMPSByPpoCode(ppo_code,mnp_id)
                {
                    $('.users-list').empty();
                    $.post("<?php echo base_url('mooe/Notif_ppo/getMPSByPpoCode')?>",
                        {ppo_code,mnp_id},
                    function(data){
                        var data = JSON.parse(data);  
                        $.each(data,function(key,value){
                            // value.ppo_code + ' : '+value.area_code + ' - ' +
                            var mps = '<span style="color:red;">'+value.assigned_place+'</span>';
                            var button = '<a value="'+value.mps_id+'" class="btnForwardToMPS text-primary pull-right"><i  class="fa fa-mail-forward"></i></a>';
                            var label = '<small class="pull-right text-right">Date Forwarded<br>'+moment(value.forwarded_date).format('LLL')+'</small>';
                            if (value.mnm_id == null) {
                                var newMpsRow = '<div class="chat-user">\
                                                <div class="chat-user-name">\
                                                    '+mps+button+'\
                                                </div>\
                                            </div>'; 
                                 $('.users-list').append(newMpsRow);
                            } else {
                                var newMpsRow = '<div class="chat-user">\
                                                <div class="chat-user-name">\
                                                    '+mps+label+'\
                                                </div>\
                                            </div>'; 
                                 $('.users-list').append(newMpsRow);
                            }
                        
                           
                        });

                        $('.btnForwardToMPS').on('click',function(){
                           var mps_id = $(this).attr('value');
                           var mnp_id = mnp_id_clicked;
                           forwardToMPS(mps_id,mnp_id);

                           function forwardToMPS(mps_id,mnp_id)
                           {
                               $.post("<?php echo base_url('mooe/Notif_ppo/forwardToMPS')?>",
                                   {mps_id,mnp_id},
                               function(data){ 
                                getMPSByPpoCode(ppo_code_clicked,mnp_id_clicked);
                               }).fail(function(xhr){
                                   console.log(xhr.responseText);
                                   swal("Connection Error",'error');
                               });
                           }
                        });
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                        swal("Connection Error",'error');
                    });
                }
            } else {
                notif = '<h2 class="text-center">No Notification</h2>';
                        $('.chat-discussion').append(notif);
            }
            jQuery(".chat-discussion").scrollTop(jQuery(".chat-discussion")[0].scrollHeight);
        }).fail(function(xhr){
            console.log(xhr.responseText);
            swal("Connection Error",'error');
        });
    }

    $('#btnSendPpoNotif').on('click',function(){
        $('#ppo_code_form').val(ppo_code_clicked);
        var notif_message = $('#ppo_notif_message').val(); 
        if (notif_message != '') { 
           
            var form = $('#notif_form')[0];
            var data = new FormData(form);
            $("#btnSendPpoNotif").prop("disabled", true);
            sendPPONotif(data);

        }    
    });

    $('#ppo_notif_message').on("keypress", function(e) {
        if(e.which == 13) {
            e.preventDefault();
            if($(this).val() != "") {
                $('#ppo_code_form').val(ppo_code_clicked);
                var notif_message = $('#ppo_notif_message').val(); 
                if (notif_message != '') { 
                    var formData = new FormData($('#notif_form')[0]);
                    sendPPONotif(formData);
                } 
            }
        }
    });

    function sendPPONotif(data)
    {
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "Notif_ppo/sendPPONotif",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) { 
                console.log(data);
                getPPONotifs(ppo_code_clicked);
                $("#btnSendPpoNotif").prop("disabled", false);
                $('#ppo_notif_message').val(''); 
                $('#file-upload').val(null);

            },
            error: function (e) { 
                $("#btnSubmit").prop("disabled", false);

            }
        });
    }
});
</script>