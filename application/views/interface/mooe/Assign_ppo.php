<div class="ibox float-e-margins" id="ibox_cdp_claim">
    <div class="ibox-title">
        <div class="ibox-tools"> 
            <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
        </div>
    </div>
     <div class="ibox-content table-responsive ">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row"> 
            <div class="col-sm-12">
                <table class="table" id="tbl_users" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="30%">UNIT</th> 
                            <th width="30%">Assinged</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($ppo as $key => $value): ?>
                            <tr id="<?php echo $value['ppo_id'] ?>" >
                                <td><?php echo $value['ppo'] ?></td>
                                <td><a class="btn_assign_ppo" name="btn_assign_ppo">
                                    <?php if ($value['user_id'] != null) {
                                        echo $value['fullname'];
                                    } else {
                                        echo 'click to assign';
                                    } ?> 
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- 
<select name="sample" class="form-control select2">
    <option></option>
</select> -->
<script> 
$(document).ready(function(){

    // $("[name=sample]").select2({
    //     placeholder:"",
    //     allowClear: true,
    //     data:
    // })

    var tbl_users = $('#tbl_users').dataTable();

    $('#tbl_users').on('click','.btn_assign_ppo',function(){ 
        // console.log(user_id);    
        var a = $(this);
        if (a.find('select').length  == 0) {
            $.post("<?= base_url('mooe/Assign_ppo/get_user_select') ?>", function(data){
                var ppo_json = JSON.parse(data);
                var ppo_select = '<select class="form-control">\
                                    <option value="0">Select User</option>';
                $.each(ppo_json,function(key,value){
                    ppo_select += '<option value="'+value.user_id+'">'+value.fullname+'</option>';
                });
                ppo_select += '</select>';
                a.html(ppo_select);
                a.find('select').focus();
                var a_select = a.find('select');
                a_select.on('change',function(){
                    var a = $(this).closest('a');
                    var tr = a.closest('tr');
                    var user_id = $(this).val();
                    var ppo_id = tr.attr('id');
                    var ppo = $(this).find('option:selected').text();
                    tr.attr('value',ppo_id);
                    a.html(ppo); 
                    console.log(user_id+' '+ppo_id);       
                    update_user_ppo(ppo_id,user_id);                      
                });
            });
                
        }

        function update_user_ppo(ppo_id,user_id)
        {
            $.post("<?php echo base_url('mooe/Assign_ppo/update_user_ppo')?>",
                {user_id,ppo_id},
            function(data){
                console.log(data);
            }).fail(function(xhr){
                console.log(xhr.responseText);
                swal("Connection Error",'error');
            });
        }
            

    });
});
    
</script>