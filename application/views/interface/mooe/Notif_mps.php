<style>
    .chat-discussion .chat-message.right .message {
        margin-right: 0px !important;
    }
</style>

<div class="ibox float-e-margins" id="ibox_cdp_claim">
    <div class="row"> 
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <!-- <small class="pull-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small> -->
                         MPS Notification List
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="chat-discussion" id="div_mps_msg"> 
                                   <!--  < ?php foreach ($mps_notifs as $key => $value): ?>
                                        <div class="chat-message right">
                                            <div class="message">
                                                <span class="message-date"> 
                                                    Date Forwarded : < ?php echo $value['date'].' Forwarded by: '.$value['by'] ?> 

                                                </span> 
                                                <span class="message-content">
                                                    < ?php echo $value['notif_message'] ?>
                                                    <br>
                                                    <a onclick="delete_this_mgs(< ?php echo $value['delete'] ?>)"><i class="fa fa-remove text-danger"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                    < ?php endforeach ?> -->
                                         
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
             
        </div>
</div> 

<script type="text/javascript">
    
    function delete_this_mgs(value){
        $.confirm({
            icon: 'fa fa-warning',
            title: 'Warning',
            content: 'Are you sure you want to delete this?',
            confirmButton: "<span class='fa fa-check'></span> Yes",
            cancelButton: "<span class='fa fa-remove'></span> No",
            confirmButtonClass: "btn btn-sm btn-default",
            cancelButtonClass: "btn btn-sm btn-primary",
            animation: 'rotateY',
            closeAnimation: 'rotateY',
            confirm: function() {
                $.post("<?= base_url('mooe/Notif_mps/delete_this_mgs') ?>",
                    { value },
                    function(data){
                        var d = JSON.parse(data);
                        if(d.success == true) {
                            $.notiny({ text: d.msg, position: "right-top" });
                            get_mpd_msg();
                        } else {
                            $.notiny({ text: d.msg, position: "right-top" });
                        }
                    }
                );
            },
            cancel: function() {}
        });
    }

    get_mpd_msg();
    function get_mpd_msg(){
        $("#div_mps_msg").empty();
        $.post("<?= base_url('mooe/Notif_mps/getMPSNotifs') ?>",
            function(data){
                var d = JSON.parse(data);
                $.each(d, function(k,v){
                    $("#div_mps_msg").append(v.new_data);
                });
                jQuery(".chat-discussion").scrollTop(jQuery(".chat-discussion")[0].scrollHeight);
            }
        ).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

</script>