<div class="ibox float-e-margins" id="ibox_cdp_claim">
    <div class="ibox-title">
        <div class="ibox-tools"> 
            <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
        </div>
    </div>
     <div class="ibox-content table-responsive ">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row"> 
            <div class="col-sm-12">
                <table class="table" id="tbl_users" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="30%">MPS</th> 
                            <th width="30%">Assigned</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($mps as $key => $value): ?>
                            <tr id="<?php echo $value['mps_id'] ?>" >
                                <td><?php echo $value['mps'] ?></td>
                                <td><a class="btn_assign_mps" name="btn_assign_mps">
                                    <?php if ($value['user_id'] != null) {
                                        echo $value['fullname'];
                                    } else {
                                        echo 'click to assign';
                                    } ?> 
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- 
<select name="sample" class="form-control select2">
    <option></option>
</select> -->
<script> 
$(document).ready(function(){

    // $("[name=sample]").select2({
    //     placeholder:"",
    //     allowClear: true,
    //     data:
    // })

    var tbl_users = $('#tbl_users').DataTable();


    $('#tbl_users').on('click','.btn_assign_mps',function(){ 
        // console.log(user_id);    
        var a = $(this);
        if (a.find('select').length  == 0) {
            $.post("<?= base_url('mooe/Assign_mps/get_user_select') ?>", function(data){
                var mps_json = JSON.parse(data);
                var mps_select = '<select class="form-control">\
                                    <option value="0">Select User</option>';
                $.each(mps_json,function(key,value){
                    mps_select += '<option value="'+value.user_id+'">'+value.fullname+'</option>';
                });
                mps_select += '</select>';
                a.html(mps_select);
                a.find('select').focus();
                var a_select = a.find('select');
                a_select.on('change',function(){
                    var a = $(this).closest('a');
                    var tr = a.closest('tr');
                    var user_id = $(this).val();
                    var mps_id = tr.attr('id');
                    var mps = $(this).find('option:selected').text();
                    tr.attr('value',mps_id);
                    a.html(mps);        
                    update_user_mps(user_id,mps_id);                      
                });
            });
                
        }

        function update_user_mps(user_id,mps_id)
        {
            $.post("<?php echo base_url('mooe/Assign_mps/update_user_mps')?>",
                {user_id,mps_id},
            function(data){
                console.log(data);
            }).fail(function(xhr){
                console.log(xhr.responseText);
                swal("Connection Error",'error');
            });
        }
            

    });
});
    
</script>