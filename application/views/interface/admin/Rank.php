<div class="ibox float-e-margins" id="ibox_rank">
    <div class="ibox-title">
        <h5>Ranked <small>List</small></h5>
        <div class="ibox-tools">
            <button onclick="clear_modal('form_rank')" name="btn_add" href="#modal_rank" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Ranked</button>

            <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
        </div>
    </div>
    <div class="ibox-content table-responsive sk-loading">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <table class="table" id="tbl_rank" style="width: 100%;">
            <thead>
                <tr>
                    <th>Ranked Name</th>
                    <th><div class='text-center'>Action</div></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

    </div>
</div>

<div class="modal inmodal" id="modal_rank" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_rank'), 'id=form_rank'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-star-o modal-icon"></i>
                    <h4 class="modal-title">Ranked <small>Form</small></h4>
                    <!-- <small class="font-bold">Form Ranked</small> -->
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Ranked Name</label>
                        <input type="hidden" name="rank_id">
                        <input type="text" name="rank_name" class="form-control" required></div>         
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="rank_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    var tbl_rank, tbl_rank_data;
    
    $(function(){

        tbl_rank = $("#tbl_rank").DataTable({
            ajax: {
                url: "<?= base_url('get_ranks') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_rank").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_rank_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        var rank_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#rank_btnSave").attr("disabled", true);
                $("#rank_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#rank_btnSave").attr("disabled", false);
                    $("#rank_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    $("#ibox_rank").children(".ibox-content").toggleClass("sk-loading");
                    tbl_rank.ajax.reload();
                } else {
                    $("#rank_btnSave").attr("disabled", false);
                    $("#rank_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_rank").ajaxForm(rank_form_options);

    });

</script>