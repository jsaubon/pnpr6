<?php if ($this->session->login_alias != "Klaven Rey Matugas"): ?><?php redirect('dashboard'); ?><?php endif ?>
<style type="text/css">
	input[type="checkbox"] {
    	display: none;
	}

	input[type="checkbox"] + label {
	    color: #f2f2f2;
	}

	input[type="checkbox"] + label span {
	    display:inline-block;
	    width: 19px;
	    height: 19px;
	    margin: -2px 10px 0 0;
	    vertical-align: middle;
	    background: url(<?= base_url() ?>assets/img/check_radio_sheet.png) left top no-repeat;
	    cursor:pointer;
	}

	input[type="checkbox"]:checked + label span {
	    background: url(<?= base_url() ?>assets/img/check_radio_sheet.png) -19px top no-repeat;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="form-group">
					<label>Username</label>
					<form id="form_user">
						<div style="position: relative; height: 30px; width: 300px;">
				            <input type="text" name="user_id-s" placeholder="Search" class="form-control input-sm ac-s" required style="position: absolute; z-index: 2; background: transparent;">
				            <input type="text" id="user_id-x" class="form-control input-sm ac-x" disabled style="color: #CCC; position: absolute; background: transparent; z-index: 1;">
				            <input type="hidden" name="user_id">
				        </div>
			        </form>
				</div>
			</div>
		</div>
		<br>
		<div class="tabs-container">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1">PNPR6</a></li>
				<!-- <li><a data-toggle="tab" href="#tab-2">Asset-Tracking</a></li>
				<li><a data-toggle="tab" href="#tab-3">Human Resource</a></li>
				<li><a data-toggle="tab" href="#tab-4">Accounting</a></li> -->
			</ul>
			<div class="tab-content">
				<div id="tab-1" class="tab-pane active">
					<div class="ibox float-e-margins" id="ibox_tab_1">
						<div class="ibox-title"></div>
						<div class="ibox-content sk-loading">
							<div class="sk-spinner sk-spinner-wave">
				                <div class="sk-rect1"></div>
				                <div class="sk-rect2"></div>
				                <div class="sk-rect3"></div>
				                <div class="sk-rect4"></div>
				                <div class="sk-rect5"></div>
				            </div>
							<button href='#modal_module' data-toggle='modal' name='btn_add' onclick="get_system_mods(1)" class='btn btn-sm btn-primary'><span class='fa fa-puzzle-piece'></span> Modules</button>
							<table class="table table-striped table-hover table-bordered" id="tbl_permission_mod1" style="font-size: 12px;">
					        	<thead>
					        		<th>Module</th>
					        		<th>Permission</th>
					        	</thead>
					        	<tbody></tbody>
					        </table>
						</div>
					</div>
				</div>

				<div id="tab-2" class="tab-pane">
					<div class="ibox float-e-margins" id="ibox_tab_2">
						<div class="ibox-title"></div>
						<div class="ibox-content sk-loading">
							<div class="sk-spinner sk-spinner-wave">
				                <div class="sk-rect1"></div>
				                <div class="sk-rect2"></div>
				                <div class="sk-rect3"></div>
				                <div class="sk-rect4"></div>
				                <div class="sk-rect5"></div>
				            </div>
							<button href='#modal_module' data-toggle='modal' name='btn_add' onclick="get_system_mods(2)" class='btn btn-sm btn-primary'><span class='fa fa-puzzle-piece'></span> Modules</button>
							<table class="table table-striped table-hover table-bordered" id="tbl_permission_mod2" style="font-size: 12px;">
					        	<thead>
					        		<th>Module</th>
					        		<th>Permission</th>
					        	</thead>
					        	<tbody></tbody>
					        </table>
					    </div>
					</div>
				</div>

				<div id="tab-3" class="tab-pane">
					<div class="ibox float-e-margins" id="ibox_tab_3">
						<div class="ibox-title"></div>
						<div class="ibox-content sk-loading">
							<div class="sk-spinner sk-spinner-wave">
				                <div class="sk-rect1"></div>
				                <div class="sk-rect2"></div>
				                <div class="sk-rect3"></div>
				                <div class="sk-rect4"></div>
				                <div class="sk-rect5"></div>
				            </div>
							<button href='#modal_module' data-toggle='modal' name='btn_add' onclick="get_system_mods(3)" class='btn btn-sm btn-primary'><span class='fa fa-puzzle-piece'></span> Modules</button>
							<table class="table table-striped table-hover table-bordered" id="tbl_permission_mod3" style="font-size: 12px;">
					        	<thead>
					        		<th>Module</th>
					        		<th>Permission</th>
					        	</thead>
					        	<tbody></tbody>
					        </table>
					    </div>
					</div>
				</div>

				<div id="tab-4" class="tab-pane">
					<div class="ibox float-e-margins" id="ibox_tab_4">
						<div class="ibox-title"></div>
						<div class="ibox-content sk-loading">
							<div class="sk-spinner sk-spinner-wave">
				                <div class="sk-rect1"></div>
				                <div class="sk-rect2"></div>
				                <div class="sk-rect3"></div>
				                <div class="sk-rect4"></div>
				                <div class="sk-rect5"></div>
				            </div>
							<button href='#modal_module' data-toggle='modal' name='btn_add' onclick="get_system_mods(4)" class='btn btn-sm btn-primary'><span class='fa fa-puzzle-piece'></span> Modules</button>
							<table class="table table-striped table-hover table-bordered" id="tbl_permission_mod4" style="font-size: 12px;">
					        	<thead>
					        		<th>Module</th>
					        		<th>Permission</th>
					        	</thead>
					        	<tbody></tbody>
					        </table>
					    </div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">

	var tbl_permission_mod1;
	var tbl_permission_mod1_data;

	var tbl_permission_mod2;
	var tbl_permission_mod2_data;

	var tbl_permission_mod3;
	var tbl_permission_mod3_data;

	var tbl_permission_mod4;
	var tbl_permission_mod4_data;

	$(function() {

		// $("[name=btn_add]").hide();

		$('[name=user_id-s]').autocomplete({
	    	lookup: <?= $users ?>,
	    	autoSelectFirst: true,
	    	showNoSuggestionNotice: true,
	    	noSuggestionNotice: 'No matching results',
	    	onHint: function (hint) {
	            $('#user_id-x').val(hint);
	        },
	        onSelect: function(suggestion) {
	        	$("[name=user_id]").val(suggestion.id);
	        	tbl_permission_mod1_data = $("#form_user").serializeArray();
	        	tbl_permission_mod1.ajax.reload();

	        	tbl_permission_mod2_data = $("#form_user").serializeArray();
	        	tbl_permission_mod2.ajax.reload();

	        	tbl_permission_mod3_data = $("#form_user").serializeArray();
	        	tbl_permission_mod3.ajax.reload();

	        	tbl_permission_mod4_data = $("#form_user").serializeArray();
	        	tbl_permission_mod4.ajax.reload();
	        },
	        onSearchStart: function() {
	        	$("[name=user_id]").val("");
	        	tbl_permission_mod1_data = $("#form_user").serializeArray();
	        	tbl_permission_mod1.ajax.reload();

	        	tbl_permission_mod2_data = $("#form_user").serializeArray();
	        	tbl_permission_mod2.ajax.reload();

	        	tbl_permission_mod3_data = $("#form_user").serializeArray();
	        	tbl_permission_mod3.ajax.reload();

	        	tbl_permission_mod4_data = $("#form_user").serializeArray();
	        	tbl_permission_mod4.ajax.reload();
	        }
		});

		$("[name=user_id-s]").on("change keyup", function() {
			if($(this).val() == "") {
				$("[name=user_id]").val();

				tbl_permission_mod1_data = $("#form_user").serializeArray();
	        	tbl_permission_mod1.ajax.reload();

	        	tbl_permission_mod2_data = $("#form_user").serializeArray();
	        	tbl_permission_mod2.ajax.reload();

	        	tbl_permission_mod3_data = $("#form_user").serializeArray();
	        	tbl_permission_mod3.ajax.reload();

	        	tbl_permission_mod4_data = $("#form_user").serializeArray();
	        	tbl_permission_mod4.ajax.reload();
			}
		});

		tbl_permission_mod1 = $("#tbl_permission_mod1").DataTable({
			dom: "ft",
			sort: false,
			bPaginate: false,
			ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod1') ?>",
                type: "POST",
                data: function() {
                	$("#ibox_tab_1").children(".ibox-content").toggleClass("sk-loading");
                  	return tbl_permission_mod1_data;
                	$("#ibox_tab_1").children(".ibox-content").toggleClass("sk-loading");
                }
            }
		});

		tbl_permission_mod2 = $("#tbl_permission_mod2").DataTable({
			dom: "ft",
			sort: false,
			bPaginate: false,
			ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod2') ?>",
                type: "POST",
                data: function() {
                	$("#ibox_tab_2").children(".ibox-content").toggleClass("sk-loading");
                  	return tbl_permission_mod2_data;
                	$("#ibox_tab_2").children(".ibox-content").toggleClass("sk-loading");
                }
            }
		});

		tbl_permission_mod3 = $("#tbl_permission_mod3").DataTable({
			dom: "ft",
			sort: false,
			bPaginate: false,
			ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod3') ?>",
                type: "POST",
                data: function() {
                	$("#ibox_tab_3").children(".ibox-content").toggleClass("sk-loading");
                  	return tbl_permission_mod3_data;
                	$("#ibox_tab_3").children(".ibox-content").toggleClass("sk-loading");
                }
            }
		});

		tbl_permission_mod4 = $("#tbl_permission_mod4").DataTable({
			dom: "ft",
			sort: false,
			bPaginate: false,
			ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod4') ?>",
                type: "POST",
                data: function() {
                	$("#ibox_tab_4").children(".ibox-content").toggleClass("sk-loading");
                  	return tbl_permission_mod4_data;
                	$("#ibox_tab_4").children(".ibox-content").toggleClass("sk-loading");
                }
            }
		});

	});

	function change_status(elem, user_id, mod_button_id) {
		var status = $(elem).prop("checked");
		if(status == true) {
			status = 1;
		} else {
			status = 0;
		}
		$.post("<?= base_url('admin/Permission/change_status') ?>",
				{ status: status, user_id: user_id, mod_button_id: mod_button_id },
				function() {}
			);		
	}


	function change_status_all(elem, user_id, module_id) {
		var status = $(elem).prop("checked");
		if(status == true) {
			status = 1;
		} else {
			status = 0;
		}
		$.post("<?= base_url('admin/Permission/change_status_all') ?>",
				{ status: status, user_id: user_id, module_id: module_id },
				function() {
					$("#ibox_tab_1").children(".ibox-content").toggleClass("sk-loading");
					$("#ibox_tab_2").children(".ibox-content").toggleClass("sk-loading");
					$("#ibox_tab_3").children(".ibox-content").toggleClass("sk-loading");
					$("#ibox_tab_4").children(".ibox-content").toggleClass("sk-loading");
					tbl_permission_mod1_data = $("#form_user").serializeArray();
	        		tbl_permission_mod1.ajax.reload();

	        		tbl_permission_mod2_data = $("#form_user").serializeArray();
		        	tbl_permission_mod2.ajax.reload();

		        	tbl_permission_mod3_data = $("#form_user").serializeArray();
		        	tbl_permission_mod3.ajax.reload();

		        	tbl_permission_mod4_data = $("#form_user").serializeArray();
		        	tbl_permission_mod4.ajax.reload();
				}
			);		
	}

</script>


<style type="text/css">
	#modal_module input[type=text] {
		font-size: 12px;
	}
</style>
<div class="modal fade" id="modal_module" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5>Module <small>List</small></h5>
			</div>
			<div class="modal-body">
				<table class="table table-striped" id="tbl_module" style="font-size: 12px;">
					<thead>
						<th>Module</th>
						<th></th>
						<th>Buttons</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<?= form_open(base_url('admin/Permission/insert_module'), ["class" => "form-inline", "id" => "form_module"]); ?>
					<div class="form-group">
						<input type="hidden" name="system_id">
						<input type="text" placeholder="Module Name" name="module_name" class="form-control input-sm" required>
						<button class="btn btn-sm btn-primary" id="mod_btnAdd">Add Module</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	var tbl_module;
	var tbl_module_data;

	$(function() {

		tbl_module = $("#tbl_module").DataTable({
			dom: "t",
			sort: false,
			bPaginate: false,
			ajax: {
                url: "<?= base_url('admin/Permission/get_modules') ?>",
                type: "POST",
                data: function() {
                  	return tbl_module_data;
                }
            }
		});

		var mod_form_options = {
            clearForm: false,
            resetForm: true,
            beforeSubmit: function() {
        		$("#mod_btnAdd").attr("disabled", true);
            	$("#mod_btnAdd").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
  					tbl_module.ajax.reload();
  					tbl_permission_mod1.ajax.reload();
  					tbl_permission_mod2.ajax.reload();
  					tbl_permission_mod3.ajax.reload();
  					tbl_permission_mod4.ajax.reload();
                }
                $.notiny({ text: d.msg, position: "right-top" });
                $("#mod_btnAdd").attr("disabled", false);
                $("#mod_btnAdd").html('Add Module');
            }
        };
        $("#form_module").ajaxForm(mod_form_options);

	});

	function get_system_mods(id) {
		$("[name=system_id]").val(id);
		tbl_module_data = [{name: "system_id", value: id}];
		tbl_module.ajax.reload();
	}

	function edit_module(elem, id) {
		var module_name = $(elem).closest("tr").find(".module_name").val();
		if(module_name != "") {
			$.post("<?= base_url('admin/Permission/insert_module') ?>",
					{ module_id: id, module_name: module_name },
					function(data) {
						var d = JSON.parse(data);
		                if(d.success == true) {
		  					tbl_module.ajax.reload();
		  					tbl_permission_mod1.ajax.reload();
		  					tbl_permission_mod2.ajax.reload();
		  					tbl_permission_mod3.ajax.reload();
		  					tbl_permission_mod4.ajax.reload();
		                }
		                $.notiny({ text: d.msg, position: "right-top" });
					}
				);
		}
	}

	function add_button(elem, module_id) {
		var button_name = $(elem).closest(".form_mod_btn").find(".button_name").val();
		var button_code = $(elem).closest(".form_mod_btn").find(".button_code").val();
		if(button_name != "" && button_code != "") {
			$.post("<?= base_url('admin/Permission/insert_module_btn') ?>",
					{ module_id: module_id, button_name: button_name, button_code: button_code },
					function(data) {
						var d = JSON.parse(data);
		                if(d.success == true) {
		  					tbl_module.ajax.reload();
		  					tbl_permission_mod1.ajax.reload();
		  					tbl_permission_mod2.ajax.reload();
		  					tbl_permission_mod3.ajax.reload();
		  					tbl_permission_mod4.ajax.reload();
		                }
		                $.notiny({ text: d.msg, position: "right-top" });
					}
				);
		}
	}

	function add_button_enter(elem, module_id) {
		var keypressed = event.keyCode || event.which;
    	if(keypressed == 13) {
			var button_name = $(elem).closest(".form_mod_btn").find(".button_name").val();
			var button_code = $(elem).closest(".form_mod_btn").find(".button_code").val();
			if(button_name != "" && button_code != "") {
				$.post("<?= base_url('admin/Permission/insert_module_btn') ?>",
						{ module_id: module_id, button_name: button_name, button_code: button_code },
						function(data) {
							var d = JSON.parse(data);
			                if(d.success == true) {
			  					tbl_module.ajax.reload();
			  					tbl_permission_mod1.ajax.reload();
			  					tbl_permission_mod2.ajax.reload();
			  					tbl_permission_mod3.ajax.reload();
			  					tbl_permission_mod4.ajax.reload();
			                }
			                $.notiny({ text: d.msg, position: "right-top" });
						}
					);
			}
		}
	}

	function edit_mod_button(elem, id) {
		var button_name = $(elem).closest("tr").find(".button_name").val();
		var button_code = $(elem).closest("tr").find(".button_code").val();
		if(button_name != "" && button_code != "") {
			$.post("<?= base_url('admin/Permission/insert_module_btn') ?>",
					{ mod_button_id: id, button_name: button_name, button_code: button_code },
					function(data) {
						var d = JSON.parse(data);
		                if(d.success == true) {
		  					tbl_module.ajax.reload();
		  					tbl_permission_mod1.ajax.reload();
		  					tbl_permission_mod2.ajax.reload();
		  					tbl_permission_mod3.ajax.reload();
		  					tbl_permission_mod4.ajax.reload();
		                }
		                $.notiny({ text: d.msg, position: "right-top" });
					}
				);
		}
	}

</script>