<div class="ibox float-e-margins" id="ibox_user">
    <div class="ibox-title">
        <h5>User <small>List</small></h5>
        <div class="ibox-tools">
            <a href="<?= base_url() ?>assets/downloadable/UserFormat.xlsx" target="new" style="margin-right: 7px;" class="btn btn-white btn-xs" download><strong class="text-danger"><i class="fa fa-file-excel-o"></i> Downloadable Excel File Format</strong></a>

            <button name="btn_advance_filter" data-toggle="modal" class="btn btn-xs btn-default" id="btn_advance_filter" onclick="$('#div_advance_filter').slideToggle()" style="margin-right:7px;">
                <strong><span class="fa fa-filter"></span> Advance Filter</strong>
            </button>

            <button name="btn_add" href="#modal_multiple_user" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Data From Spreadsheet</button>

            <button onclick="clear_modal('form_user')" name="btn_add" href="#modal_user" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add User</button>

            <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
        </div>
    </div>
    <div class="ibox-content table-responsive sk-loading">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row">
            <div class="col-sm-12" id="div_advance_filter" style="display: none;">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <form id="form_user_filter">
                            <div class="col-sm-4">
                                <label for="f_user_role">User Role</label>
                                <select name="f_user_role_id" class="form-control filter" id="f_user_role" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="f_ranked">Ranked</label>
                                <select name="f_rank_id" class="form-control filter" id="f_ranked" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label for="f_account_number">Account Number</label>
                                <select name="f_account_number" class="form-control filter" id="f_account_number" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </form>
                    </div>  
                </div>
            </div>
            <div class="col-sm-12">
                <table class="table table-hover" id="tbl_user" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Fullname</th>
                            <th>Account</th>
                            <th>User Role</th>
                            <th>Ranked</th>
                            <th>Unit</th>
                            <th>Status</th>
                            <th><div class='text-center'>Action</div></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_user" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_user'), 'id="form_user" class="form-horizontal"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-user modal-icon"></i>
                    <h4 class="modal-title">User <small class="font-bold">Form</small></h4>                    
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username" class="control-label col-sm-3">Username: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="hidden" name="user_id">
                            <input type="text" name="username" class="form-control" id="username" required>
                        </div>
                    </div>
                    <!-- Username -->
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-3">Password:</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                    </div>    
                    <!-- Password -->
                    <div class="form-group">
                        <label for="firstname" class="control-label col-sm-3">Firstname: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="firstname" class="form-control" id="firstname" required>
                        </div>
                    </div> 
                    <!-- Firstname -->
                    <div class="form-group">
                        <label for="middlename" class="control-label col-sm-3">Middlename:</label>
                        <div class="col-sm-9">
                            <input type="text" name="middlename" class="form-control" id="middlename">
                        </div>
                    </div>    
                    <!-- Middlename -->
                    <div class="form-group">
                        <label for="lastname" class="control-label col-sm-3">Lastname: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="lastname" class="form-control" id="lastname" required>
                        </div>
                    </div>    
                    <!-- Lastname -->
                    <div class="form-group">
                        <label for="name_ext" class="control-label col-sm-3">Name Extension:</label>
                        <div class="col-sm-9">
                            <input type="text" name="name_ext" class="form-control" id="name_ext">
                        </div>
                    </div> 
                    <!-- Name Extension -->
                    <div class="form-group">
                        <label for="account_number" class="control-label col-sm-3">Account Number:</label>
                        <div class="col-sm-9">
                            <input type="text" name="account_number" class="form-control" id="account_number">
                        </div>
                    </div>    
                    <!-- Account Number -->
                    <div class="form-group">
                        <label for="atm_number" class="control-label col-sm-3">ATM Number:</label>
                        <div class="col-sm-9">
                            <input type="text" name="atm_number" class="form-control" id="atm_number">
                        </div>
                    </div>    
                    <!-- ATM Number -->
                    <div class="form-group">
                        <label for="tin_number" class="control-label col-sm-3">TIN Number:</label>
                        <div class="col-sm-9">
                            <input type="text" name="tin_number" class="form-control" id="tin_number">
                        </div>
                    </div>    
                    <!-- TIN Number -->
                    <div class="form-group">
                        <label for="rank_id" class="control-label col-sm-3">Ranked: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="rank_id" class="form-control select2" id="rank_id" required style="width: 100%;">
                                <option></option>
                            </select>
                        </div>
                    </div> 
                    <!-- Ranked -->
                    <div class="form-group">
                        <label for="user_role_id" class="control-label col-sm-3">User Role: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="user_role_id" class="form-control select2" id="user_role_id" style="width: 100%;" required>
                                <option></option>
                            </select>
                        </div>
                    </div> 
                    <!-- User Role -->
                    <div class="form-group">
                        <label for="unit_id" class="control-label col-sm-3">Unit: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="unit_id" class="form-control select2" id="unit_id" style="width: 100%;" required>
                                <option></option>
                            </select>
                        </div>
                    </div> 
                    <!-- Unit -->
                    <div class="form-group">
                        <label for="status" class="control-label col-sm-3">Status: <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="status" class="form-control" id="status" required>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>
                    </div>   
                    <!-- Status -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="user_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_multiple_user" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_multiple_user'), 'id=form_multiple_user'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">Users</h4>
                    <small class="font-bold"><i class="fa fa-upload"></i> Upload Data From Spreadsheet</small>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="alert">
                        <label for="">Spreadsheet File</label>
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="file" required>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <span id="span_warning_msg" class="text-center" style="display: none;"><strong>Note: <span class="text-danger">Please do not close the form or refresh the page.</span></strong></span>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="multiple_user_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var tbl_user, tbl_user_data;

    $(function(){

        $("[name=f_user_role_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $user_roles; ?>
        });
        $("[name=f_rank_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $ranks; ?>
        });
        $("[name=f_account_number]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $account_numbers; ?>
        });
        $(".filter").on("change", function() {
            tbl_user_data = $("#form_user_filter").serializeArray();
            $("#ibox_user").children(".ibox-content").toggleClass("sk-loading");
            tbl_user.ajax.reload();
        });

        $("[name=rank_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $ranks; ?>
        });
        $("[name=unit_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $units; ?>
        });
        $("[name=user_role_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $user_roles; ?>
        });

        var user_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#user_btnSave").attr("disabled", true);
                $("#user_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#user_btnSave").attr("disabled", false);
                    $("#user_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    $("#ibox_user").children(".ibox-content").toggleClass("sk-loading");
                    tbl_user.ajax.reload();
                } else {
                    $("#user_btnSave").attr("disabled", false);
                    $("#user_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_user").ajaxForm(user_form_options);

        $("#form_multiple_user [name=file]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'xls':
                case 'xlsx':
                case 'xml':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var multiple_user_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#multiple_user_btnSave").attr("disabled", true);
                $("#multiple_user_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
                $("#span_warning_msg").show('1000');
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_user").children(".ibox-content").toggleClass("sk-loading");
                    $("#multiple_user_btnSave").attr("disabled", false);
                    $("#multiple_user_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_user.ajax.reload();
                    location.replace("<?= base_url('admin/user') ?>");                    
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_multiple_user [name=alert]").val("ok");
                            $("#form_multiple_user").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#multiple_user_btnSave").attr("disabled", false);
                            $("#multiple_user_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#multiple_user_btnSave").attr("disabled", false);
                    $("#multiple_user_btnSave").html("<span class=\"fa fa-repeat\"></span> Try Again");
                    swal({
                        html:   true,
                        title:  "Warning",
                        text:   d.msg,
                        type:   "warning"
                    });
                }
            }
        };
        $("#form_multiple_user").ajaxForm(multiple_user_form_options);

        tbl_user = $("#tbl_user").DataTable({
            ajax: {
                url: "<?= base_url('get_users') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_user").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_user_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

    });

</script>


<style type="text/css">
    input[type="checkbox"] {
        display: none;
    }

    input[type="checkbox"] + label {
        color: #f2f2f2;
    }

    input[type="checkbox"] + label span {
        display:inline-block;
        width: 19px;
        height: 19px;
        margin: -2px 10px 0 0;
        vertical-align: middle;
        background: url(<?= base_url() ?>assets/img/check_radio_sheet.png) left top no-repeat;
        cursor:pointer;
    }

    input[type="checkbox"]:checked + label span {
        background: url(<?= base_url() ?>assets/img/check_radio_sheet.png) -19px top no-repeat;
    }
</style>

<div class="modal fade" id="modal_permission" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5>Permission <small>Setting</small></h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#permission-tab-1">RCD PRO 13</a></li>
                            <!-- <li><a data-toggle="tab" href="#permission-tab-2">Asset-Tracking</a></li>
                            <li><a data-toggle="tab" href="#permission-tab-3">Human Resource</a></li>
                            <li><a data-toggle="tab" href="#permission-tab-4">Accounting</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div id="permission-tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <table class="table table-striped table-hover table-bordered" id="tbl_permission_mod1" style="font-size: 12px; width: 100%;">
                                        <thead>
                                            <th>Module</th>
                                            <th>
                                                Permission
                                                <div class="hr-line-dashed" style="margin: 5px 0px;"></div>
                                                <input type="checkbox" name="super_select_all" id="super_select_all" class="super_select_all"><label for="super_select_all"><span></span></label>
                                                Super Select All
                                            </th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="permission-tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <table class="table table-striped table-hover table-bordered" id="tbl_permission_mod2" style="font-size: 12px;">
                                        <thead>
                                            <th>Module</th>
                                            <th>Permission</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="permission-tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <table class="table table-striped table-hover table-bordered" id="tbl_permission_mod3" style="font-size: 12px;">
                                        <thead>
                                            <th>Module</th>
                                            <th>Permission</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="permission-tab-4" class="tab-pane">
                                <div class="panel-body">
                                    <table class="table table-striped table-hover table-bordered" id="tbl_permission_mod4" style="font-size: 12px;">
                                        <thead>
                                            <th>Module</th>
                                            <th>Permission</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $("#super_select_all").on("click",function(){
        $("#permission-tab-1 .check").each(function(){
            $(this).click();
            $(this).prop("checked", "checked");
        });
    });

    var tbl_permission_mod1;
    var tbl_permission_mod1_data;

    var tbl_permission_mod2;
    var tbl_permission_mod2_data;

    var tbl_permission_mod3;
    var tbl_permission_mod3_data;

    var tbl_permission_mod4;
    var tbl_permission_mod4_data;

    $(function() {

        tbl_permission_mod1 = $("#tbl_permission_mod1").DataTable({
            dom: "ft",
            sort: false,
            bPaginate: false,
            ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod1') ?>",
                type: "POST",
                data: function() {
                  return tbl_permission_mod1_data;
                }
            }
        });

        tbl_permission_mod2 = $("#tbl_permission_mod2").DataTable({
            dom: "ft",
            sort: false,
            bPaginate: false,
            ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod2') ?>",
                type: "POST",
                data: function() {
                  return tbl_permission_mod2_data;
                }
            }
        });

        tbl_permission_mod3 = $("#tbl_permission_mod3").DataTable({
            dom: "ft",
            sort: false,
            bPaginate: false,
            ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod3') ?>",
                type: "POST",
                data: function() {
                  return tbl_permission_mod3_data;
                }
            }
        });

        tbl_permission_mod4 = $("#tbl_permission_mod4").DataTable({
            dom: "ft",
            sort: false,
            bPaginate: false,
            ajax: {
                url: "<?= base_url('admin/Permission/get_permission_mod4') ?>",
                type: "POST",
                data: function() {
                  return tbl_permission_mod4_data;
                }
            }
        });

    });

    $( "#modal_permission" ).on('shown.bs.modal', function(){
        var count = $("#permission-tab-1 #tbl_permission_mod1 .check").length;
        var count_checked = $("#permission-tab-1 #tbl_permission_mod1 .check:checked").length;
        var count_unchecked = $("#permission-tab-1 #tbl_permission_mod1 .check:not(:checked)").length;
        if (count_checked == count) {
            $("#permission-tab-1 #tbl_permission_mod1 #super_select_all").prop("checked", true);
        } else {
            $("#permission-tab-1 #tbl_permission_mod1 #super_select_all").prop("checked", false);
        }
    });

    function get_permission(id) {
        

        tbl_permission_mod1_data = [{ name: "user_id", value: id }];
        tbl_permission_mod1.ajax.reload();

        tbl_permission_mod2_data = [{ name: "user_id", value: id }];
        tbl_permission_mod2.ajax.reload();

        tbl_permission_mod3_data = [{ name: "user_id", value: id }];
        tbl_permission_mod3.ajax.reload();

        tbl_permission_mod4_data = [{ name: "user_id", value: id }];
        tbl_permission_mod4.ajax.reload();
    }

    function change_status(elem, user_id, mod_button_id) {
        var status = $(elem).prop("checked");
        if(status == true) {
            status = 1;
        } else {
            status = 0;
        }
        $.post("<?= base_url('admin/Permission/change_status') ?>",
                { status: status, user_id: user_id, mod_button_id: mod_button_id },
                function() {}
            );      
    }

    function change_status_all(elem, user_id, module_id) {
        var status = $(elem).prop("checked");
        if(status == true) {
            status = 1;
        } else {
            status = 0;
        }
        $.post("<?= base_url('admin/Permission/change_status_all') ?>",
                { status: status, user_id: user_id, module_id: module_id },
                function() {
                    tbl_permission_mod1_data = [{ name: "user_id", value: user_id }];
                    tbl_permission_mod1.ajax.reload();

                    tbl_permission_mod2_data = [{ name: "user_id", value: user_id }];
                    tbl_permission_mod2.ajax.reload();

                    tbl_permission_mod3_data = [{ name: "user_id", value: user_id }];
                    tbl_permission_mod3.ajax.reload();

                    tbl_permission_mod4_data = [{ name: "user_id", value: user_id }];
                    tbl_permission_mod4.ajax.reload();
                }
            );      
    }

</script>