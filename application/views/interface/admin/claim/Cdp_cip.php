<div class="tabs-container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab_cdp_claim"> CDP <small>List</small></a></li>
        <li class=""><a data-toggle="tab" href="#tab_cip_claim"> CIP <small>List</small></a></li>
    </ul>
    <div class="tab-content">

        <div id="tab_cdp_claim" class="tab-pane active">
            <div class="ibox float-e-margins" id="ibox_cdp_claim">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>assets/downloadable/CDPClaimsFormat.xlsx" target="new" style="margin-right: 7px;" class="btn btn-white btn-xs" download><strong class="text-danger"><i class="fa fa-file-excel-o"></i> CDP Downloadable Excel File Format</strong></a>

                        <button name="btn_advanced_filter" class="btn btn-xs btn-white" onclick="$('#form_advanced_filter_cdp').slideToggle()" style="margin-right:7px;"><span class="fa fa-filter"></span> <strong>Advanced Filter</strong></button>
                        
                        <button name="btn_add" href="#modal_cdp_claim_temp" data-toggle="modal" class="btn btn-xs btn-white hidden" style="margin-right:7px;"><span class="fa fa-recycle"></span> CDP Claim Temp <span class="text-danger" id="count_cdp_temp"></span></button>

                        <button name="btn_add" href="#modal_cdp_claim_credited" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Spreadsheet Claim <span class="text-danger"><strong>( Credited to ATM )</strong></span></button>

                        <button onclick="clear_modal('form_cdp_claim_single')" name="btn_add" href="#modal_cdp_claim_single" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Claim</button>

                        <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                 <div class="ibox-content table-responsive sk-loading">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" id="form_advanced_filter_cdp" style="display: none;">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="af_user_id">Fullname</label>
                                                    <select name="af_user_id" id="af_user_id" class="form-control cdp_af_filter" style="width: 100%;">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="af_date_paid">Date Paid</label>
                                                    <input type="date" name="af_date_paid" class="form-control cdp_af_filter" id="af_date_paid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12">
                            <table class="table" id="tbl_claim_cdp" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Amount</th>
                                        <th>Date Paid</th>
                                        <th>Status</th>
                                        <th><div class='text-center'>Action</div></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="tab_cip_claim" class="tab-pane">
            <div class="ibox float-e-margins" id="ibox_cip_claim">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>assets/downloadable/CIPClaimsFormat.xlsx" target="new" style="margin-right: 7px;" class="btn btn-white btn-xs" download><strong class="text-danger"><i class="fa fa-file-excel-o"></i> CIP Downloadable Excel File Format</strong></a>

                        <button name="btn_advanced_filter_cip" class="btn btn-xs btn-white" onclick="$('#form_advanced_filter_cip').slideToggle()" style="margin-right:7px;"><span class="fa fa-filter"></span> <strong>Advanced Filter</strong></button>
                        
                        <button name="btn_add" href="#modal_cip_claim_temp" data-toggle="modal" class="btn btn-xs btn-white hidden" style="margin-right:7px;"><span class="fa fa-recycle"></span> CIP Claim Temp <span class="text-danger" id="count_cip_temp"></span></button>

                        <button name="btn_add" href="#modal_cip_claim_credited" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Spreadsheet Claim <span class="text-danger"><strong>( Credited to ATM )</strong></span></button>

                        <button onclick="clear_modal('form_cip_claim_single')" name="btn_add" href="#modal_cip_claim_single" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Claim</button>

                        <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                 <div class="ibox-content table-responsive sk-loading">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" id="form_advanced_filter_cip" style="display: none;">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="af_user_id">Fullname</label>
                                                    <select name="af_user_id" id="af_user_id" class="form-control cip_af_filter" style="width: 100%;">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="af_date_paid">Date Paid</label>
                                                    <input type="date" name="cip_af_date_paid" class="form-control cip_af_filter" id="af_date_paid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12">
                            <table class="table" id="tbl_claim_cip" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Amount</th>
                                        <th>Date Paid</th>
                                        <th>Status</th>
                                        <th><div class='text-center'>Action</div></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Start Modal CDP -->

<div class="modal inmodal" id="modal_cdp_claim_single" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_cdp_claim_single'), 'id="form_cdp_claim_single"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-check modal-icon"></i>
                    <h4 class="modal-title">
                        CDP Claim <small class="font-bold">Form</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_id">Fullname <span class="text-danger">*</span></label>
                        <input type="hidden" name="claim_cdp_id">
                        <input type="hidden" name="alert" value="">
                        <select name="user_id" class="form-control select2" id="user_id" required style="width: 100%;">
                            <option></option>
                        </select>
                    </div>    
                    <div class="form-group">
                        <label for="date_paid">Date Paid <span class="text-danger">*</span></label>
                        <input type="date" name="date_paid" class="form-control" id="date_paid">
                    </div>    
                    <div class="form-group">
                        <label for="amount">Amount <span class="text-danger">*</span></label>
                        <input type="number" name="amount" class="form-control" id="amount" step="any" min="0">
                    </div>    
                    <div class="form-group">
                        <label for="status">Status <span class="text-danger">*</span></label>
                        <select name="status" class="form-control" id="status" required>  
                            <option value="0">Request for FUND</option>
                            <option value="1">Credited to ATM</option>
                        </select>
                    </div>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="single_cdp_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_cdp_claim_credited" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_cdp_claim_credited'), 'id="form_cdp_claim_credited"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Credited to ATM</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-upload"></i> Upload Spreadsheet</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" required />
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="credited_cdp_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- End Modal CDP -->

<script type="text/javascript">

    var tbl_claim_cdp, tbl_claim_cdp_data;
    
    $(function(){

        $("#form_advanced_filter_cdp [name=af_user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
        });

        $("#form_cdp_claim_single [name=user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
        });

        var single_cdp_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#single_cdp_claim_btnSave").attr("disabled", true);
                $("#single_cdp_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#single_cdp_claim_btnSave").attr("disabled", false);
                    $("#single_cdp_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim_cdp.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#single_cdp_claim_btnSave").attr("disabled", false);
                            $("#single_cdp_claim_btnSave").html("Save");
                            $("#form_cdp_claim_single [name=alert]").val("ok");
                            $("#form_cdp_claim_single").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#single_cdp_claim_btnSave").attr("disabled", false);
                            $("#single_cdp_claim_btnSave").html("Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#single_cdp_claim_btnSave").attr("disabled", false);
                    $("#single_cdp_claim_btnSave").html("Try Again");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_cdp_claim_single").ajaxForm(single_cdp_claim_form_options);

        $("#form_cdp_claim_credited [name=file]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'xls':
                case 'xlsx':
                case 'xml':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var credited_cdp_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#credited_cdp_claim_btnSave").attr("disabled", true);
                $("#credited_cdp_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#credited_cdp_claim_btnSave").attr("disabled", false);
                    $("#credited_cdp_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim_cdp.ajax.reload();
                    location.replace("<?= base_url('admin/claim/cdpcip') ?>");  
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_cdp_claim_credited [name=alert]").val("ok");
                            $("#form_cdp_claim_credited").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#credited_cdp_claim_btnSave").attr("disabled", false);
                            $("#credited_cdp_claim_btnSave").html("Save");
                            $(".close").click();
                        }
                    });
                } else if(d.success == false && d.wrong_file == true) {
                    swal({
                        title:  "Warning",
                        text:   "Your file uploaded is not allowed!",
                        type:   "warning"
                    });
                    $("#credited_cdp_claim_btnSave").attr("disabled", false);
                    $("#credited_cdp_claim_btnSave").html("Try Again");
                } else {
                    $("#credited_cdp_claim_btnSave").attr("disabled", false);
                    $("#credited_cdp_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_cdp_claim_credited").ajaxForm(credited_cdp_claim_form_options);

        $(".cdp_af_filter").on("change keyup", function() {
            tbl_claim_cdp_data = $("#form_advanced_filter_cdp").serializeArray();
            $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
            tbl_claim_cdp.ajax.reload(function() {
                check_user_permission();
            });
        });
        tbl_claim_cdp = $("#tbl_claim_cdp").DataTable({
            pageLength: 25,
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ "10", "25", "50", "100", "ALL" ]
            ],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                },
                {extend: 'csv',
                    title: 'Survey Report',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                },
                {extend: 'excel', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        },
                },
                {extend: 'pdf', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        },
                },
                {extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                    title: '<div class="text-center"><img alt="image" style="width: 100px;" src="<?= base_url() ?>assets/custom/images/Philippine_National_Police_seal.svg.png"/><br>PNPR6 Report</div>', // need to change this
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            ajax: {
                url: "<?= base_url('get_cdp_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_claim_cdp_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

    });

    function cdp_change_status(value){
        $.post("<?= base_url('cdp_change_status') ?>",
            { value },
            function(data){
                var d = JSON.parse(data);
                if (d.success == true) {
                    $("#ibox_cdp_claim").children(".ibox-content").toggleClass("sk-loading");
                    tbl_claim_cdp.ajax.reload();
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        );
    }

</script>


<!-- Start Modal CIP -->

<div class="modal inmodal" id="modal_cip_claim_single" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_cip_claim_single'), 'id="form_cip_claim_single"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-check modal-icon"></i>
                    <h4 class="modal-title">
                        CIP Claim <small class="font-bold">Form</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_id">Fullname <span class="text-danger">*</span></label>
                        <input type="hidden" name="claim_cip_id">
                        <input type="hidden" name="alert" value="">
                        <select name="user_id" class="form-control select2" id="user_id" required style="width: 100%;">
                            <option></option>
                        </select>
                    </div>    
                    <div class="form-group">
                        <label for="date_paid">Date Paid <span class="text-danger">*</span></label>
                        <input type="date" name="date_paid" class="form-control" id="date_paid">
                    </div>    
                    <div class="form-group">
                        <label for="amount">Amount <span class="text-danger">*</span></label>
                        <input type="number" name="amount" class="form-control" id="amount" step="any" min="0">
                    </div>    
                    <div class="form-group">
                        <label for="status">Status <span class="text-danger">*</span></label>
                        <select name="status" class="form-control" id="status" required>  
                            <option value="0">Request for FUND</option>
                            <option value="1">Credited to ATM</option>
                        </select>
                    </div>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="single_cip_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_cip_claim_credited" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_cip_claim_credited'), 'id="form_cip_claim_credited"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Credited to ATM</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-upload"></i> Upload Spreadsheet</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" required />
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="credited_cip_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- End Modal -->

<script type="text/javascript">

    var tbl_claim_cip, tbl_claim_cip_data;
    
    $(function(){

        $("#form_advanced_filter_cip [name=af_user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
        });

        $("#form_cip_claim_single [name=user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $users ?>
        });

        var single_cip_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#single_cip_claim_btnSave").attr("disabled", true);
                $("#single_cip_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#single_cip_claim_btnSave").attr("disabled", false);
                    $("#single_cip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim_cip.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_cip_claim_single [name=alert]").val("ok");
                            $("#form_cip_claim_single").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#single_cip_claim_btnSave").attr("disabled", false);
                            $("#single_cip_claim_btnSave").html("Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#single_cip_claim_btnSave").attr("disabled", false);
                    $("#single_cip_claim_btnSave").html("Try Again");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_cip_claim_single").ajaxForm(single_cip_claim_form_options);

        $("#form_cip_claim_credited [name=file]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'xls':
                case 'xlsx':
                case 'xml':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var credited_cip_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#credited_cip_claim_btnSave").attr("disabled", true);
                $("#credited_cip_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#credited_cip_claim_btnSave").attr("disabled", false);
                    $("#credited_cip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim_cip.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_cip_claim_credited [name=alert]").val("ok");
                            $("#form_cip_claim_credited").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#credited_cip_claim_btnSave").attr("disabled", false);
                            $("#credited_cip_claim_btnSave").html("Save");
                            $(".close").click();
                        }
                    });
                } else if(d.success == false && d.wrong_file == true) {
                    swal({
                        title:  "Warning",
                        text:   "Your file uploaded is not allowed!",
                        type:   "warning"
                    });
                    $("#credited_cip_claim_btnSave").attr("disabled", false);
                    $("#credited_cip_claim_btnSave").html("Try Again");
                } else {
                    $("#credited_cip_claim_btnSave").attr("disabled", false);
                    $("#credited_cip_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_cip_claim_credited").ajaxForm(credited_cip_claim_form_options);

        $(".cip_af_filter").on("change keyup", function() {
            tbl_claim_cip_data = $("#form_advanced_filter_cip").serializeArray();
            $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
            tbl_claim_cip.ajax.reload(function() {
                check_user_permission();
            });
        });
        tbl_claim_cip = $("#tbl_claim_cip").DataTable({
            pageLength: 25,
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ "10", "25", "50", "100", "ALL" ]
            ],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                },
                {extend: 'csv',
                    title: 'Survey Report',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                },
                {extend: 'excel', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        },
                },
                {extend: 'pdf', 
                        title: 'Survey Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        },
                },
                {extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    },
                    title: '<div class="text-center"><img alt="image" style="width: 100px;" src="<?= base_url() ?>assets/custom/images/Philippine_National_Police_seal.svg.png"/><br>PNPR6 Report</div>', // need to change this
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            ajax: {
                url: "<?= base_url('get_cip_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_claim_cip_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

    });

    function cip_change_status(value){
        $.post("<?= base_url('cip_change_status') ?>",
            { value },
            function(data){
                var d = JSON.parse(data);
                if (d.success == true) {
                    $("#ibox_cip_claim").children(".ibox-content").toggleClass("sk-loading");
                    tbl_claim_cip.ajax.reload();
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        );
    }

</script>