<div class="ibox float-e-margins" id="ibox_other_claim">
    <div class="ibox-title">
        <h5>Claim <small>List</small></h5>
        <div class="ibox-tools">
            <a href="<?= base_url() ?>assets/downloadable/OtherClaimsFormat.xlsx" target="new" style="margin-right: 7px;" class="btn btn-white btn-xs" download><strong class="text-danger"><i class="fa fa-file-excel-o"></i> Downloadable Excel File Format</strong></a>

            <button name="btn_advanced_filter" class="btn btn-xs btn-white" onclick="$('#form_advanced_filter').slideToggle()" style="margin-right:7px;"><span class="fa fa-filter"></span> <strong>Advanced Filter</strong></button>

            <button name="btn_add" href="#modal_other_claim_request_for_fund" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Spreadsheet Claim <span class="text-danger"><strong>( Request for FUND )</strong></span></button>

            <button name="btn_add" href="#modal_other_claim_credited" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-right:7px;"><span class="fa fa-upload"></span> Upload Spreadsheet Claim <span class="text-danger"><strong>( Credited to ATM )</strong></span></button>

            <button onclick="clear_modal('form_other_claim_single')" name="btn_add" href="#modal_other_claim_single" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Claim</button>

            <a class="btn btn-xs btn-primary fullscreen-link" title="Expand"><i class="fa fa-expand"></i></a>
        </div>
    </div>
    <div class="ibox-content table-responsive sk-loading">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row">
            <div class="col-sm-12" id="dv_claim_temp" style="display: none;">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Claim temporary
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table" id="tbl_claim_temp">
                            <thead>
                                <th>Name</th>
                                <th>Type of Claim</th>
                                <th>Period Covered</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th class="text-center">Actions</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <form role="form" id="form_advanced_filter" style="display: none;">
                    <div class="panel panel-info">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="af_user_id">Fullname</label>
                                        <select name="af_user_id" id="af_user_id" class="form-control af_filter" style="width: 100%;">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="af_status">Status</label>
                                        <select name="af_status" id="af_status" class="form-control af_filter">
                                            <option value="0">Request for FUND</option>
                                            <option value="1">Credited to ATM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="af_date_requested">Date Requested</label>
                                        <input type="date" name="af_date_requested" id="af_date_requested" class="form-control af_filter">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="af_date_approved">Date Approved</label>
                                        <input type="date" name="af_date_approved" id="af_date_approved" class="form-control af_filter">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-12">
                <table class="table" id="tbl_claim_other" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Account Number</th>
                            <th>Type of claim</th>
                            <th>Period Covered</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th><div class='text-center'>Action</div></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_other_claim_single" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_other_claim_single'), 'id="form_other_claim_single"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-check modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <small class="font-bold">Form</small>
                        <!-- <br> -->
                        <!-- <small class="font-bold">Single Add</small> -->
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_id">Fullname <span class="text-danger">*</span></label>
                        <input type="hidden" name="claim_other_id">
                        <input type="hidden" name="alert" value="">
                        <select name="user_id" class="form-control select2" id="user_id" required style="width: 100%;">
                            <option></option>
                        </select>
                    </div>    
                    <div class="form-group">
                        <label for="type_of_claim_id">Type of claim <span class="text-danger">*</span></label>
                        <select name="type_of_claim_id" class="form-control select2" id="type_of_claim_id" required style="width: 100%;">
                            <option></option>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="period_covered">Period Covered <span class="text-danger">*</span></label>
                        <input type="date" name="period_covered" class="form-control" id="period_covered" required>
                    </div>    
                    <div class="form-group">
                        <label for="amount">Amount <span class="text-danger">*</span></label>
                        <input type="number" name="amount" class="form-control" id="amount" step="any" min="0" required>
                    </div>    
                    <div class="form-group">
                        <label for="date_requested">Date Requested </label>
                        <input type="date" name="date_requested" class="form-control" id="date_requested">
                    </div>  
                    <div class="form-group">
                        <label for="date_approved">Date Approved</label>
                        <input type="date" name="date_approved" class="form-control" id="date_approved">
                    </div>  
                    <div class="form-group">
                        <label for="status">Status <span class="text-danger">*</span></label>
                        <select name="status" class="form-control" id="status" required>  
                            <option value="0">Request for FUND</option>
                            <option value="1">Credited to ATM</option>
                        </select>
                    </div>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="single_other_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_other_claim_request_for_fund" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_other_claim_request_for_fund'), 'id="form_other_claim_request_for_fund"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Request for FUND</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-upload"></i> Spreadsheet</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" required />
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="request_for_fund_other_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_other_claim_credited" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_other_claim_credited'), 'id="form_other_claim_credited"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-file-excel-o modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Credited to ATM</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-upload"></i> Spreadsheet</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="file" required />
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="credited_other_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_claim_other_temp" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_claim_temp_to_user_and_claim'), 'id="form_claim_other_temp"'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-user modal-icon"></i>
                    <h4 class="modal-title">
                        Claim <i class="text-danger font-bold">Temp</i> <small class="font-bold">Form</small>
                        <br>
                        <small class="font-bold"><i class="fa fa-user-o"></i> Claim not exist from user</small>
                        <br>
                    </h4>
                    <p class="text-danger"><i><strong>Note:</strong> Click save if the information below is correct!</i></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="alert" value="">
                    <div class="form-group">
                        <label for="firstname">Firstname</label>
                        <input type="hidden" name="claim_other_temp_id">
                        <input type="hidden" name="date_requested">
                        <input type="hidden" name="date_approved">
                        <input type="text" name="firstname" class="form-control" id="firstname" required></div>
                    <div class="form-group">
                        <label for="middlename">Middlename</label>
                        <input type="text" name="middlename" class="form-control" id="middlename"></div>
                    <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input type="text" name="lastname" class="form-control" id="lastname" required></div>
                    <div class="form-group">
                        <label for="name_ext">Name Extension</label>
                        <input type="text" name="name_ext" class="form-control" id="name_ext"></div>
                    <div class="form-group">
                        <label for="rank_id">Ranked</label>
                        <select name="rank_id" class="form-control select2" id="rank_id" required style="width: 100%;">
                            <option></option>
                        </select></div>
                    <div class="form-group">
                        <label for="account_number">Account Number</label>
                        <input type="text" name="account_number" class="form-control" id="account_number" required></div>
                    <div class="form-group">
                        <label for="type_of_claim_id">Type of claim</label>
                        <select name="type_of_claim_id" class="form-control select2" id="type_of_claim_id" required style="width: 100%;">
                            <option></option>
                        </select>
                    <div class="form-group">
                        <label for="period_covered">Period Covered</label>
                        <input type="text" name="period_covered" class="form-control" id="period_covered" required>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" name="amount" class="form-control" id="amount" step="any"></div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                            <option value="0">Request for FUND</option>
                            <option value="1">Credited to ATM</option>
                        </select></div>            
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" id="claim_temp_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var tbl_claim_other, tbl_claim_other_data;

    $(function(){

        $("[name=af_user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            templateResult: user_select_format,
            dropdownAutoWidth: true,
            data: <?= $users ?>
        });

        $("[name=user_id]").select2({
            placeholder: "Search",
            allowClear: true,
            templateResult: user_select_format,
            dropdownAutoWidth: true,
            data: <?= $users ?>
        });

        $("[name=rank_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $ranks ?>
        });

        $("[name=type_of_claim_id]").select2({
            placeholder: "Search",
            allowClear: true,
            data: <?= $type_of_claims ?>
        });

        var single_other_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#single_other_claim_btnSave").attr("disabled", true);
                $("#single_other_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#single_other_claim_btnSave").attr("disabled", false);
                    $("#single_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    tbl_claim_other.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#single_other_claim_btnSave").attr("disabled", false);
                            $("#single_other_claim_btnSave").html("Save");
                            $("#form_other_claim_single [name=alert]").val("ok");
                            $("#form_other_claim_single").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#single_other_claim_btnSave").attr("disabled", false);
                            $("#single_other_claim_btnSave").html("Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#single_other_claim_btnSave").attr("disabled", false);
                    $("#single_other_claim_btnSave").html("Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_other_claim_single").ajaxForm(single_other_claim_form_options);

        $("#form_other_claim_request_for_fund [name=file]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'xls':
                case 'xlsx':
                case 'xml':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var request_for_fund_other_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#request_for_fund_other_claim_btnSave").attr("disabled", true);
                $("#request_for_fund_other_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#request_for_fund_other_claim_btnSave").attr("disabled", false);
                    $("#request_for_fund_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    get_claim_temp_other_claims();
                    tbl_claim_other.ajax.reload();
                    location.replace("<?= base_url('admin/claim/otherclaim') ?>");  
                } else if(d.success == false && d.wrong_file == true) {
                    swal({
                        title:  "Warning",
                        text:   "Your file uploaded is not allowed!",
                        type:   "warning"
                    });
                    $("#request_for_fund_other_claim_btnSave").attr("disabled", false);
                    $("#request_for_fund_other_claim_btnSave").html("Try Again");
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Inserted!", "Your data has been save.", "success");
                            $("#form_other_claim_request_for_fund [name=alert]").val("ok");
                            $("#form_other_claim_request_for_fund").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#request_for_fund_other_claim_btnSave").attr("disabled", false);
                            $("#request_for_fund_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#request_for_fund_other_claim_btnSave").attr("disabled", false);
                    $("#request_for_fund_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_other_claim_request_for_fund").ajaxForm(request_for_fund_other_claim_form_options);

        $("#form_other_claim_credited [name=file]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'xls':
                case 'xlsx':
                case 'xml':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var credited_other_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#credited_other_claim_btnSave").attr("disabled", true);
                $("#credited_other_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#credited_other_claim_btnSave").attr("disabled", false);
                    $("#credited_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    get_claim_temp_other_claims();
                    tbl_claim_other.ajax.reload();
                    location.replace("<?= base_url('admin/claim/otherclaim') ?>"); 
                } else if(d.success == false && d.wrong_file == true) {
                    swal({
                        title:  "Warning",
                        text:   "Your file uploaded is not allowed!",
                        type:   "warning"
                    });
                    $("#credited_other_claim_btnSave").attr("disabled", false);
                    $("#credited_other_claim_btnSave").html("Try Again");
                }  else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $("#form_other_claim_credited [name=alert]").val("ok");
                            $("#form_other_claim_credited").submit();
                        } else {
                            swal("Cancelled", "Your data are not save :)", "error");
                            $("#credited_other_claim_btnSave").attr("disabled", false);
                            $("#credited_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#credited_other_claim_btnSave").attr("disabled", false);
                    $("#credited_other_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_other_claim_credited").ajaxForm(credited_other_claim_form_options);

        var claim_temp_otherclaim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#claim_temp_btnSave").attr("disabled", true);
                $("#claim_temp_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    $("#claim_temp_btnSave").attr("disabled", false);
                    $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    get_claim_temp_other_claims();
                    tbl_claim_other.ajax.reload();
                } else if(d.success == false && d.alert == true) {
                    swal({
                        html: true,
                        title: "Are you sure?",
                        text: d.msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#18a689",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: true,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Inserted!", "Your data has been save.", "success");
                            $("#form_claim_temp [name=alert]").val("ok");
                            $("#form_claim_temp").submit();
                        } else {
                            swal("Cancelled", "Your data is not save :)", "error");
                            $("#claim_temp_btnSave").attr("disabled", false);
                            $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                            $(".close").click();
                        }
                    });
                } else {
                    $("#claim_temp_btnSave").attr("disabled", false);
                    $("#claim_temp_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_claim_temp").ajaxForm(claim_temp_otherclaim_form_options);

        $(".af_filter").on("change keyup", function() {
            tbl_claim_other_data = $("#form_advanced_filter").serializeArray();
            $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
            tbl_claim_other.ajax.reload(function() {
                check_user_permission();
            });
        });
        tbl_claim_other = $("#tbl_claim_other").DataTable({
            pageLength: 100,
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ "10", "25", "50", "100", "ALL" ]
            ],
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    },
                },
                {extend: 'csv',
                    title: 'Other Claim Report',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    },
                },
                {extend: 'excel', 
                        title: 'Other Claim Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4 ]
                        },
                },
                {extend: 'pdf', 
                        title: 'Other Claim Report',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4 ]
                        },
                },
                {extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    },
                    title: '<div class="text-center"><img alt="image" style="width: 100px;" src="<?= base_url() ?>assets/custom/images/Philippine_National_Police_seal.svg.png"/><br>PNPR6 Report</div>', // need to change this
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            ajax: {
                url: "<?= base_url('get_other_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_claim_other_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        get_claim_temp_other_claims();

    });

    function get_claim_temp_other_claims(){
        $("#tbl_claim_temp tbody").empty();
        $.post("<?= base_url('get_claim_temp_other_claims') ?>",
            function(data){
                var d = JSON.parse(data);
                var count = 0;

                for (var i = 0; i < d.length; i++) {
                    count = d.length;
                }

                if (count != 0) {

                    $("#dv_claim_temp").show();
                    $.each(d, function(k, v){

                        $("#tbl_claim_temp tbody").append('<tr>\
                                                                <td>'+v.account_number+'</td>\
                                                                <td>'+v.type_of_claim+'</td>\
                                                                <td>'+v.period_covered+'</td>\
                                                                <td>'+v.amount+'</td>\
                                                                <td>'+( v.status == 0 && v.status != null ? '<label class="label label-info block">Request for FUND</label>':'<label class="label label-info block">Credited to ATM</label>' )+'</td>\
                                                                <td class="text-center"><button class="btn btn-primary btn-circle" href="#modal_claim_other_temp" data-toggle="modal" onclick="get_info_claim_temp_other_claim('+v.claim_other_temp_id+',\''+v.account_number+'\',\''+v.type_of_claim_id+'\',\''+v.period_covered+'\',\''+v.amount+'\',\''+v.status+'\',\''+v.date_requested+'\',\''+v.date_approved+'\')" title="Add to user"><i class="fa fa-plus"></i></button></td>\
                                                            </tr>');

                    });
                    
                }
            }
        );

    }

    function get_info_claim_temp_other_claim(claim_other_temp_id, account_number, type_of_claim_id, period_covered, amount, status, date_requested, date_approved) {
        clear_modal("form_claim_other_temp");
        $("#form_claim_other_temp [name=claim_other_temp_id]").val(( claim_other_temp_id != "null" ? claim_other_temp_id:"" ));
        $("#form_claim_other_temp [name=account_number]").val(( account_number != "null" ? account_number:"" ));
        $("#form_claim_other_temp [name=type_of_claim_id]").val(( type_of_claim_id != "null" ? type_of_claim_id:"" ));
        $("#form_claim_other_temp [name=period_covered]").val(( period_covered != "null" ? period_covered:"" ));
        $("#form_claim_other_temp [name=amount]").val(( amount != "null" ? amount:"" ));
        $("#form_claim_other_temp [name=status]").val(( status != "null" ? status:"" ));
        $("#form_claim_other_temp [name=date_requested]").val(( date_requested != "null" ? date_requested:"" ));
        $("#form_claim_other_temp [name=date_approved]").val(( date_approved != "null" ? date_approved:"" ));
    }

    function user_select_format(state) {
        if (!state.id) { return state.text; }
        var $state = $(
            "<table style='width: 100%; font-size: 12px;'>\
                <tr>\
                    <td style='width: 60%; padding: 10px; vertical-align: text-top;'>"+state.text+"</td>\
                    <td style='width: 40%; padding: 10px; vertical-align: text-top;'>"+( state.col1 != null ? state.col1:"No account_number" )+"</td>\
                </tr>\
            </table>"
        );
        return $state;
    }

    function change_status(value){
        $.post("<?= base_url('change_status') ?>",
            { value },
            function(data){
                var d = JSON.parse(data);
                if (d.success == true) {
                    $("#ibox_other_claim").children(".ibox-content").toggleClass("sk-loading");
                    tbl_claim_other.ajax.reload();
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        );
    }

</script>