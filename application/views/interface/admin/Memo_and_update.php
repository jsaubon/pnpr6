<div class="tabs-container">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tab_memo"> Memo List</a></li>
        <li class=""><a data-toggle="tab" href="#tab_update"> Update List</a></li>
    </ul>
    <div class="tab-content">
        <div id="tab_memo" class="tab-pane active">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox float-e-margins" id="ibox_memo">
                        <div class="ibox-title">
                            <h5> <small></small></h5>
                            <div class="ibox-tools">
                                <button onclick="clear_modal('form_memo')" name="btn_add" href="#modal_memo" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Memo</button>

                                <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
                            </div>
                        </div>
                        <div class="ibox-content table-responsive sk-loading">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">                                
                                    <table class="table" id="tbl_memo" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Memo</th>
                                                <th>Title</th>
                                                <th>File Type</th>
                                                <th><div class='text-center'>Action</div></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                   
            </div>
        </div>
        <!-- end tab_memo -->
        <div id="tab_update" class="tab-pane">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox float-e-margins" id="ibox_update">
                        <div class="ibox-title">
                            <h5> <small></small></h5>
                            <div class="ibox-tools">
                                <button onclick="clear_modal('form_update')" name="btn_add" href="#modal_update" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Update</button>

                                <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
                            </div>
                        </div>
                        <div class="ibox-content table-responsive sk-loading">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">                                
                                    <table class="table" id="tbl_update" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Update</th>
                                                <th>Title</th>
                                                <th>File Type</th>
                                                <th><div class='text-center'>Action</div></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
        <!-- end tab_update -->
    </div>
</div>

<!-- modal -->
<div class="modal inmodal" id="modal_memo" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_memo'), 'id=form_memo'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-sticky-note-o modal-icon"></i>
                    <h4 class="modal-title">Memo</h4>
                    <small class="font-bold">Form</small>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <!-- <label for="">Spreadsheet File</label> -->
                        <input type="hidden" name="memo_and_update_id">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="file_memo">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Title <span class="text-danger">*</span></label>
                        <input type="text" name="title" class="form-control" required></div>                          
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="memo_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- memo -->

<div class="modal inmodal" id="modal_update" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open_multipart(base_url('insert_update'), 'id=form_update'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-newspaper-o modal-icon"></i>
                    <h4 class="modal-title">Update</h4>
                    <small class="font-bold">Form</small>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <!-- <label for="">Spreadsheet File</label> -->
                        <input type="hidden" name="memo_and_update_id">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="file_update">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="">Title <span class="text-danger">*</span></label>
                        <input type="text" name="title" class="form-control" required></div>                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="update_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update -->

<div class="modal inmodal" id="modal_memo_and_update_preview" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-eye modal-icon"></i>
                <h4 class="modal-title">Memo and Updates <small class="font-bold">Preview</small></h4>
            </div>
            <div class="modal-body" style="padding: 0px;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="update_btnSave">Save</button>
            </div>
        </div>
    </div>
</div>
<!--  preview-->

<!-- end modal -->

<script type="text/javascript">
    
    var tbl_memo, tbl_memo_data;
    var tbl_update, tbl_update_data;

    $(function(){

        tbl_memo = $("#tbl_memo").DataTable({
            ajax: {
                url: "<?= base_url('get_memos') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_memo").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_memo_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        tbl_update = $("#tbl_update").DataTable({
            ajax: {
                url: "<?= base_url('get_updates') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_update").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_update_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        $("#form_memo [name=file_memo]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'jpeg':
                case 'jpg':
                case 'png':
                case 'pdf':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var memo_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#memo_btnSave").attr("disabled", true);
                $("#memo_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    location.replace("<?= base_url('admin/memo_and_update') ?>");                    
                } else {
                    $("#memo_btnSave").attr("disabled", false);
                    $("#memo_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_memo").ajaxForm(memo_form_options);

        $("#form_update [name=file_update]").on("change", function(){
            var ext = this.value.match(/\.([^\.]+)$/)[1];
            switch(ext)
            {
                case 'jpeg':
                case 'jpg':
                case 'png':
                case 'pdf':
                    break;
                default:
                    swal({
                        title: "Warning",
                        text: "Your file uploaded is not allowed!",
                        type: "warning"
                    });
                    this.value='';
            }
        });
        var update_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#update_btnSave").attr("disabled", true);
                $("#update_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    location.replace("<?= base_url('admin/memo_and_update') ?>");  
                } else {
                    $("#update_btnSave").attr("disabled", false);
                    $("#update_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_update").ajaxForm(update_form_options);

    });

    function get_memo_and_update_preview(value) {
        $("#modal_memo_and_update_preview .modal-body").empty();
        $.post("<?= base_url('get_memo_and_update_preview') ?>",
            { value },
            function(data) {
                var d = JSON.parse(data);

                $("#modal_memo_and_update_preview .modal-body").append(d);
            }
        );
    }

</script>