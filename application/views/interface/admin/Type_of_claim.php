<div class="ibox float-e-margins" id="ibox_type_of_claim">
    <div class="ibox-title">
        <h5>Type of claims <small>List</small></h5>
        <div class="ibox-tools">
            <button onclick="clear_modal('form_type_of_claim')" name="btn_add" href="#modal_type_of_claim" data-toggle="modal" class="btn btn-xs btn-primary"><span class="fa fa-plus"></span> Add Type of Claims</button>

            <a class="btn btn-xs btn-primary fullscreen-link"><i class="fa fa-expand"></i></a>
        </div>
    </div>
    <div class="ibox-content table-responsive sk-loading">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <table class="table" id="tbl_type_of_claim" style="width: 100%;">
            <thead>
                <tr>
                    <th>Type of Claims</th>
                    <th><div class='text-center'>Action</div></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

    </div>
</div>

<div class="modal inmodal" id="modal_type_of_claim" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <?= form_open(base_url('insert_type_of_claim'), 'id=form_type_of_claim'); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-star-o modal-icon"></i>
                    <h4 class="modal-title">Type of Claims <small>Form</small></h4>
                    <!-- <small class="font-bold">Form Ranked</small> -->
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Type of Claims</label>
                        <input type="hidden" name="type_of_claim_id">
                        <input type="text" name="type_of_claim" class="form-control" required></div>         
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="type_of_claim_btnSave">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    var tbl_type_of_claim, tbl_type_of_claim_data;
    
    $(function(){

        tbl_type_of_claim = $("#tbl_type_of_claim").DataTable({
            ajax: {
                url: "<?= base_url('get_type_of_claims') ?>",
                type: "POST",
                data: function() {
                    $("#ibox_type_of_claim").children(".ibox-content").toggleClass("sk-loading");
                    return tbl_type_of_claim_data;
                }
            },
            fnDrawCallback() {
                check_user_permission();
            }
        });

        var type_of_claim_form_options = {
            clearForm: false,
            resetForm: false,
            beforeSubmit: function() {
                $("#type_of_claim_btnSave").attr("disabled", true);
                $("#type_of_claim_btnSave").html("<span class=\"fa fa-spinner fa-pulse\"></span>");
            },
            success: function(data) {
                var d = JSON.parse(data);
                if(d.success == true) {
                    $.notiny({ text: d.msg, position: "right-top" });
                    $("#type_of_claim_btnSave").attr("disabled", false);
                    $("#type_of_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $(".close").click();
                    $("#ibox_type_of_claim").children(".ibox-content").toggleClass("sk-loading");
                    tbl_type_of_claim.ajax.reload();
                } else {
                    $("#type_of_claim_btnSave").attr("disabled", false);
                    $("#type_of_claim_btnSave").html("<span class=\"fa fa-check\"></span> Save");
                    $.notiny({ text: d.msg, position: "right-top" });
                }
            }
        };
        $("#form_type_of_claim").ajaxForm(type_of_claim_form_options);

    });

</script>