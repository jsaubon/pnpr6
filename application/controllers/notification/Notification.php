<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_notification');
	}

	public function get_notifications() {
		$data = [];
		$notifs = [];
		$count_notif = 0;

		if($this->session->login_id != null) {

			foreach ($this->model_notification->select("*", ["user_id" => $this->session->login_id], [], ["date" => "desc"], [5000 => 0]) as $key => $value) {
				$notifs[] = [
					"n_id"	=> $value->n_id,
					"notif"	=> $value->notification,
					"date"	=> date("M d, Y h:i A", strtotime($value->date)) 
				];
			}

			foreach ($this->model_notification->select("COUNT(*) as count", ["user_id" => $this->session->login_id, "is_read" => 0]) as $key => $value) {
				$count_notif = $value->count;
			}
		}
		
		$data = [
			"notifs"		=> $notifs,	
			"count_notif"	=> $count_notif
		];
		echo json_encode($data);
	}

	public function set_as_read() {
		$this->model_notification->update(["is_read" => 1], ["user_id" => $this->session->login_id, "is_read" => 0]);
	}
}

/* End of file Notification.php */
/* Location: ./application/controllers/notification/Notification.php */