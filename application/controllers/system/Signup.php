<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect_home();
		$this->load->model('model_rank');
		$this->load->model('model_permission');
	}

	public function index()
	{
		$data = $this->system();
		$data += [
			"page_title" 		=> "Signup",
			"permission"		=> $this->check_user_permission(""),
			"ranks"				=> $this->get_rank_select()
		];
		$this->load->view('interface/system/Signup', $data, FALSE);
	}

	public function request_signup()
	{
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$badge_number = $this->input->post("badge_number");
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$con_password = $this->input->post("con_password");
		$firstname = $this->input->post("firstname");
		$lastname = $this->input->post("lastname");
		$rank_id = $this->input->post("rank_id");

		$data = [		
			"gender" 		=> $this->input->post("gender"),
			"address" 		=> $this->input->post("address"),
			"rank_id" 		=> $rank_id
		];

		if ($badge_number != "") {
			if ($username != "") {
				if ($firstname != "") {
					if ($lastname != "") {
						if ($rank_id != "") {
							if ($password == $con_password) {
								if ($this->check_badge_number($badge_number) == 0) {
									if ($this->check_username($username) == 0) {
										if ($this->check_fullname($firstname, $lastname) == 0) {
											if (strlen($password) >= 5) {
												$data += [
													"badge_number"	=> $badge_number,
													"username" 		=> $username,
													"password"		=> $password,
													"firstname" 	=> $firstname,
													"lastname" 		=> $lastname,
													"rank_id" 		=> $rank_id,
													"status" 		=> 1,
													"user_role_id"	=> 3
												];

												$this->user_log("Inserted user '".$username."'");
												if($this->model_user->insert($data)) {
													$user_id = $this->db->insert_id();

													$this->model_permission->insert([
														"user_id"		=> $user_id,
														"mod_button_id"	=> 21,
														"status"		=> 1
													]);

													$this->model_permission->insert([
														"user_id"		=> $user_id,
														"mod_button_id"	=> 22,
														"status"		=> 1
													]);

													$this->model_permission->insert([
														"user_id"		=> $user_id,
														"mod_button_id"	=> 23,
														"status"		=> 1
													]);

													$this->model_permission->insert([
														"user_id"		=> $user_id,
														"mod_button_id"	=> 24,
														"status"		=> 1
													]);

													$ret = [
														"success" 	=> true,
														"msg"		=> "<span class='fa fa-check'></span> Success"
													];
												}
											} else {
												$ret = [
													"success" 	=> false,
													"msg"		=> "<span class='fa fa-warning'></span> Password must be alteast 5 characters long"
												];
											}
										} else {
											$ret = [
												"success" 	=> false,
												"msg"		=> "<span class='fa fa-warning'></span> Firstname and Lastname is already taken"
											];
										}
									} else {
										$ret = [
											"success" 	=> false,
											"msg"		=> "<span class='fa fa-warning'></span> Username is already taken"
										];
									}
								} else {
									$ret = [
										"success" 	=> false,
										"msg"		=> "<span class='fa fa-warning'></span> Badge Number is already taken"
									];
								}
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Password did match"
								];
							}
						} else {
							$ret = [
								"success" 	=> false,
								"msg"		=> "<span class='fa fa-warning'></span> Please enter your ranked"
							];
						}
					} else {
						$ret = [
							"success" 	=> false,
							"msg"		=> "<span class='fa fa-warning'></span> Please enter your Lastname"
						];
					}
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Please enter your Firstname"
					];
				}
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Please enter your Username"
				];
			}
		} else {
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Please enter your badge number"
			];
		}
		

		echo json_encode($ret);
	}

	public function check_badge_number($badge_number) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(badge_number) as count", ["badge_number" => $badge_number]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	public function check_username($username) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(username) as count", ["username" => $username]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}

	public function check_fullname($firstname, $lastname) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(*) as count", ["firstname" => $firstname, "lastname" => $lastname]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}

	// others
	public function get_rank_select()
	{
		$data = [];
		foreach ($this->model_rank->select() as $key => $value) {
			$data[] = [
				"id"	=> $value->rank_id,
				"text"	=> $value->rank_name
			];
		}
		return json_encode($data);
	}

}

/* End of file Signup.php */
/* Location: ./application/controllers/system/Signup.php */