<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect_home();
	}

	public function index()
	{
		$data = $this->system();
		$data += [
			"page_title" 	=> "Login",
			"permission"	=> $this->check_user_permission(""),
			"ip"			=> $this->get_ip()
		];
		$this->load->view('interface/system/Login', $data, FALSE);
	}

	public function request_login() {
		if($result = $this->model_user->select("*", ["username" => $this->input->post('username'), "password" => $this->input->post("password")], [
				"user_roles" => "users.user_role_id = user_roles.user_role_id",
				"mooe_ppo"	=> "users.ppo_id = mooe_ppo.ppo_id",
				// "mooe_mps"	=> "users.mps_id = users.mps_id"
			])) {
			foreach ($result as $key => $value) {
				if ($value->status == 1) {
					$data = [
				        'login_id'  		=> $value->user_id,
				        'login_alias' 		=> $value->firstname." ".$value->lastname,
				        'login_role'		=> $value->user_role_id,
				        'login_mps_id'		=> $value->mps_id,
				        'login_ppo_id'		=> $value->ppo_id,
				        'login_ppo_code'	=> $value->ppo_code
					];
					$this->session->set_userdata($data);
					$this->model_user->update(["is_active" => "1"], ["user_id" => $value->user_id]);
					$this->user_log("User ".$this->input->post("username")." has logged in.");
					redirect('dashboard');		
				} else {
					redirect('login?login_attempt='.md5(1));
				}
			}
		} else {
			redirect('login?login_attempt='.md5(0));
		}
	}

	public function request_logout() {
		$this->model_user->update(["is_active" => "0"], ["user_id" => $this->session->login_id]);
		$this->user_log("User ".$this->get_logout_username($this->session->login_id)." has logged out.");
		$array_logout = ['login_id' => '', 'login_alias' => '', 'login_role' => '', 'login_mps_id' => '', 'login_ppo_id' => '', 'login_ppo_code' => ''];
		$this->session->unset_userdata($array_logout);
		$this->session->sess_destroy();
		redirect('login');
	}

	function get_logout_username($user_id){
		$data = "";
		foreach ($this->model_user->select("username", ["user_id" => $user_id]) as $key => $value) {
			$data = $value->username;
			break;
		}
		return $data;
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/system/Login.php */