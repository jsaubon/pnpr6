<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect_home();
	}

	public function index()
	{
		$data = $this->system();
		$data += [
			"page_title" 		=> "Home",
			"permission"		=> $this->check_user_permission(""),
		];
		$this->load->view('interface/home/Home', $data, FALSE);
	}

	public function page_not_found(){
		$data = $this->system();
		$data += [
			"page_title"	=> "Page Not Found"
		];
		$this->load->view('interface/system/pages/Page_not_found', $data);
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */