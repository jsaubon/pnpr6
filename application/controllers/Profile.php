<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Profile",
			"content_title"	=> "<strong>Profile</strong> Page",
			"permission"	=> $this->check_user_permission(""),
			"content_data"	=> [$this->load->view("interface/profile/Profile", [
			], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_info_profile()
	{
		$data = [];
		$data_user = [];
		$data_user_image_single = [];
		$data_user_image_array = [];

		foreach ($this->model_user->select("*", ['user_id' => $this->session->login_id], [
			'units' => 'users.unit_id = units.unit_id',
			'ranks' => 'users.rank_id = ranks.rank_id'
		]) as $key => $value) {
			$data_user = [
				"username" 		=> $value->username,
				"firstname" 	=> $value->firstname,
				"middlename" 	=> $value->middlename,
				"lastname" 		=> $value->lastname,
				"name_ext" 		=> $value->name_ext,
				'unit'			=> $value->unit,
				'rank_name'		=> $value->rank_name
			];
		}

		foreach ($this->model_user_image->select("*", ['user_id' => $this->session->login_id, 'status' => 1]) as $key => $value) {
			$data_user_image_single = [
				'image'	=> $value->location_path
			];
		}

		foreach ($this->model_user_image->select("*", ['user_id' => $this->session->login_id]) as $key => $value) {
			$data_user_image_array[] = [
				"location_path" => $value->location_path
			];
		}

		$data = [
			"data_user"					=> $data_user,
			"data_user_image_single"	=> $data_user_image_single,
			"data_user_image_array"		=> $data_user_image_array
		];
		echo json_encode($data);
	}

	function update_profile(){
		$this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

		$username = $this->input->post('username');
		$old_password = $this->input->post('old_password');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$alert = $this->input->post('alert');

		if ($old_password != "") {
			if ($this->get_last_password($this->session->login_id) == $old_password) {
				if ($username != "") {
					if ($alert == 'ok' && $alert != "") {
						if ($this->get_cur_username($this->session->login_id) == $username) {
							$data = [
								"username" 		=> $username,
								"modified_by"	=> $this->session->login_id,
								"date_modified"	=> $this->now(),
							];
							if ($password !== "" && $confirm_password !== "") {
								$data += [
									"password"		=> $password
								];
							} else {
								$ret = [
						            "success"   => false,
						            "msg"       => "<span class='fa fa-warning'></span> Please enter your password"
						        ];
							}

							$this->model_user->update($data, ['user_id' => $this->session->login_id]);
							$this->user_log("Updated the credential");
							$ret = [
			                    "success"   => true,
			                    "msg"       => "<span class='fa fa-check'></span> Updated"
			                ];	
						} else {
							if ($this->check_username($username) == 0) {
								$data = [
									"username" 		=> $username,
									"modified_by"	=> $this->session->login_id,
									"date_modified"	=> $this->now(),
								];
								if ($password !== "" && $confirm_password !== "") {
									$data += [
										"password"		=> $password
									];
								}
								$this->model_user->update($data, ['user_id' => $this->session->login_id]);
								$this->user_log("Updated your credential");
								$ret = [
				                    "success"   => true,
				                    "msg"       => "<span class='fa fa-check'></span> Updated"
				                ];	
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Username is already taken"
								];
							}
						}
					} else {
						$ret = [
		                    "success"   => false,
		                    "msg"       => "You want to update your credential",
		                    'alert'		=> 'ok'
		                ];	
					}
				} else {
					$ret = [
			            "success"   => false,
			            "msg"       => "<span class='fa fa-warning'></span> Please enter your username"
			        ];
				}
			} else {
				$ret = [
		            "success"   => false,
		            "msg"       => "<span class='fa fa-warning'></span> Old password did not match"
		        ];
			}
		} else {
			$ret = [
	            "success"   => false,
	            "msg"       => "<span class='fa fa-warning'></span> Please enter your old password"
	        ];
		}

		if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];
        }
        else {
            $this->db->trans_commit();
        }

		echo json_encode($ret);
	}

	function check_username($username) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(username) as count", ["username" => $username]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_cur_username($user_id) {
		$username = "";
		foreach ($this->model_user->select("username", ["user_id" => $user_id]) as $key => $value) {
			$username = $value->username;
			break;
		}
		return $username;
	}

	function get_last_password($user_id){
		$password = "";
		foreach ($this->model_user->select("password", ["user_id" => $user_id]) as $key => $value) {
			$password = $value->password;
			break;
		}
		return $password;
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */