<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assign_rmfb extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect(); 
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Assign RMFB",
			"content_title"	=> "<strong>Assign RMFB for Users</strong>",
			"permission"	=> $this->check_user_permission("Assign RMFB"),
			"content_data"	=> [$this->load->view("interface/mooe/Assign_rmfb", [
								"mps"				=> $this->get_mps_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_user_select()
	{
		$data = $this->fetchRawData("SELECT user_id,CONCAT(lastname,' ',firstname,' ',LEFT(middlename , 1) ,'. ',name_ext) as `fullname` FROM users");
		echo json_encode($data);
	}

	function get_mps_select() {
		$data = $this->fetchRawData("SELECT aaa.mps_id, CONCAT('<span style=color:red;>',ccc.place, '</span> - ', assigned_place) as `mps`,user_id,CONCAT(lastname,' ',firstname,' ',LEFT(middlename , 1) ,'. ',name_ext) as `fullname` FROM mooe_mps as aaa LEFT JOIN users as bbb ON aaa.mps_id = bbb.mps_id LEFT JOIN mooe_ppo as ccc ON aaa.ppo_code = ccc.ppo_code WHERE ccc.place = '13TH REGIONAL PUBLIC SAFETY BATTALION'");
		return $data;
	}

	function update_user_mps() {
		$mps_id = $this->input->post('mps_id');
		$user_id = $this->input->post('user_id');
		$data = $this->db->query("UPDATE users SET mps_id = 0 WHERE mps_id=$mps_id");
		$data = $this->db->query("UPDATE users SET mps_id = $mps_id WHERE user_id=$user_id");
		echo "updated!";
	}
 

}