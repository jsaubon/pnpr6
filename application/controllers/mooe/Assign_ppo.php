<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assign_ppo extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect(); 
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Assign PPO",
			"content_title"	=> "<strong>Assign PPO for Users</strong>",
			"permission"	=> $this->check_user_permission("Assign PPO"),
			"content_data"	=> [$this->load->view("interface/mooe/Assign_ppo", [ 
								"ppo"				=> $this->get_ppo_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_user_select()
	{
		$data = $this->fetchRawData("SELECT user_id,CONCAT(lastname,' ',firstname,' ',LEFT(middlename , 1) ,'. ',name_ext) as `fullname` FROM users");
		echo json_encode($data);
	}

	function get_ppo_select() {
		$data = $this->fetchRawData("SELECT aaa.ppo_id,CONCAT('<span style=color:red;>',place,'</span>') as `ppo`,user_id,CONCAT(lastname,' ',firstname,' ',LEFT(middlename , 1) ,'. ',name_ext) as `fullname` FROM mooe_ppo as aaa LEFT JOIN users as bbb ON aaa.ppo_id = bbb.ppo_id");
		return $data;
	}

	function update_user_ppo() {
		$ppo_id = $this->input->post('ppo_id');
		$user_id = $this->input->post('user_id');
		$data = $this->db->query("UPDATE users SET ppo_id = 0 WHERE ppo_id=$ppo_id");
		$data = $this->db->query("UPDATE users SET ppo_id = $ppo_id WHERE user_id=$user_id");
		echo "updated!";
	}
 

}