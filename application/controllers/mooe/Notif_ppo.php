<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_ppo extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect(); 
	}
	
	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Notif PPO",
			"content_title"	=> "<strong>Notifications for PPO </strong>",
			"permission"	=> $this->check_user_permission("Notify PPO"),
			"content_data"	=> [$this->load->view("interface/mooe/Notif_ppo", [ 
								"ppo"				=> $this->get_ppo_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_ppo_select()
	{
		$data = $this->fetchRawData("SELECT * FROM mooe_ppo");
		return $data;
	}

	function getPPONotifs()
	{
		$ppo_code = $this->input->post('ppo_code');
		$data = $this->fetchRawData("SELECT mooe_notif_ppo.*,CONCAT(users.lastname,' ',users.firstname,' ',COALESCE(LEFT(users.middlename , 1),'') ,'. ',COALESCE(users.name_ext,'')) as `fullname` FROM mooe_notif_ppo INNER JOIN users ON mooe_notif_ppo.sender_id = users.user_id WHERE ppo_code='$ppo_code' AND soft_deleted=0");
		echo json_encode($data);
	}

	function sendPPONotif()
	{
		$ppo_code = $this->input->post('ppo_code');
		$notif_message = $this->input->post('notif_message');
		// $attachment = $this->input->post('attachment');

		$sender_id = $this->session->login_id;
		$date_sent = date('Y-m-d H:i:s');

		// print_r($_FILES) ;
		if ($_FILES['attachment']['name'] != '') {
			$_FILES['repoFile']['name'] = date('YmdHis').'_'.$_FILES['attachment']['name'];
            $_FILES['repoFile']['type'] = $_FILES['attachment']['type'];
            $_FILES['repoFile']['tmp_name'] = $_FILES['attachment']['tmp_name'];
            $_FILES['repoFile']['error'] = $_FILES['attachment']['error'];
            $_FILES['repoFile']['size'] = $_FILES['attachment']['size'];

            $uploadPath = 'assets/attachments';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

             if($this->upload->do_upload('repoFile')){
                $fileData = $this->upload->data();

                $file_name = $fileData['file_name'];
                $file_type = $fileData['file_type'];
                $file_size = $fileData['file_size'];
                
                
				$data = $this->db->query("INSERT INTO mooe_notif_ppo (sender_id,notif_message,ppo_code,date_sent,attachment) VALUES ($sender_id,'$notif_message','$ppo_code','$date_sent','$file_name')"); 
            } 
            else
            {
            	$error = array('error' => $this->upload->display_errors());
            	echo json_encode($error);
            }
		} else { 
			$data = $this->db->query("INSERT INTO mooe_notif_ppo (sender_id,notif_message,ppo_code,date_sent) VALUES ($sender_id,'$notif_message','$ppo_code','$date_sent')"); 
		}

		$ppo_code_users = $this->fetchRawData("SELECT user_id FROM users INNER JOIN mooe_ppo ON users.ppo_id = mooe_ppo.ppo_id WHERE ppo_code='$ppo_code'");
		foreach ($ppo_code_users as $key => $value) {
			$user_id = $value['user_id'];
			$this->insertNotfication($user_id,"New message from PPO");
		}
		
		echo 'inserted!';
	}

	function getMPSByPpoCode() {
		$ppo_code = $this->input->post('ppo_code');
		$mnp_id = $this->input->post('mnp_id');
		$data = $this->fetchRawData("SELECT mooe_mps.*,mooe_notif_mps.mnm_id,mooe_notif_mps.forwarded_date FROM mooe_mps LEFT JOIN mooe_notif_mps ON mooe_mps.mps_id = mooe_notif_mps.mps_id AND mnp_id=$mnp_id WHERE mooe_mps.ppo_code = '$ppo_code'  ;");
		echo json_encode($data);
	}

	function forwardToMPS() {
		$mps_id = $this->input->post('mps_id');
		$mnp_id = $this->input->post('mnp_id');
		$forwarded_by = $this->session->login_id;
		$forwarded_date = date('Y-m-d H:i:s');
		$data = $this->db->query("INSERT INTO mooe_notif_mps (forwarded_by,forwarded_date,mps_id,mnp_id) VALUES ($forwarded_by,'$forwarded_date',$mps_id,$mnp_id)");
		$ppo_code_users = $this->fetchRawData("SELECT user_id FROM users  WHERE mps_id=$mps_id");
		foreach ($ppo_code_users as $key => $value) {
			$user_id = $value['user_id'];
			$this->insertNotfication($user_id,"New message forwarded to MPS");
		}
		echo "inserted!";
	}

	function forwardToAllMPS() {
		$mnp_id = $this->input->post('mnp_id');
		$ppo_code = $this->input->post('ppo_code');
		$forwarded_by = $this->session->login_id;
		$forwarded_date = date('Y-m-d H:i:s');
		$data = $this->fetchRawData("SELECT mps_id FROM mooe_mps WHERE ppo_code = '$ppo_code'");
		// $data_delete = $this->db->query("DELETE FROM mooe_notif_mps WHERE $mnp_id = $mnp_id");
		$forwarded_to_all = 1;
		$insert_array = array();
		foreach ($data as $key => $value) {
			$mps_id = $value['mps_id'];
			array_push($insert_array, "($forwarded_by,'$forwarded_date',$mps_id,$mnp_id)");
			
		}
		$update_mnp = $this->db->query("UPDATE mooe_notif_ppo SET forwarded_to_all=$forwarded_to_all WHERE mnp_id=$mnp_id");
		$insert_q = implode(',', $insert_array);
		$insert = $this->db->query("INSERT INTO mooe_notif_mps (forwarded_by,forwarded_date,mps_id,mnp_id) VALUES $insert_q");


		$mps_users = $this->fetchRawData("SELECT user_id FROM users INNER JOIN mooe_mps ON users.mps_id = mooe_mps.mps_id  WHERE ppo_code='$ppo_code'");
		foreach ($ppo_code_users as $key => $value) {
			$user_id = $value['user_id'];
			$this->insertNotfication($user_id,"New message from MPS");
		}
		echo "inserted!";
		// echo json_encode($insert_q);
	}

	function deleteMNP()
	{
		$mnp_id = $this->input->post('mnp_id');
		$user_id = $this->session->login_id;
		$data = $this->db->query("UPDATE mooe_notif_ppo SET soft_deleted = 1, deleted_by = $user_id WHERE mnp_id=$mnp_id");
		echo $data;
	}
	 
 

}