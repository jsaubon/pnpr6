<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_mps extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect(); 
		$this->load->model('model_mooe_notif_mps');		
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Notif MPS",
			"content_title"	=> "<strong>Notifications for MPS </strong>",
			"permission"	=> $this->check_user_permission("Notify MPS"),
			"content_data"	=> [$this->load->view("interface/mooe/Notif_mps", [
									// "mps_notifs"				=> $this->getMPSNotifs()
										 ], TRUE)]
		];
		$this->create_page($page_data);
	} 

	function getMPSNotifs() {
		$mps_id = $this->session->login_mps_id;
		$data = [];
		foreach ($this->model_mooe_notif_mps->query("SELECT aaa.*,bbb.notif_message,bbb.attachment FROM mooe_notif_mps as aaa INNER JOIN mooe_notif_ppo as bbb ON aaa.mnp_id = bbb.mnp_id AND bbb.soft_deleted=0 WHERE aaa.mps_id = $mps_id ORDER BY aaa.mnm_id DESC")->result() as $key => $value) {

			$attachment = $value->attachment;

			if ($attachment != null) {
                $attachment_url = base_url('assets/attachments/'.$attachment.'');
                $attachment_arr = explode('_', $attachment);
                $attachment = '<a  download href="'.$attachment_url.'">'.$attachment_arr[1].'</a>';
            } else {
                $attachment = '';
            }

			$data[] = [
				'new_data' => '<div class="chat-message right">
                                    <div class="message">
                                        
                                        <p class="message-content">
	                                        <span class="message-date text-left"> 
	                                           Forwarded by: '.$this->full_name($value->forwarded_by).'<br> Date Forwarded : '.date("F j, Y, g:i a",strtotime($value->forwarded_date)).'

	                                        </span> 
                                            '.$value->notif_message.'<br>'.$attachment.'
                                        </p>
                                    </div>
                                </div>'
			];
		}
		echo json_encode($data);
	}
 
 	function delete_this_mgs()
 	{
 		$this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];
        
        $this->db->query("UPDATE mooe_notif_mps SET soft_delete=1, delete_by=".$this->session->login_mps_id." WHERE mnm_id=".$this->input->post("value"));
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $ret = [
	            "success"   => false,
	            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
	        ];
        }
        else {
            $this->db->trans_commit();
            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];  
        }
        echo json_encode($ret);
 	}

}