<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_claim extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_claim_cdp');
		$this->load->model('model_claim_cip');
		$this->load->model('model_claim_other');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Claims",
			"content_title"	=> "<strong>Claims</strong> Page",
			"permission"	=> $this->check_user_permission("User Claims"),
			"content_data"	=> [$this->load->view('interface/User_claim', [], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_user_other_claims()
	{
		$data = ["data" => []];
		$where = [
			"user_id" => $this->session->login_id
		];
		foreach ($this->model_claim_other->select("*", $where, [
			"type_of_claims" => "claim_others.type_of_claim_id = type_of_claims.type_of_claim_id"
		], ["status" => "asc"]) as $key => $value) {

			$status = ['<label class="label label-success block">Request for FUND<label>', '<label class="label label-primary block">Credited to ATM<label>'];


			$data["data"][] = [
				$value->type_of_claim." - ".$value->period_covered,
				$value->amount,
				$status[$value->status],
			];
		}
		echo json_encode($data);
	}

	function get_user_cdp_claims()
	{
		$data = ["data" => []];
		$where = [
			"user_id" => $this->session->login_id
		];
		foreach ($this->model_claim_cdp->select("*", $where, [], ["status" => "asc"]) as $key => $value) {

			$status = ['<label class="label label-success block">Request for FUND<label>', '<label class="label label-primary block">Credited to ATM<label>'];

			$data["data"][] = [
				$value->date_paid,
				$value->amount,
				$status[$value->status],
			];
		}
		echo json_encode($data);
	}

	function get_user_cip_claims()
	{
		$data = ["data" => []];
		$where = [
			"user_id" => $this->session->login_id
		];
		foreach ($this->model_claim_cip->select("*", $where, [], ["status" => "asc"]) as $key => $value) {

			$status = ['<label class="label label-success block">Request for FUND<label>', '<label class="label label-primary block">Credited to ATM<label>'];


			$data["data"][] = [
				$value->date_paid,
				$value->amount,
				$status[$value->status],
			];
		}
		echo json_encode($data);
	}

}

/* End of file User_claim.php */
/* Location: ./application/controllers/User_claim.php */