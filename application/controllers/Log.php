<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_user_log');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Logs",
			"content_title"	=> "<strong>Logs</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Logs"),
			"content_data"	=> [$this->load->view("interface/log/Log", [], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_logs()
	{
		$data = ["data" => []];;
		foreach ($this->model_user_log->select("*", [], [], ["date" => "desc"], [5000=>0]) as $key => $value) {

			$status = ['<label class="label label-success block">Request for FUND<label>', '<label class="label label-primary block">Credited to ATM<label>'];


			$data["data"][] = [
				$this->full_name($value->user_id),
				$value->action,
				date("M d, Y H:i:s", strtotime($value->date)),
			];
		}
		echo json_encode($data);
	}
	
}

/* End of file Log.php */
/* Location: ./application/controllers/Log.php */