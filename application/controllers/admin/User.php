<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_user');
		$this->load->model('model_unit');
		$this->load->model('model_user_role');
		$this->load->model('model_rank');
		$this->load->model('model_position');
		$this->load->model('model_permission');
		$this->load->library('excel'); 
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "User",
			"content_title"	=> "<strong>User</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Users"),
			"content_data"	=> [$this->load->view("interface/admin/User", [
								"ranks"				=> $this->get_rank_select(),
								"units"				=> $this->get_unit_select(),
								"user_roles"		=> $this->get_user_role_select(),
								"account_numbers"	=> $this->get_account_number_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_users() {
		$data = ["data" => []];

		$where = [];

		$f_user_role_id = $this->input->post("f_user_role_id");
		$f_rank_id = $this->input->post("f_rank_id");
		$f_account_number = $this->input->post("f_account_number");

		if ($f_user_role_id != null) {
			$where += ["users.user_role_id" => $f_user_role_id];
		}
		if ($f_rank_id != null) {
			$where += ["users.rank_id" => $f_rank_id];
		}
		if ($f_account_number != null) {
			$where += ["user_id" => $f_account_number];
		}

		foreach ($this->model_user->select("*", $where, [
				"user_roles" => "users.user_role_id = user_roles.user_role_id",
				"ranks" => "users.rank_id = ranks.rank_id",
				"units" => "users.unit_id = units.unit_id",
			], ["lastname" => "asc"], []) as $key => $value) {
			$id = $value->user_id;
			$url_delete = "\"delete_user\"";
			$url_edit = "\"get_info_user\"";
			$form_id = "\"form_user\"";
			$tbl_id = "[tbl_user]";
			$modal = "modal_user";

			$account_number = "";
			if ($value->account_number != null) {
				$exploded_account_number = explode("æ", $value->account_number);
				$account_number = $exploded_account_number[1];				
			}

			$show = false;
			if ($value->firstname." ".$value->lastname != "Klaven Rey Matugas") {
				$show = true;
			}

			if ($show == true) {
				$data["data"][] = [
					$value->username,
					$this->full_name($value->user_id),
					$account_number,
					$value->user_role,
					$value->rank_name,
					$value->unit,
					( $value->status == 1 ? '<label class="label label-primary block">Active</label>':'<label class="label label-danger block">Inactive</label>' ),
					"<div class='text-center'>
						<button class='btn btn-info btn-circle' name='btn_edit_permission' data-toggle='modal' href='#modal_permission' onclick='get_permission($id)' title='Permission'><span class='fa fa-cog'></span></button>
						<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id);' title='Edit'><span class='fa fa-edit'></span></button>
						<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
					</div>"
				];
				
			}
		}
		echo json_encode($data);
	}

	function insert_user()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$user_id 		= $this->input->post("user_id");
		$username 		= $this->input->post("username");
		$password 		= $this->input->post("password");
		$firstname 		= $this->input->post("firstname");
		$lastname 		= $this->input->post("lastname");
		$middlename 	= $this->input->post("middlename");
		$name_ext 		= $this->input->post("name_ext");

		$data = [
			"status" 			=> $this->input->post("status"),
			"rank_id" 			=> $this->input->post("rank_id"),
			"unit_id" 			=> $this->input->post("unit_id"),
			"user_role_id" 		=> $this->input->post("user_role_id"),
			"tin_number" 		=> $this->input->post("tin_number"),
			"account_number" 	=> ( $this->input->post("account_number") != "" ? md5($this->input->post("account_number")).'æ'.$this->input->post("account_number").'æ'.md5($this->input->post("account_number")):"" ),
			"atm_number" 		=> ( $this->input->post("atm_number") != "" ? md5($this->input->post("atm_number")).'æ'.$this->input->post("atm_number").'æ'.md5($this->input->post("atm_number")):'' )
		];

		if ($user_id == null) {
			if ($this->input->post("account_number") != "" || $this->input->post("atm_number") != "") {
				if ($this->check_username($username) == 0) {
					if ($this->check_fullname($firstname, $middlename, $lastname, $lastname) == 0) {
						if ($password != "") {
							if (strlen($password) >= 5) {
								$data += [
									"password" 		=> $password,
									"username"		=> $username,
									"firstname" 	=> $firstname,
									"middlename"	=> $middlename,
									"lastname" 		=> $lastname,
									"name_ext"		=> $name_ext
								];

								$this->user_log("Inserted user '".$username."'");

								if($this->model_user->insert($data)) {
									$ret = [
										"success" 	=> true,
										"msg"		=> "<span class='fa fa-check'></span> Success"
									];
								}
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Password must be alteast 5 characters long"
								];
							}
						} else {
							$ret = [
								"success" 	=> false,
								"msg"		=> "<span class='fa fa-warning'></span> Please enter your password"
							];
						}
					} else {
						$ret = [
							"success" 	=> false,
							"msg"		=> "<span class='fa fa-warning'></span> Firstname and Lastname is already taken"
						];
					}
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Username is already taken"
					];
				}
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Account Number or ATM Number cannot be empty!"
				];
			}
		} else {
			if ($this->input->post("account_number") != "" || $this->input->post("atm_number") != "") {
				if ($this->get_cur_username($user_id) == $username) {
					if ($this->get_fullname($user_id) == $firstname."".$middlename."".$lastname."".$name_ext) {
						if ($password != "") {
							if (strlen($password) >= 5) {
								$data += [
									"password" 		=> $password,
									"username"		=> $username,
									"firstname" 	=> $firstname,
									"middlename"	=> $middlename,
									"lastname" 		=> $lastname,
									"name_ext"		=> $name_ext,
								];

								$this->user_log("Updated user '".$username."'");
								
								if($this->model_user->update($data, ["user_id" => $user_id])) {
									$ret = [
										"success" 	=> true,
										"msg"		=> "<span class='fa fa-check'></span> Updated"
									];
								}
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Password must be alteast 5 characters long"
								];
							}
						} else {
							$data += [
								"username"		=> $username,
								"firstname" 	=> $firstname,
								"middlename"	=> $middlename,
								"lastname" 		=> $lastname,
								"name_ext"		=> $name_ext,
							];
							if($this->model_user->update($data, ["user_id" => $user_id])) {

								$this->user_log("Updated user '".$username."'");

								$ret = [
									"success" 	=> true,
									"msg"		=> "<span class='fa fa-check'></span> Updated"
								];
							}
						}
					} else {
						if ($this->check_fullname($firstname, $middlename, $lastname, $name_ext) == 0) {
							if ($password != "") {
								if (strlen($password) >= 5) {
									$data += [
										"password" 		=> $password,
									];
								} else {
									$ret = [
										"success" 	=> false,
										"msg"		=> "<span class='fa fa-warning'></span> Password must be alteast 5 characters long"
									];
								}
								$data += [
									"username"		=> $username,
									"firstname" 	=> $firstname,
									"middlename"	=> $middlename,
									"lastname" 		=> $lastname,
									"name_ext"		=> $name_ext,
								];
								if($this->model_user->update($data, ["user_id" => $user_id])) {

									$this->user_log("Updated user '".$username."'");

									$ret = [
										"success" 	=> true,
										"msg"		=> "<span class='fa fa-check'></span> Updated"
									];
								}
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Please enter your password"
								];
							}
						} else {
							$ret = [
								"success" 	=> false,
								"msg"		=> "<span class='fa fa-warning'></span> Firstname and Lastname is already taken"
							];
						}
					}
				} else {
					if ($this->check_username($username) == 0) {
						if ($password != "") {
							if (strlen($password) >= 5) {
								$data += [
									"password" 		=> $password,
								];
							} else {
								$ret = [
									"success" 	=> false,
									"msg"		=> "<span class='fa fa-warning'></span> Password must be alteast 5 characters long"
								];
							}
							$data += [
								"username"		=> $username,
								"firstname" 	=> $firstname,
								"middlename"	=> $middlename,
								"lastname" 		=> $lastname,
								"name_ext"		=> $name_ext,
							];
							if($this->model_user->update($data, ["user_id" => $user_id])) {

								$this->user_log("Updated user '".$username."'");

								$ret = [
									"success" 	=> true,
									"msg"		=> "<span class='fa fa-check'></span> Updated"
								];
							}
						} else {
							$ret = [
								"success" 	=> false,
								"msg"		=> "<span class='fa fa-warning'></span> Please enter your password"
							];
						}
					} else {
						$ret = [
							"success" 	=> false,
							"msg"		=> "<span class='fa fa-warning'></span> Username is already taken"
						];
					}
				}
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Account Number or ATM Number cannot be empty!"
				];
			}
		}

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	function check_username($username) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(username) as count", ["username" => $username]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_cur_username($user_id) {
		$username = "";
		foreach ($this->model_user->select("username", ["user_id" => $user_id]) as $key => $value) {
			$username = $value->username;
			break;
		}
		return $username;
	}

	function check_fullname($firstname, $middlename, $lastname, $name_ext) {
		$count = 0;
		foreach ($this->model_user->select("COUNT(*) as count", ["firstname" => $firstname, "middlename" => $middlename, "lastname" => $lastname, "name_ext" => $name_ext]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_fullname($user_id) {
		$fullname = "";
		foreach ($this->model_user->select("firstname, middlename, lastname, name_ext", ["user_id" => $user_id]) as $key => $value) {
			$fullname = $value->firstname."".$value->middlename."".$value->lastname."".$value->name_ext;
			break;
		}
		return $fullname;
	}

	function insert_multiple_user()
	{
		
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$warning = "";
        $upload_alert = $this->input->post("alert");

		$count = 0;

		if (!empty($_FILES["file"]["name"])) { 

			$path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];

            if ($extension == "xls" || $extension == "xlsx" || $extension == "xml") {
            	$path = $_FILES["file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load($path);
				$count = $object->setActiveSheetIndex(0)->getHighestRow() - 1;	

				if ($upload_alert == "ok" && $upload_alert != null) {

					try {

						foreach($object->getWorksheetIterator() as $worksheet)
						{
							$highestRow = $worksheet->getHighestRow();
							$highestColumn = $worksheet->getHighestColumn();

							$a = $worksheet->getCellByColumnAndRow(0,1)->getValue();
							$b = $worksheet->getCellByColumnAndRow(1,1)->getValue();
							$c = $worksheet->getCellByColumnAndRow(2,1)->getValue();
							$d = $worksheet->getCellByColumnAndRow(3,1)->getValue();
							$e = $worksheet->getCellByColumnAndRow(4,1)->getValue();
							$f = $worksheet->getCellByColumnAndRow(5,1)->getValue();
							$g = $worksheet->getCellByColumnAndRow(6,1)->getValue();
							$h = $worksheet->getCellByColumnAndRow(7,1)->getValue();
							$i = $worksheet->getCellByColumnAndRow(8,1)->getValue();

							if (strtoupper(preg_replace('/\s+/', '', $a)) == "ACCOUNTNUMBER" && strtoupper(preg_replace('/\s+/', '', $b)) == "RANK" && strtoupper(preg_replace('/\s+/', '', $c)) == "LASTNAME" && strtoupper(preg_replace('/\s+/', '', $d)) == "FIRSTNAME" && strtoupper(preg_replace('/\s+/', '', $e)) == "MIDDLENAME" && strtoupper(preg_replace('/\s+/', '', $f)) == "SUFFIXNAME" && strtoupper(preg_replace('/\s+/', '', $g)) == "UNIT" && strtoupper(preg_replace('/\s+/', '', $h)) == "TINNUMBER" && strtoupper(preg_replace('/\s+/', '', $i)) == "ATMNUMBER") {

								for($row=2; $row<=$highestRow; $row++)
								{
									$account_number 		= $worksheet->getCellByColumnAndRow(0,$row)->getValue();       
					                $rank 					= $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
					                $lastname				= $worksheet->getCellByColumnAndRow(2,$row)->getValue(); 
					                $firstname				= $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
					                $middlename				= $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
					                $name_ext				= $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
					                $unit					= $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
					                $tin_number				= $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 
					                $atm_number				= $worksheet->getCellByColumnAndRow(8,$row)->getValue(); 

					                if ($account_number != "" || $atm_number != "") {

					                	$rank_id = "";
						                if ($this->check_ranked($rank) != 0) {
						                	$rank_id = $this->get_ranked_id($rank);
						                } else {
						                	$this->model_rank->insert([
						                		"rank_name" 	=> $rank,
						                		"created_by"	=> $this->session->login_id,
												"date_created"	=> $this->now()
						                	]);
						                	$rank_id = $this->db->insert_id();
						                }

						                $unit_id = "";
						                if ($this->check_unit($unit) != 0) {
						                	$unit_id = $this->get_unit_id($unit);
						                } else {
						                	$this->model_unit->insert([
						                		"unit" 			=> $rank,
						                		"created_by"	=> $this->session->login_id,
												"date_created"	=> $this->now()
						                	]);
						                	$unit_id = $this->db->insert_id();
						                }

						                $data = [
						                	"account_number"	=> md5($account_number).'æ'.$account_number.'æ'.md5($account_number),
						                	"firstname"			=> strtoupper($firstname),
						                	"middlename"		=> strtoupper($middlename),
						                	"lastname"			=> strtoupper($lastname),
						                	"name_ext"			=> strtoupper($name_ext),
						                	'tin_number'		=> $tin_number,
						                	"atm_number"		=> md5($atm_number).'æ'.$atm_number.'æ'.md5($atm_number),
						                	"username"			=> $account_number,
						                	"password"			=> strtolower($firstname[0]).strtolower($middlename[0]).strtolower($lastname).strtolower($name_ext),
						                	"user_role_id"		=> 3,
						                	"status"			=> 1,
						                	"rank_id"			=> $rank_id,
						                	"unit_id"			=> $unit_id
						                ];

						                $fullname = strtoupper($firstname)." ".strtoupper($middlename)." ".strtoupper($lastname)." ".strtoupper($name_ext);

					                	if ($this->multi_check_user_fullname(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext)) == 0) {

					                		$data += [
					                			"created_by"	=> $this->session->login_id,
												"date_created"	=> $this->now()
					                		];
					                		$this->model_user->insert($data);
					                		$this->user_log("Inserted user '".$fullname."'");
					                		$warning = "success";

										} else {
											$data += [
					                			"modified_by"	=> $this->session->login_id,
												"date_modified"	=> $this->now()
					                		];
					                		$this->model_user->update($data, ['user_id' => $this->multi_user_id(strtoupper($firstname), strtoupper($middlename), strtoupper($lastname), strtoupper($name_ext))]);
					                		$this->user_log("Updated user '".$fullname."'");
					                		$warning = "success";
										}
					                }
								}

								$ret = [
				                    "success"   	=> true,
				                    "msg"       	=> "<span class='fa fa-check'></span> Success"
				                ];
							} else {
								 $ret = [
				                    "success"   	=> false,
				                    "msg"       	=> "<strong>Your file uploaded is wrong format! <br> Please download the correct format</strong>"
				                ];
							}
						}



					} catch (Exception $e) {
						 $ret = [
							"success" 	=> false,
							"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
						];
                        // echo $e->getMessaage();
					}

				} else {
					$ret = [
	                    "success"   => false,
	                    "msg"       => "You want to upload these <strong>$count</strong> data?",
	                    "alert"     => true,
	                    "count"     => "<strong>$count</strong>"
	                ];
				}
            } else {

            	$ret = [
                    "success"   	=> false,
                    "msg"       	=> "Your file uploaded is not allowed!"
                ];

            }			
				
		} 

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	function multi_check_user_fullname($firstname, $middlename, $lastname, $name_ext)
	{
		$count = 0;
		foreach ($this->model_user->select("COUNT(*) as count", ["firstname" => $firstname, "middlename" => $middlename, "lastname" => $lastname, "name_ext" => $name_ext]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function multi_user_id($firstname, $middlename, $lastname, $name_ext)
	{
		$user_id = "";
		foreach ($this->model_user->select("user_id", ["firstname" => $firstname, "middlename" => $middlename, "lastname" => $lastname, "name_ext" => $name_ext]) as $key => $value) {
			$user_id = $value->user_id;
			break;
		}
		return $user_id;
	}

	function check_ranked($rank_name)
	{
		$count = 0;
		foreach ($this->model_rank->select("COUNT(*) as count", ["rank_name" => $rank_name]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_ranked_id($rank_name)
	{
		$rank_id = "";
		foreach ($this->model_rank->select("rank_id", ["rank_name" => $rank_name]) as $key => $value) {
			$rank_id = $value->rank_id;
			break;
		}
		return $rank_id;
	}

	function check_unit($unit)
	{
		$count = 0;
		foreach ($this->model_unit->select("COUNT(*) as count", ["unit" => $unit]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_unit_id($unit)
	{
		$unit_id = "";
		foreach ($this->model_unit->select("unit_id", ["unit" => $unit]) as $key => $value) {
			$unit_id = $value->unit_id;
			break;
		}
		return $unit_id;
	}

	function get_info_user()
	{
		$data = [];
		foreach ($this->model_user->select("*", ["user_id" => $this->input->post("value")]) as $key => $value) {

			$account_number = "";
			$atm_number = "";
			if ($value->account_number != null) {
				$exploded_account_number = explode("æ", $value->account_number);
				$account_number = $exploded_account_number[1];
			}
			if ($value->atm_number != null) {
				$exploded_atm_number = explode("æ", $value->atm_number);
				$atm_number = $exploded_atm_number[1];
			}

			$data = [
				"user_id" 			=> $value->user_id,
				"user_role_id"		=> $value->user_role_id,
				"username"			=> $value->username,
				"firstname"			=> $value->firstname,
				"middlename"		=> $value->middlename,
				"lastname"			=> $value->lastname,
				"name_ext"			=> $value->name_ext,
				"account_number"	=> $account_number,
				"atm_number"		=> $atm_number,
				"tin_number"		=> $value->tin_number,
				"status"			=> $value->status,
				"rank_id"			=> $value->rank_id,
				"unit_id"			=> $value->unit_id,
				"position_id"		=> $value->position_id
			];
		}
		echo json_encode($data);
	}

	function delete_user() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		// $this->notify("Users", "<b>".$this->user_full_name($this->session->login_id)."</b> deleted user \"".$this->get_username($this->input->post("value"))."\"", "all");

		$this->user_log("Deleted user \"".$this->get_username($this->input->post("value"))."\"");

		$this->model_permission->delete(["user_id" => $this->input->post("value")]);
		$this->model_user->delete(["user_id" => $this->input->post("value")]);

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];
		}
		echo json_encode($ret);
	}

	function get_username($user_id) {
		$ret = "";
		foreach ($this->model_user->select("username", ["user_id" => $user_id]) as $key => $value) {
			$ret = $value->username;
			break;
		}
		return $ret;
	}

	// others
	function get_unit_select()
	{
		$data = [];
		foreach ($this->model_unit->select() as $key => $value) {
			$data[] = [
				"id"	=> $value->unit_id,
				"text"	=> $value->unit
			];
		}
		return json_encode($data);
	}

	function get_rank_select()
	{
		$data = [];
		foreach ($this->model_rank->select() as $key => $value) {
			$data[] = [
				"id"	=> $value->rank_id,
				"text"	=> $value->rank_name
			];
		}
		return json_encode($data);
	}

	function get_user_role_select() {
		$data = [];
		foreach ($this->model_user_role->select("*", [], [], ["user_role_id" => "asc"]) as $key => $value) {
			$data[] = [
				"id" 	=> $value->user_role_id,
				"text"	=> $value->user_role
			];
		}
		return json_encode($data);
	}

	function get_account_number_select() {
		$data = [];
		foreach ($this->model_user->select("SUBSTRING_INDEX(SUBSTRING_INDEX(account_number,'æ',2),'æ',-1) AS SubStr, user_id", [], [], ["account_number" => "asc"]) as $key => $value) {

			$data[] = [
				"id" 	=> $value->user_id,
				"text"	=> $value->SubStr
			];
		}
		return json_encode($data);
	}

	// function query(){
	// 	$this->db->query("UPDATE users SET rank_id=10  WHERE user_id IN (8,11,16,81,139,170,250,259,263,289,349,404,512,574,578,591,596,644,781,821,853,908,922,946,1006,1080,1088,1142,1145,1183,1197,1221,1242,1258,1285,1344,1375,1414,1420,1439,1503,1548,1564,1631,1650,1668,1707,1745,1758,1759,1783,1869,1884,1906,1962,2240,2293,2324,2329,2344,2413,2436,2465,2688,2691,2745,2748,2779,2785,2848,2860,2871,2891,2937,3019,3054,3105,3110,3163,3431,3432,3464,3521,3526,3539,3587,3635,3673,3698,3726,3813,3866,3893,3895,3939,3950,3966,4033,4216,4243,4253,4267,4300,4375,4419,4724,4733,4737,4765,4768,4787,4815,4852,4976,5036,5053,5149,5158,5185,5202,5224,5239,5259,5291,5299,5377,5385,5391,5395,5405,5459,5470,5495,5507,5544,5552,5722,5730,5819,5951,5960,5976,5993,6001,6082,6091,6109,6142)");
	// }
	// 
	
	// function account_to_user(){
	// 	$data = $this->db->query("UPDATE users SET username=account_number");
	// }

}

/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */