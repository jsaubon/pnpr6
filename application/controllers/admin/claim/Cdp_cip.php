<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdp_cip extends Core_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->redirect();
        $this->load->library('excel'); 
        $this->load->model('model_claim_cdp');
        $this->load->model('model_claim_cdp_temp');
        $this->load->model('model_claim_cip');
        $this->load->model('model_claim_cip_temp');
        $this->load->model('model_rank');
        $this->load->model('model_permission');
    }

    public function index()
    {
        $page_data = $this->system();
        $page_data += [
            "page_title"    => "CDP/CIP Claims",
            "content_title" => "<strong>Claim for CDP/CIP</strong> Page",
            "permission"    => $this->check_user_permission("Admin CDP CIP Claims"),
            "content_data"  => [$this->load->view("interface/admin/claim/Cdp_cip", [
                                "users" => $this->get_user_select()
                            ], TRUE)]
        ];
        $this->create_page($page_data);
    }

    function get_cdp_claims()
    {
        $data = ["data" => []];

        $where = [];

        $af_user_id = $this->input->post("af_user_id");
        $af_date_paid = $this->input->post("af_date_paid");

        if ($af_user_id != null) {
            $where += ["user_id" => $af_user_id];
        }
        if ($af_date_paid != null) {
            $where += ["date_paid" => $af_date_paid];
        }

        foreach ($this->model_claim_cdp->select("*", $where, [], ["status" => "asc"], []) as $key => $value) {

            $id = $value->claim_cdp_id;
            $url_delete = "\"delete_cdp_claim\"";
            $url_edit = "\"get_info_cdp_claim\"";
            $form_id = "\"form_cdp_claim_single\"";
            $tbl_id = "[tbl_claim_cdp]";
            $modal = "modal_cdp_claim_single";

            $status = ['<label class="label label-success" style="font-size:12px;">Request for FUND</label> <button name="btn_status" class="btn btn-primary btn-sm btn-circle pull-right" style="margin-bottom:0px" title="Claim" onclick="cdp_change_status('.$id.')"><i class="fa fa-thumbs-up"></i></button>', '<label class="label label-primary block" style="font-size:12px;">Credited to ATM</label>'];

            $data["data"][] = [
                $this->full_name($value->user_id),
                number_format($value->amount, 2),
                date("M d, Y", strtotime($value->date_paid)),
                $status[$value->status],
                "<div class='text-center'>
                    <button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
                    <button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
                </div>"
            ];
        }
        echo json_encode($data);
    }

    function get_info_cdp_claim() {
        $data = [];
        foreach ($this->model_claim_cdp->select("*", ["claim_cdp_id" => $this->input->post("value")]) as $key => $value) {
            $data = [
                "claim_cdp_id"  => $value->claim_cdp_id,
                "user_id"       => $value->user_id,
                "date_paid"     => date("Y-m-d", strtotime($value->date_paid)),
                "amount"        => $value->amount,
                "status"        => $value->status,
            ];
        }
        echo json_encode($data);
    }

    function delete_cdp_claim() {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $this->user_log("Deleted claim CDP '".$this->get_cdp_user_id($this->input->post("value"))."'");
        
        if ($this->model_claim_cdp->delete(["claim_cdp_id" => $this->input->post("value")])) {

            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];          
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
        echo json_encode($ret);
    }

    function get_cdp_user_id($claim_cdp_id)
    {
        $data = "";
        foreach ($this->model_claim_cdp->select("user_id", ["claim_cdp_id" => $claim_cdp_id]) as $key => $value) {
            $data = $value->user_id;
        }
        return $data;
    }

    function cdp_change_status(){
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];
        if ($this->model_claim_cdp->update(["status" => 1], ["claim_cdp_id" => $this->input->post("value")])) {
            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];
        }
        echo json_encode($ret);
    }

    function insert_cdp_claim_single()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $process = "";

        $claim_cdp_id   = $this->input->post("claim_cdp_id");
        $alert      = $this->input->post("alert");
        $data = [
            "user_id"           => $this->input->post("user_id"),
            "date_paid"         => $this->input->post("date_paid"),
            "amount"            => $this->input->post("amount"),
            "status"            => $this->input->post("status"),
            "date_approved"     => $this->now()
        ];

        if($claim_cdp_id == null) {
            if ($alert == "ok" && $alert != "") {
                $data += [
                    "created_by"    => $this->session->login_id,
                    "date_created"  => $this->now()
                ];
                $this->model_claim_cdp->insert($data);
                $warning = "success";
            } else {
                $warning = "alert";
            }
            $process = "insert";                
        } else {
            $data += [
                "modified_by"   => $this->session->login_id,
                "date_modified" => $this->now()
            ];
            $this->model_claim_cdp->update($data, ["claim_cdp_id" => $claim_cdp_id]);
            $process = "update";
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];
        }
        else {
            $this->db->trans_commit();
            if ($process == "insert" && $warning == "alert") {

                $ret = [
                    "success"   => false,
                    "msg"       => "<span class='fa fa-warning'></span> You want to add this data?",
                    "alert"     => true
                ];

            } elseif ($process == "insert" && $warning == "success") {

                $this->insertNotfication($this->input->post("user_id"),"You have CDP Claim amount of PHP ".number_format($this->input->post("amount"),2).". <br> Date Paid: ".$this->input->post("date_paid"));
                $this->user_log("Inserted claim CDP '".$this->full_name($this->input->post("user_id"))."'");

                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Success"
                ];  

            } elseif ($process == "update") {

                $this->user_log("Updated claim CDP '".$this->full_name($this->input->post("user_id"))."'");
                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Updated"
                ];

            }
        }
        echo json_encode($ret);
    }

    function insert_cdp_claim_credited()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $upload_alert = $this->input->post("alert");
        $upload_error = "";

        $count = 0;
        if (!empty($_FILES["file"]["name"])) {

            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];

            if ($extension == "xls" || $extension == "xlsx" || $extension == "xml") {
                
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                $count = $object->setActiveSheetIndex(0)->getHighestRow() - 9;  

                if ($upload_alert == "ok" && $upload_alert != null) {

                    try {
                        foreach ($object->getWorksheetIterator() as $worksheet) {
                            
                            $highestRow = $worksheet->getHighestRow();
                            $highestColumn = $worksheet->getHighestColumn(); 

                            $claim_type = $worksheet->getCellByColumnAndRow(5,8)->getValue();

                            $a = $worksheet->getCellByColumnAndRow(0,9)->getValue();
                            $b = $worksheet->getCellByColumnAndRow(1,9)->getValue();
                            $c = $worksheet->getCellByColumnAndRow(2,9)->getValue();
                            $d = $worksheet->getCellByColumnAndRow(3,9)->getValue();
                            $e = $worksheet->getCellByColumnAndRow(4,9)->getValue();
                            $f = $worksheet->getCellByColumnAndRow(5,9)->getValue();
                            $g = $worksheet->getCellByColumnAndRow(6,9)->getValue();
                            $h = $worksheet->getCellByColumnAndRow(7,9)->getValue();
                            $i = $worksheet->getCellByColumnAndRow(8,9)->getValue();

                            $new_claim_type = "";

                            if (strtoupper(preg_replace('/\s+/', '', $claim_type)) == "COMBATDUTYPAY" && $claim_type != "") {
                                
                                if (strtoupper(preg_replace('/\s+/', '', $a)) == "NO." && strtoupper(preg_replace('/\s+/', '', $b)) == "RANK" && strtoupper(preg_replace('/\s+/', '', $c)) == "ACCOUNTNUMBER" && strtoupper(preg_replace('/\s+/', '', $d)) == "LASTNAME" && strtoupper(preg_replace('/\s+/', '', $e)) == "FIRSTNAME" && strtoupper(preg_replace('/\s+/', '', $f)) == "MIDDLENAME" && strtoupper(preg_replace('/\s+/', '', $g)) == "QLFR" && strtoupper(preg_replace('/\s+/', '', $h)) == "AMOUNT" && strtoupper(preg_replace('/\s+/', '', $i)) == "DATEPAID") {
                                    
                                    for ($row=10; $row<=$highestRow; $row++) {

                                        $no                 = $worksheet->getCellByColumnAndRow(0,$row)->getValue();   
                                        $rank_name          = $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
                                        $account_number     = $worksheet->getCellByColumnAndRow(2,$row)->getValue();      
                                        $lastname           = $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
                                        $firstname          = $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
                                        $middlename         = $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
                                        $name_ext           = $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
                                        $amount             = $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 
                                        $date_paid          = $worksheet->getCellByColumnAndRow(8,$row)->getValue(); 

                                        if ($account_number != "") {
                                            $rank_id = "";
                                            if ($this->check_rank_name($rank_name) != 0) {

                                                $rank_id = $this->get_rank_id($rank_name);

                                            } else {

                                                $this->model_rank->insert([
                                                    "rank_name"     => $rank_name,
                                                    "created_by"    => $this->session->login_id,
                                                    "date_created"  => $this->now()
                                                ]);
                                                $rank_id = $this->db->insert_id();

                                            }

                                            $fullname = ucfirst($firstname)." ".ucfirst($middlename[0])." ".ucfirst($lastname)." ".ucfirst($name_ext);

                                            $user_id = "";
                                            if ($this->check_user_exist($account_number) != 0) {

                                                $user_id = $this->get_user_exist_id($account_number);

                                                if ($this->check_claim_cdp($user_id, $date_paid) == 0) {

                                                    $data = [
                                                        "user_id"           => $user_id,
                                                        "date_paid"         => $date_paid,
                                                        "amount"            => $amount,
                                                        "status"            => 1,
                                                        "date_approved"     => $this->now(),
                                                        "created_by"        => $this->session->login_id,
                                                        "date_created"      => $this->now()
                                                    ];
                                                    $this->model_claim_cdp->insert($data);
                                                    $this->user_log("Inserted claim CDP '".$fullname."'");
                                                    $this->insertNotfication($user_id,"You have CDP Claim amount of PHP ".number_format($amount,2).". <br> Date Paid: ".$date_paid);

                                                } else {

                                                    $data = [
                                                        "user_id"           => $user_id,
                                                        "date_paid"         => $date_paid,
                                                        "amount"            => $amount,
                                                        "status"            => 1,
                                                        "date_approved"     => $this->now(),
                                                        "modified_by"       => $this->session->login_id,
                                                        "date_modified"     => $this->now()
                                                    ];
                                                    $this->model_claim_cdp->update($data, ["claim_cdp_id" => $this->get_claim_cdp_id($user_id, $date_paid)]);
                                                    $this->user_log("Updated claim CDP '".$fullname."'");

                                                }

                                            } else {

                                                $data = [
                                                    "account_number"    => $account_number,
                                                    "firstname"         => strtoupper($firstname),
                                                    "middlename"        => strtoupper($middlename),
                                                    "lastname"          => strtoupper($lastname),
                                                    "name_ext"          => strtoupper($name_ext),
                                                    "rank_name"         => $rank_name,
                                                    "date_paid"         => $date_paid,
                                                    "amount"            => $amount,
                                                    "date_approved"     => $this->now(),
                                                    "status"            => 1,
                                                    "is_added"          => 0,
                                                    "created_by"        => $this->session->login_id,
                                                    "date_created"      => $this->now()
                                                ];
                                                $this->model_claim_cdp_temp->insert($data);
                                                $this->user_log("Inserted claim in temp not exist from user '".$fullname."'");

                                            }
                                        }

                                    }
                                } else {
                                    $warning = "combat_duty_pay";
                                }
                            } else {
                                $warning = "combat_duty_pay";
                            }
                            
                        }

                        $warning = "success";

                    } catch (Exception $e) {

                        $warning = "catch_warning";
                        echo $e->getMessaage();

                    }
                } else {

                    $warning = "alert";

                }
               
            } else {
                $warning = "wrong_file";
            }

        }

        if($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];

        }
        else {

            $this->db->trans_commit();

            if ($warning == "alert") {

                $ret = [
                    "success"   => false,
                    "msg"       => "You want to upload these <strong>$count</strong> data?",
                    "alert"     => true,
                    "count"     => "<strong>$count</strong>"
                ];

            } elseif ($warning == "combat_duty_pay") {

                $ret = [
                    "success"       => false,
                    "msg"           => "Your file uploaded is not allowed!",
                    "wrong_file"    => true
                ];

            } elseif ($warning == "wrong_file") {

                $ret = [
                    "success"       => false,
                    "msg"           => "Your file uploaded is not allowed!",
                    "wrong_file"    => true
                ];

            } elseif ($warning == "catch_warning") {

                $ret = [
                    "success"   => false,
                    "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
                ];

            } else {

                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Success"
                ];  

            }

        }

        echo json_encode($ret);
    }

    // checking for CDP
    function check_claim_cdp($user_id, $date_paid)
    {
        $count = 0;
        $where = [
            "user_id" => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cdp->select("COUNT(*) AS count", $where) as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    function get_claim_cdp_id($user_id, $date_paid)
    {
        $data = "";
        $where = [
            "user_id"   => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cdp->select("*", $where) as $key => $value) {
            $data = $value->claim_cdp_id;
        }
        return $data;
    }
    // end of CDP

    // start of CIP
    function get_cip_claims()
    {
        $data = ["data" => []];

        $where = [];

        $af_user_id = $this->input->post("af_user_id");
        $af_date_paid = $this->input->post("af_date_paid");

        if ($af_user_id != null) {
            $where += ["user_id" => $af_user_id];
        }
        if ($af_date_paid != null) {
            $where += ["date_paid" => $af_date_paid];
        }

        foreach ($this->model_claim_cip->select("*", $where, [], ["status" => "asc"], []) as $key => $value) {

            $id = $value->claim_cip_id;
            $url_delete = "\"delete_cip_claim\"";
            $url_edit = "\"get_info_cip_claim\"";
            $form_id = "\"form_cip_claim_single\"";
            $tbl_id = "[tbl_claim_cip]";
            $modal = "modal_cip_claim_single";

            $status = ['<label class="label label-success" style="font-size:12px;">Request for FUND</label> <button name="btn_status" class="btn btn-primary btn-sm btn-circle pull-right" style="margin-bottom:0px" title="Claim" onclick="cip_change_status('.$id.')"><i class="fa fa-thumbs-up"></i></button>', '<label class="label label-primary block" style="font-size:12px;">Credited to ATM</label>'];

            $data["data"][] = [
                $this->full_name($value->user_id),
                number_format($value->amount, 2),
                date("M d, Y", strtotime($value->date_paid)),
                $status[$value->status],
                "<div class='text-center'>
                    <button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
                    <button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
                </div>"
            ];
        }
        echo json_encode($data);
    }

    function get_info_cip_claim() {
        $data = [];
        foreach ($this->model_claim_cip->select("*", ["claim_cip_id" => $this->input->post("value")]) as $key => $value) {
            $data = [
                "claim_cip_id"  => $value->claim_cip_id,
                "user_id"       => $value->user_id,
                "date_paid"     => date("Y-m-d", strtotime($value->date_paid)),
                "amount"        => $value->amount,
                "status"        => $value->status,
            ];
        }
        echo json_encode($data);
    }

    function delete_cip_claim() {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $this->user_log("Deleted claim CIP '".$this->get_cip_user_id($this->input->post("value"))."'");
        
        if ($this->model_claim_cip->delete(["claim_cip_id" => $this->input->post("value")])) {

            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];          
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
        echo json_encode($ret);
    }

    function get_cip_user_id($claim_cip_id)
    {
        $data = "";
        foreach ($this->model_claim_cip->select("user_id", ["claim_cip_id" => $claim_cip_id]) as $key => $value) {
            $data = $value->user_id;
        }
        return $data;
    }

    function cip_change_status(){
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];
        if ($this->model_claim_cip->update(["status" => 1], ["claim_cip_id" => $this->input->post("value")])) {
            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];
        }
        echo json_encode($ret);
    }

    function insert_cip_claim_single()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $process = "";

        $claim_cip_id   = $this->input->post("claim_cip_id");
        $alert      = $this->input->post("alert");
        $data = [
            "user_id"           => $this->input->post("user_id"),
            "date_paid"         => $this->input->post("date_paid"),
            "amount"            => $this->input->post("amount"),
            "status"            => $this->input->post("status"),
            "date_approved"     => $this->now()
        ];

        if($claim_cip_id == null) {
            if ($alert == "ok" && $alert != "") {
                $data += [
                    "created_by"    => $this->session->login_id,
                    "date_created"  => $this->now()
                ];
                $this->model_claim_cip->insert($data);
                $warning = "success";
            } else {
                $warning = "alert";
            }
            $process = "insert";                
        } else {
            $data += [
                "modified_by"   => $this->session->login_id,
                "date_modified" => $this->now()
            ];
            $this->model_claim_cip->update($data, ["claim_cip_id" => $claim_cip_id]);
            $process = "update";
        }

        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];
        }
        else {
            $this->db->trans_commit();
            if ($process == "insert") {
                if ($warning == "alert") {
                    $ret = [
                        "success"   => false,
                        "msg"       => "<span class='fa fa-warning'></span> You want to add this data?",
                        "alert"     => true
                    ];
                } else {
                    $this->user_log("Inserted claim CIP '".$this->full_name($this->input->post("user_id"))."'");
                    $this->insertNotfication($this->input->post("user_id"),"You have CIP Claim amount of PHP ".number_format($this->input->post("amount"),2).". <br> Date Paid: ".$this->input->post("date_paid"));
                    $ret = [
                        "success"   => true,
                        "msg"       => "<span class='fa fa-check'></span> Success"
                    ];                  
                }
            } else {
                $this->user_log("Updated claim CIP '".$this->full_name($this->input->post("user_id"))."'");
                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Updated"
                ];
            }
        }
        echo json_encode($ret);
    }

    function insert_cip_claim_credited()
    {
        $this->db->trans_begin();
        $ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];

        $warning = "";
        $upload_alert = $this->input->post("alert");
        $upload_error = "";

        $count = 0;
        if (!empty($_FILES["file"]["name"])) {

            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];

            if ($extension == "xls" || $extension == "xlsx" || $extension == "xml") {
                
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                $count = $object->setActiveSheetIndex(0)->getHighestRow() - 9;  

                if ($upload_alert == "ok" && $upload_alert != null) {

                    try {
                        foreach ($object->getWorksheetIterator() as $worksheet) {
                            
                            $highestRow = $worksheet->getHighestRow();
                            $highestColumn = $worksheet->getHighestColumn(); 
                            $claim_type = $worksheet->getCellByColumnAndRow(5,8)->getValue();

                            $new_claim_type = "";

                            if ($claim_type == "COMBAT INCENTIVE PAY" && $claim_type != "") {
                                for ($row=10; $row<=$highestRow; $row++) {

                                    $no                 = $worksheet->getCellByColumnAndRow(0,$row)->getValue();   
                                    $rank_name          = $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
                                    $account_number     = $worksheet->getCellByColumnAndRow(2,$row)->getValue();      
                                    $lastname           = $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
                                    $firstname          = $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
                                    $middlename         = $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
                                    $name_ext           = $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
                                    $amount             = $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 
                                    $date_paid          = $worksheet->getCellByColumnAndRow(8,$row)->getValue(); 

                                    $rank_id = "";
                                    if ($this->check_rank_name($rank_name) != 0) {

                                        $rank_id = $this->get_rank_id($rank_name);

                                    } else {

                                        $this->model_rank->insert([
                                            "rank_name"     => $rank_name,
                                            "created_by"    => $this->session->login_id,
                                            "date_created"  => $this->now()
                                        ]);
                                        $rank_id = $this->db->insert_id();

                                    }

                                    $fullname = ucfirst($firstname)." ".ucfirst($middlename[0])." ".ucfirst($lastname)." ".ucfirst($name_ext);

                                    $user_id = "";
                                    if ($this->check_user_exist($account_number) != 0) {

                                        $user_id = $this->get_user_exist_id($account_number);

                                        if ($this->check_claim_cip($user_id, $date_paid) == 0) {

                                            $data = [
                                                "user_id"           => $user_id,
                                                "date_paid"         => $date_paid,
                                                "amount"            => $amount,
                                                "status"            => 1,
                                                "date_approved"     => $this->now(),
                                                "created_by"        => $this->session->login_id,
                                                "date_created"      => $this->now()
                                            ];
                                            $this->model_claim_cip->insert($data);
                                            $this->user_log("Inserted claim CIP '".$fullname."'");
                                            $this->insertNotfication($user_id,"You have CIP Claim amount of PHP ".number_format($amount,2).". <br> Date Paid: ".$date_paid);

                                        } else {

                                            $data = [
                                                "user_id"           => $user_id,
                                                "date_paid"         => $date_paid,
                                                "amount"            => $amount,
                                                "status"            => 1,
                                                "date_approved"     => $this->now(),
                                                "modified_by"       => $this->session->login_id,
                                                "date_modified"     => $this->now()
                                            ];
                                            $this->model_claim_cip->update($data, ["claim_cip_id" => $this->get_claim_cip_id($user_id, $date_paid)]);
                                            $this->user_log("Updated claim CIP '".$fullname."'");

                                        }

                                    } else {

                                        $data = [
                                            "account_number"    => $account_number,
                                            "firstname"         => strtoupper($firstname),
                                            "middlename"        => strtoupper($middlename),
                                            "lastname"          => strtoupper($lastname),
                                            "name_ext"          => strtoupper($name_ext),
                                            "rank_id"           => $rank_id,
                                            "date_paid"         => $date_paid,
                                            "amount"            => $amount,
                                            "date_approved"     => $this->now(),
                                            "status"            => 1,
                                            "is_added"          => 0,
                                            "created_by"        => $this->session->login_id,
                                            "date_created"      => $this->now()
                                        ];
                                        $this->model_claim_cip_temp->insert($data);
                                        $this->user_log("Inserted claim in temp not exist from user '".$fullname."'");

                                    }

                                }
                            } else {
                                $warning = "combat_duty_pay";
                            }
                            
                        }

                        $warning = "success";

                    } catch (Exception $e) {

                        $warning = "catch_warning";
                        echo $e->getMessaage();

                    }
                } else {

                    $warning = "alert";

                }
               
            } else {
                $warning = "wrong_file";
            }

        }

        if($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            $ret = [
                "success"   => false,
                "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
            ];

        }
        else {

            $this->db->trans_commit();

            if ($warning == "alert") {

                $ret = [
                    "success"   => false,
                    "msg"       => "You want to upload these <strong>$count</strong> data?",
                    "alert"     => true,
                    "count"     => "<strong>$count</strong>"
                ];

            } elseif ($warning == "combat_duty_pay") {

                $ret = [
                    "success"       => false,
                    "msg"           => "Your file uploaded is not allowed!",
                    "wrong_file"    => true
                ];

            } elseif ($warning == "wrong_file") {

                $ret = [
                    "success"       => false,
                    "msg"           => "Your file uploaded is not allowed!",
                    "wrong_file"    => true
                ];

            } elseif ($warning == "catch_warning") {

                $ret = [
                    "success"   => false,
                    "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
                ];

            } else {

                $ret = [
                    "success"   => true,
                    "msg"       => "<span class='fa fa-check'></span> Success"
                ];  

            }

        }

        echo json_encode($ret);
    }

    // checking for CIP
    function check_claim_cip($user_id, $date_paid)
    {
        $count = 0;
        $where = [
            "user_id" => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cip->select("COUNT(*) AS count", $where) as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    function get_claim_cip_id($user_id, $date_paid)
    {
        $data = "";
        $where = [
            "user_id"   => $user_id,
            "date_paid" => $date_paid
        ];
        foreach ($this->model_claim_cip->select("*", $where) as $key => $value) {
            $data = $value->claim_cip_id;
            break;
        }
        return $data;
    }
    // end of CIP

    // checking for CDP and CIP
    function check_rank_name($rank_name)
    {
        $count = 0;
        $where = [
            "rank_name" => $rank_name
        ];
        foreach ($this->model_rank->select("COUNT(rank_name) AS count", $where) as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    function get_rank_id($rank_name)
    {
        $rank_id = "";
        $where = [
            "rank_name" => $rank_name
        ];
        foreach ($this->model_rank->select("rank_id", $where) as $key => $value) {
            $rank_id = $value->rank_id;
            break;
        }
        return $rank_id;
    }

    function check_user_exist($account_number)
    {
        $count = 0;
        foreach ($this->model_user->query("SELECT COUNT(*) AS count FROM users WHERE account_number='".md5($account_number).'æ'.$account_number.'æ'.md5($account_number)."' OR atm_number='".md5($account_number).'-'.$account_number.'-'.md5($account_number)."'")->result() as $key => $value) {
            $count = $value->count;
        }
        return $count;
    }
    function get_user_exist_id($account_number)
    {
        $data = "";
        foreach ($this->model_user->query("SELECT user_id FROM users WHERE account_number='".md5($account_number).'æ'.$account_number.'æ'.md5($account_number)."' OR atm_number='".md5($account_number).'-'.$account_number.'-'.md5($account_number)."'")->result() as $key => $value) {
            $data = $value->user_id;
            break;
        }
        return $data;
    }

    // others
    function get_user_select()
    {
        $data = [];
        foreach ($this->model_user->select("*", [], [], ["lastname" => "asc"]) as $key => $value) {
            $data[] = [
                "id"    => $value->user_id,
                "text"  => $value->lastname.", ".$value->firstname." ".$value->middlename." ".$value->name_ext
            ];
        }
        return json_encode($data);
    }

}

/* End of file Cdp_cip.php */
/* Location: ./application/controllers/admin/addClaim/Cdp_cip.php */