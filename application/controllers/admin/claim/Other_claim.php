<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Other_claim extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->library('excel'); 
		$this->load->model('model_claim_other');
		$this->load->model('model_claim_other_temp');
		$this->load->model('model_rank');
		$this->load->model('model_permission');
		$this->load->model('model_type_of_claim');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Other Claims",
			"content_title"	=> "<strong>Claim for Other Claim</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Other Claims"),
			"content_data"	=> [$this->load->view("interface/admin/claim/Other_claim", [
								"users"				=> $this->get_user_select(),
								"ranks"				=> $this->get_ranks_select(),
								"type_of_claims"	=> $this->get_type_of_claim_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_other_claims()
	{
		$data = ["data" => []];

		$where = [];

		$af_user_id = $this->input->post("af_user_id");
		$af_status = $this->input->post("af_status");
		$af_date_requested = $this->input->post("af_date_requested");
		$af_date_approved = $this->input->post("af_date_approved");

		if ($af_user_id != null) {
			$where += ["user_id" => $af_user_id];
		}
		if ($af_status != null) {
			$where += ["status" => $af_status];
		}
		if ($af_date_approved != null) {
			$where += ["date_approved" => $af_date_approved];
		}
		if ($af_date_approved != null) {
			$where += ["date_approved" => $af_date_approved];
		}

		foreach ($this->model_claim_other->select("*", $where, [
			"type_of_claims" => "claim_others.type_of_claim_id = type_of_claims.type_of_claim_id"
		], ["status" => "asc"]) as $key => $value) {

			$id = $value->claim_other_id;
			$url_delete = "\"delete_other_claim\"";
			$url_edit = "\"get_info_other_claim\"";
			$form_id = "\"form_other_claim_single\"";
			$tbl_id = "[tbl_claim_other]";
			$modal = "modal_other_claim_single";

			$status = ['<label class="label label-success" style="font-size:12px;">Request for FUND</label> <button name="btn_status" class="btn btn-primary btn-sm btn-circle pull-right" style="margin-bottom:0px" title="Claim" onclick="change_status('.$id.')"><i class="fa fa-thumbs-up"></i></button>', '<label class="label label-primary block" style="font-size:12px;">Credited to ATM</label>'];

			$data["data"][] = [
				$this->full_name($value->user_id),
				$value->type_of_claim,
				$value->period_covered,
				"PHP ".number_format($value->amount, 2),
				$status[$value->status],
				"<div class='text-center'>
					<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
					<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>"
			];
		}
		echo json_encode($data);
	}

	function get_claim_temp_other_claims()
	{
		$data = [];
		$where = [
			"claim_other_temps.created_by" 	=> $this->session->login_id,
			"is_added"		=> 0
		];
		foreach ($this->model_claim_other_temp->select("*", $where, [
			"type_of_claims" => "claim_other_temps.type_of_claim_id = type_of_claims.type_of_claim_id"
		]) as $key => $value) {
			$data[] = [
				"claim_other_temp_id"	=> $value->claim_other_temp_id,
				"account_number"		=> $value->account_number,
				"type_of_claim_id"		=> $value->type_of_claim_id,
				"type_of_claim"			=> $value->type_of_claim,
				"period_covered"		=> $value->period_covered,
				"amount"				=> number_format($value->amount, 2),
				"status"				=> $value->status,
				"date_requested"		=> $value->date_requested,
				"date_approved"			=> $value->date_approved,
			];
		}
		echo json_encode($data);
	}

	function get_info_other_claim() {
		$data = [];
		foreach ($this->model_claim_other->select("*", ["claim_other_id" => $this->input->post("value")]) as $key => $value) {
			$data = [
				"claim_other_id" 	=> $value->claim_other_id,
				"user_id"			=> $value->user_id,
				"type_of_claim_id"	=> $value->type_of_claim_id,
				"period_covered"	=> $value->period_covered,
				"date_requested"	=> $value->date_requested,
				"date_approved"		=> $value->date_approved,
				"amount"			=> $value->amount,
				"status"			=> $value->status,
			];
		}
		echo json_encode($data);
	}

	function delete_other_claim() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$this->user_log("Deleted other claim '".$this->get_claim_other_user_id($this->input->post("value"))."'");
		
		if ($this->model_claim_other->delete(["claim_other_id" => $this->input->post("value")])) {

		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];			
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	function change_status()
	{
		$ret = [
            "success"   => false,
            "msg"       => "<span class='fa fa-warning'></span> Something went wrong"
        ];
        if ($this->model_claim_other->update(["status" => 1], ["claim_other_id" => $this->input->post("value")])) {
            $ret = [
                "success"   => true,
                "msg"       => "<span class='fa fa-check'></span> Success"
            ];
        }
        echo json_encode($ret);
	}

	function insert_other_claim_single()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$warning = "";
		$process = "";

		$claim_other_id 	= $this->input->post("claim_other_id");
		$alert 				= $this->input->post("alert");

		$data = [
			"user_id" 			=> $this->input->post("user_id"),
			"type_of_claim_id" 	=> $this->input->post("type_of_claim_id"),
			"period_covered" 	=> $this->input->post("period_covered"),
			"amount" 			=> $this->input->post("amount"),
			"status" 			=> $this->input->post("status"),
			"date_requested"	=> $this->input->post("date_requested"),
			"date_approved"		=> $this->input->post("date_approved"),
		];

		if($claim_other_id == null) {
			if ($alert == "ok" && $alert != "") {
				$data += [
					"created_by"	=> $this->session->login_id,
					"date_created"	=> $this->now()
				];
				$this->model_claim_other->insert($data);
				$warning = "success";
			} else {
				$warning = "alert";
			}				
			$process = "insert";				
		} else {
			$data += [
				"modified_by"	=> $this->session->login_id,
				"date_modified"	=> $this->now()
			];
			$this->model_claim_other->update($data, ["claim_other_id" => $claim_other_id]);
			$process = "update";
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    if ($process == "insert") {
		    	if ($warning == "alert") {
		    		$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> You want to add this data?",
						"alert"		=> true
					];
		    	} else {
		    		$this->user_log("Inserted other claims '".$this->full_name($this->input->post("user_id"))."'");

		    		$this->insertNotfication($this->input->post("user_id"),"You have Other Claim amount of PHP ".number_format($this->input->post("amount"),2).". <br> Type of claim: ".$this->get_type_of_claim_name($this->input->post("type_of_claim_id"))."<br> Period Covered: ".$this->input->post("period_covered"));
		    		$ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Success"
					];		    		
		    	}
	    	} else {
	    		$this->user_log("Updated other claims '".$this->full_name($this->input->post("user_id"))."'");
	    		$ret = [
					"success" 	=> true,
					"msg"		=> "<span class='fa fa-check'></span> Updated"
				];
	    	}
		}
		echo json_encode($ret);
	}

	function insert_other_claim_request_for_fund()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$warning = "";
		$upload_alert = $this->input->post("alert");
		$upload_error = "";

		$count = 0;

		if (!empty($_FILES["file"]["name"])) {

			$path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];

            if ($extension == "xls" || $extension == "xlsx" || $extension == "xml") {

            	$path = $_FILES["file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load($path);
				$count = $object->setActiveSheetIndex(0)->getHighestRow() - 10;	

				if ($upload_alert == "ok" && $upload_alert != null) {

					try {

						foreach ($object->getWorksheetIterator() as $worksheet) {

							$highestRow = $worksheet->getHighestRow();
							$highestColumn = $worksheet->getHighestColumn();

							for ($row=11; $row<=$highestRow; $row++) {

								$name       		= $worksheet->getCellByColumnAndRow(0,$row)->getValue();       
				                $account_number 	= $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
				                $amount				= $worksheet->getCellByColumnAndRow(2,$row)->getValue(); 
				                $type_of_claim		= $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
				                $period_covered		= $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
				                $status				= $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
				                $date_requested		= $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
				                $date_approved		= $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 

				                if ($account_number != "") {
				                	
				                	$new_status = "";
					                if (strtolower(preg_replace('/\s+/', '', $status)) == "requestforfund") {
					                	$new_status = "0";
					                } elseif (strtolower(preg_replace('/\s+/', '', $status)) == "creditedtoatm") {
					                	$new_status = "1";
					                }

					                $type_of_claim_id = "";
					                if ($this->check_type_of_claim($type_of_claim) != 0) {

					                	$type_of_claim_id = $this->get_type_of_claim_id($type_of_claim);

					                } else {

					                	$data_type_of_claim = [
					                		"type_of_claim"	=> $type_of_claim,
					                		"created_by"	=> $this->session->login_id,
					                		"date_created"	=> $this->now()
					                	];
					                	$this->model_type_of_claim->insert($data_type_of_claim);
					                	$type_of_claim_id = $this->db->insert_id();
					                	$this->user_log("Inserted type of claim '".$type_of_claim."'");

					                }
					                $user_id = "";
					                if ($this->check_user_exist($account_number) != 0) {

					                	$user_id = $this->get_user_exist_id($account_number);

					                	if ($this->check_other_claim($user_id, $type_of_claim_id, $period_covered) == 0) {

					                		$data = [
					                			"user_id" 			=> $user_id,
					                			"type_of_claim_id"	=> $type_of_claim_id,
					                			"period_covered" 	=> $period_covered,
					                			"amount"			=> $amount,
					                			"status"			=> $new_status,
					                			"date_requested"	=> $date_requested,
												"created_by"		=> $this->session->login_id,
												"date_created"		=> $this->now()
					                		];
					                		$this->model_claim_other->insert($data);

					                		$this->insertNotfication($user_id,"You have Other Claim amount of PHP ".number_format($amount,2).". <br> Type of claim: ".$this->get_type_of_claim_name($type_of_claim_id)."<br> Period Covered: ".$period_covered);

					                		$this->user_log("Inserted other claims '".$this->full_name($user_id)."'");

					                	}

					                } else {

					                	$data = [
					                		"account_number"	=> $account_number,
					                		"type_of_claim"		=> $type_of_claim,
					                		"period_covered"	=> $period_covered,
					                		"amount"			=> $amount,
				                			"date_requested"	=> $date_requested,
					                		"status"			=> $new_status,
					                		"is_added"			=> 0,
					                		"created_by"		=> $this->session->login_id,
					                		"date_created"		=> $this->now()
					                	];
					                	$this->model_claim_other_temp->insert($data);
					                	$this->user_log("Inserted claims temp '".$account_number."'");

					                }

				                }

							}
						}

						$warning = "success";

					} catch (Exception $e) {

						$warning = "catch_warning";
						$upload_error = $e->getMessaage();

					}

				} else {

					$warning = "alert";

				}

            } else {

            	$warning = "wrong_file";

            }		

		}

		if($this->db->trans_status() === false) {

			$this->db->trans_rollback();

			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];

		}
		else {

		    $this->db->trans_commit();

		    if ($warning == "alert") {

		    	$ret = [
					"success" 	=> false,
					"msg"		=> "You want to upload these <strong>$count</strong> data?",
					"alert"		=> true,
					"count"		=> "<strong>$count</strong>"
				];

		    } elseif ($warning == "wrong_file") {

		    	$ret = [
					"success" 		=> false,
					"msg"			=> "<span class='fa fa-warning'></span> Something went wrong",
					"wrong_file"	=> true
				];

		    } elseif ($warning == "catch_warning") {

		    	$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
				];

		    } else {

			    $ret = [
					"success" 	=> true,
					"msg"		=> "<span class='fa fa-check'></span> Success"
				];	

		    }

		}

		echo json_encode($ret);
	}

	function insert_other_claim_credited()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$warning = "";
		$upload_alert = $this->input->post("alert");
		$upload_error = "";

		$count = 0;

		if (!empty($_FILES["file"]["name"])) {

			$path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];

            if ($extension == "xls" || $extension == "xlsx" || $extension == "xml") {

            	$path = $_FILES["file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load($path);
				$count = $object->setActiveSheetIndex(0)->getHighestRow() - 10;	

				if ($upload_alert == "ok" && $upload_alert != null) {

					try {

						foreach ($object->getWorksheetIterator() as $worksheet) {

							$highestRow = $worksheet->getHighestRow();
							$highestColumn = $worksheet->getHighestColumn();

							for ($row=11; $row<=$highestRow; $row++) {

								$name       		= $worksheet->getCellByColumnAndRow(0,$row)->getValue();       
				                $account_number 	= $worksheet->getCellByColumnAndRow(1,$row)->getValue(); 
				                $amount				= $worksheet->getCellByColumnAndRow(2,$row)->getValue(); 
				                $type_of_claim		= $worksheet->getCellByColumnAndRow(3,$row)->getValue(); 
				                $period_covered		= $worksheet->getCellByColumnAndRow(4,$row)->getValue(); 
				                $status				= $worksheet->getCellByColumnAndRow(5,$row)->getValue(); 
				                $date_requested		= $worksheet->getCellByColumnAndRow(6,$row)->getValue(); 
				                $date_approved		= $worksheet->getCellByColumnAndRow(7,$row)->getValue(); 


				                if ($account_number != "") {
				                	
					                $new_status = "";
					                if (strtolower(preg_replace('/\s+/', '', $status)) == "requestforfund") {
					                	$new_status = "0";
					                } elseif (strtolower(preg_replace('/\s+/', '', $status)) == "creditedtoatm") {
					                	$new_status = "1";
					                }

				                	$type_of_claim_id = "";
					                if ($this->check_type_of_claim($type_of_claim) != 0) {

					                	$type_of_claim_id = $this->get_type_of_claim_id($type_of_claim);

					                } else {

					                	$data_type_of_claim = [
					                		"type_of_claim"	=> $type_of_claim,
					                		"created_by"	=> $this->session->login_id,
					                		"date_created"	=> $this->now()
					                	];
					                	$this->model_type_of_claim->insert($data_type_of_claim);
					                	$type_of_claim_id = $this->db->insert_id();

					                	$this->user_log("Inserted type of claim '".$type_of_claim."'");
					                	
					                }

					                $user_id = "";
				                	if ($this->check_user_exist($account_number) != 0) {

					                	$user_id = $this->get_user_exist_id($account_number);

					                	if ($this->check_other_claim($user_id, $type_of_claim_id, $period_covered) == 0) {

					                		$data = [
					                			"user_id" 			=> $user_id,
					                			"type_of_claim_id" 	=> $type_of_claim_id,
					                			"period_covered" 	=> $period_covered,
					                			"amount"			=> $amount,
					                			"status"			=> $new_status,
					                			"date_requested"	=> $date_requested,
					                			"date_approved"		=> $date_approved,
												"created_by"		=> $this->session->login_id,
												"date_created"		=> $this->now()
					                		];
					                		$this->model_claim_other->insert($data);
					                		$this->user_log("Inserted other claims '".$this->full_name($user_id)."'");
					                		$this->insertNotfication($user_id,"You have Other Claim amount of PHP ".number_format($amount,2).". <br> Type of claim: ".$this->get_type_of_claim_name($type_of_claim_id)."<br> Period Covered: ".$period_covered);

					                	} else {

					                		$data = [
					                			"user_id" 			=> $user_id,
					                			"type_of_claim_id" 	=> $type_of_claim_id,
					                			"period_covered" 	=> $period_covered,
					                			"amount"			=> $amount,
					                			"status"			=> $new_status,
					                			"date_approved"		=> $date_approved,
												"modified_by"		=> $this->session->login_id,
												"date_modified"		=> $this->now()
					                		];
					                		$this->model_claim_other->update($data, ["claim_other_id" => $this->get_other_claim_id($user_id, $type_of_claim_id, $period_covered)]);
					                		$this->user_log("Updated other claim '".$this->full_name($user_id)."'");

					                	}

					                } else {
					                	$data = [
					                		"account_number"	=> $account_number,
				                			"type_of_claim_id" 	=> $type_of_claim_id,
					                		"period_covered"	=> $period_covered,
					                		"amount"			=> $amount,
				                			"date_requested"	=> $this->now(),
				                			"date_approved"		=> $this->now(),
					                		"status"			=> $new_status,
					                		"is_added"			=> 0,
					                		"created_by"		=> $this->session->login_id,
					                		"date_created"		=> $this->now()
					                	];
					                	$this->model_claim_other_temp->insert($data);
					                	$this->user_log("Inserted claims not exist user '".$account_number."'");	

					                }
				                }

							}
						}

						$warning = "success";

					} catch (Exception $e) {

						$warning = "catch_warning";
						$upload_error = $e->getMessaage();

					}

				} else {

					$warning = "alert";

				}

            } else {
            	$warning = "wrong_file";
            }

		}

		if($this->db->trans_status() === false) {

			$this->db->trans_rollback();

			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];

		}
		else {

		    $this->db->trans_commit();

		    if ($warning == "alert") {

		    	$ret = [
					"success" 	=> false,
					"msg"		=> "You want to upload these <strong>$count</strong> data?",
					"alert"		=> true,
					"count"		=> "<strong>$count</strong>"
				];

		    } elseif ($warning == "wrong_file") {

		    	$ret = [
					"success" 		=> false,
					"msg"			=> "<span class='fa fa-warning'></span> Something went wrong",
					"wrong_file"	=> true
				];

		    } elseif ($warning == "catch_warning") {

		    	$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
				];

		    } else {

			    $ret = [
					"success" 	=> true,
					"msg"		=> "<span class='fa fa-check'></span> Success"
				];	

		    }

		}

		echo json_encode($ret);
	}

	function insert_claim_temp_to_user_and_claim()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$warning = "";
		$process = "";

		$claim_temp_id 	= $this->input->post("claim_temp_id");
		$alert 			= $this->input->post("alert");

		$firstname = $this->input->post("firstname");
		$middlename = $this->input->post("middlename");
		$lastname = $this->input->post("lastname");
		$name_ext = $this->input->post("name_ext");

		$user_id = "";

		$data = [
			"firstname" 		=> $this->input->post("firstname"),
			"middlename" 		=> $this->input->post("middlename"),
			"lastname" 			=> $this->input->post("lastname"),
			"name_ext" 			=> $this->input->post("name_ext"),
			"rank_id" 			=> $this->input->post("rank_id"),
			"account_number" 	=> $this->input->post("account_number"),
			"user_role_id" 		=> 3,
			"status" 			=> 1,
        	"badge_number"		=> ucfirst($firstname[0]).ucfirst($middlename[0]).ucfirst($lastname),
        	"username"			=> strtolower($firstname),
        	"password"			=> md5(strtolower($lastname)),
		];

		if ($alert == "ok" && $alert != "") {

			$data += [
				"created_by"	=> $this->session->login_id,
				"date_created"	=> $this->now()
			];
			$this->model_user->insert($data);
			$user_id = $this->db->insert_id();

			$data_claim = [
				"user_id"			=> $user_id,
				"amount" 			=> $this->input->post("amount"),
				"type_of_claim_id" 	=> $this->get_type_of_claim_id($this->input->post("type_of_claim")),
				"period_covered" 	=> $this->input->post("period_covered"),
				"date_requested" 	=> $this->input->post("date_requested"),
				"date_approved" 	=> $this->input->post("date_approved"),
				"status" 			=> $this->input->post("status"),
				"claim_code" 		=> $this->input->post("claim_code")
			];
			$this->model_claim->insert($data_claim);

			// $this->model_permission->insert([
			// 	"user_id"		=> $user_id,
			// 	"mod_button_id"	=> 21,
			// 	"status"		=> 1
			// ]);

			// $this->model_permission->insert([
			// 	"user_id"		=> $user_id,
			// 	"mod_button_id"	=> 22,
			// 	"status"		=> 1
			// ]);

			// $this->model_permission->insert([
			// 	"user_id"		=> $user_id,
			// 	"mod_button_id"	=> 23,
			// 	"status"		=> 1
			// ]);

			// $this->model_permission->insert([
			// 	"user_id"		=> $user_id,
			// 	"mod_button_id"	=> 24,
			// 	"status"		=> 1
			// ]);

			$this->model_claim_other_temp->update(["is_added" => 1], ["claim_temp_id" => $claim_temp_id]);

			$warning = "success";

		} else {

			$warning = "alert";

		}

		if($this->db->trans_status() === false) {

			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];

		}
		else {

		    $this->db->trans_commit();

		    if ($warning == "alert") {

	    		$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> You want to add this data?",
					"alert"		=> true
				];

	    	} else {

	    		$this->user_log("Inserted users '".$this->full_name($user_id)."'");
	    		$this->user_log("Inserted claims '".$this->full_name($user_id)."'");
	    		$ret = [
					"success" 	=> true,
					"msg"		=> "<span class='fa fa-check'></span> Success"
				];	

	    	}

		}
		echo json_encode($ret);
	}

	// checking
	function check_user_exist($account_number)
	{
		$count = 0;
		foreach ($this->model_user->query("SELECT COUNT(*) AS count FROM users WHERE account_number='".md5($account_number).'æ'.$account_number.'æ'.md5($account_number)."' OR atm_number='".md5($account_number).'-'.$account_number.'-'.md5($account_number)."'")->result() as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_user_exist_id($account_number)
	{
		$data = "";
		foreach ($this->model_user->query("SELECT user_id FROM users WHERE account_number='".md5($account_number).'æ'.$account_number.'æ'.md5($account_number)."' OR atm_number='".md5($account_number).'-'.$account_number.'-'.md5($account_number)."'")->result() as $key => $value) {
			$data = $value->user_id;
			break;
		}
		return $data;
	}

	function check_other_claim($user_id, $type_of_claim_id, $period_covered)
	{
		$count = 0;
		$where = [
			"user_id" 			=> $user_id,
			"type_of_claim_id"	=> $type_of_claim_id,
			"period_covered" 	=> $period_covered
		];
		foreach ($this->model_claim_other->select("COUNT(*) AS count", $where) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_other_claim_id($user_id, $type_of_claim_id, $period_covered)
	{
		$data = "";
		$where = [
			"user_id"			=> $user_id,
			"type_of_claim_id"	=> $type_of_claim_id,
			"period_covered"	=> $period_covered
		];
		foreach ($this->model_claim_other->select("*", $where) as $key => $value) {
			$data = $value->claim_other_id;
			break;
		}
		return $data;
	}
	function check_type_of_claim($type_of_claim)
	{
		$count = 0;
		foreach ($this->model_type_of_claim->select("COUNT(type_of_claim) AS count", ["type_of_claim" => $type_of_claim]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}
	function get_type_of_claim_id($type_of_claim)
	{
		$data = "";
		foreach ($this->model_type_of_claim->select("type_of_claim_id", ["type_of_claim" => $type_of_claim]) as $key => $value) {
			$data = $value->type_of_claim_id;
		}
		return $data;
	}

	// others
	function get_user_select()
	{
		$data = [];
		foreach ($this->model_user->select("*, SUBSTRING_INDEX(SUBSTRING_INDEX(account_number,'æ',2),'æ',-1) AS SubStr", [], [], ["lastname" => "asc"]) as $key => $value) {
			$data[] = [
				"id"	=> $value->user_id,
				"text"	=> $this->full_name($value->user_id),
				"col1"	=> $value->SubStr
			];
		}
		return json_encode($data);
	}

	function get_ranks_select()
	{
		$data = [];
		foreach ($this->model_rank->select("*", [], [], ["rank_name" => "asc"]) as $key => $value) {
			$data[] = [
				"id"	=> $value->rank_id,
				"text"	=> $value->rank_name
			];
		}
		return json_encode($data);
	}

	function get_type_of_claim_select()
	{
		$data = [];
		foreach ($this->model_type_of_claim->select("*", [], [], ["type_of_claim" => "asc"]) as $key => $value) {
			$data[] = [
				"id"	=> $value->type_of_claim_id,
				"text"	=> $value->type_of_claim
			];
		}
		return json_encode($data);
	}

	function get_claim_other_user_id($claim_other_id)
	{
		$data = "";
		foreach ($this->model_claim_other->select("*", ["claim_other_id" => $claim_other_id]) as $key => $value) {
			$data = $this->full_name($value->user_id);
			break;
		}
		return $data;
	}

	function get_type_of_claim_name($type_of_claim_id){
		$data = "";
		foreach ($this->model_type_of_claim->select("*", ["type_of_claim_id" => $type_of_claim_id]) as $key => $value) {
			$data = $value->type_of_claim;
			break;
		}
		return $data;
	}

}

/* End of file Other_claim.php */
/* Location: ./application/controllers/admin/claim/Other_claim.php */