<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_module');
		$this->load->model('model_module_button');
		$this->load->model('model_permission');
		$this->load->model('model_user');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Permission",
			"content_title"	=> "<strong>Permission</strong> Page",
			"permission" 	=> $this->check_user_permission("Admin Permissions"),
			"content_data"	=> [$this->load->view('interface/admin/permission/Main_permission', [
					"users" => $this->get_user_select()
				], true)]
		];
		$this->create_page($page_data);
	}

	//Permissions
	public function get_permission_mod1() {
		$data = ["data" => []];
		$user_id = $this->input->post("user_id");
		if($user_id != null) {
			foreach ($this->model_module->select("*", ["system_id" => 1]) as $key => $value) {
				$id = $value->module_id;
				$url = "\"admin/Permission/delete_module\"";
				$tbl_id = "[tbl_module]";

				$data["data"][$key] = [
					$value->module_name,
					$this->get_permission_mod_btns($id, $user_id)
				];
			}
		}
		echo json_encode($data);
	}

	public function get_permission_mod2() {
		$data = ["data" => []];
		$user_id = $this->input->post("user_id");
		if($user_id != null) {
			foreach ($this->model_module->select("*", ["system_id" => 2]) as $key => $value) {
				$id = $value->module_id;
				$url = "\"admin/Permission/delete_module\"";
				$tbl_id = "[tbl_module]";

				$data["data"][$key] = [
					$value->module_name,
					$this->get_permission_mod_btns($id, $user_id)
				];
			}
		}
		echo json_encode($data);
	}

	public function get_permission_mod3() {
		$data = ["data" => []];
		$user_id = $this->input->post("user_id");
		if($user_id != null) {
			foreach ($this->model_module->select("*", ["system_id" => 3]) as $key => $value) {
				$id = $value->module_id;
				$url = "\"admin/Permission/delete_module\"";
				$tbl_id = "[tbl_module]";

				$data["data"][$key] = [
					$value->module_name,
					$this->get_permission_mod_btns($id, $user_id)
				];
			}
		}
		echo json_encode($data);
	}

	public function get_permission_mod4() {
		$data = ["data" => []];
		$user_id = $this->input->post("user_id");
		if($user_id != null) {
			foreach ($this->model_module->select("*", ["system_id" => 4]) as $key => $value) {
				$id = $value->module_id;
				$url = "\"admin/Permission/delete_module\"";
				$tbl_id = "[tbl_module]";

				$data["data"][$key] = [
					$value->module_name,
					$this->get_permission_mod_btns($id, $user_id)
				];
			}
		}
		echo json_encode($data);
	}

	public function get_permission_mod_btns($module_id, $user_id) {
		$is_check_all = $this->is_check_all($module_id, $user_id);
		$btn = "<table>
					<tr>
						<td>
							<input class='check' type='checkbox' id='check_all".$module_id."' ".( $is_check_all == true ? "checked" : "" )." onchange='change_status_all(this, $user_id, $module_id)'>
							<label for='check_all".$module_id."'><span></span></label>
						</td>
						<td><b>Select/Deselect All</b></td>
					</tr>";
		foreach ($this->model_module_button->select("*", ["module_id" => $module_id]) as $key => $value) {
			$status = $this->user_permission($user_id, $value->mod_button_id);
			$btn .= "<tr>
						<td>
							<input class='check' type='checkbox' id='check".$value->mod_button_id."' ".( $status == 1 ? "checked" : "" )." onchange='change_status(this, $user_id, $value->mod_button_id)'>
							<label for='check".$value->mod_button_id."'><span></span></label>
						</td>
						<td>".$value->button_name."</td>
					</tr>";
		}
		$btn .= "</table>";
		return $btn;
	}

	public function user_permission($user_id, $mod_button_id) {
		$status = 0;
		foreach ($this->model_permission->select("status", ["user_id" => $user_id, "mod_button_id" => $mod_button_id]) as $key => $value) {
			$status = $value->status;
		}
		return $status;
	}

	public function change_status() {
		$this->db->trans_begin();

		$permission_id = "";
		foreach ($this->model_permission->select("permission_id", ["user_id" => $this->input->post("user_id"), "mod_button_id" => $this->input->post("mod_button_id")]) as $key => $value) {
			$permission_id = $value->permission_id;
		}
		$data = ["status" => $this->input->post("status")];
		if($permission_id == null) {
			$data += [
				"user_id"		=> $this->input->post("user_id"),
				"mod_button_id" => $this->input->post("mod_button_id")
			];
			$this->model_permission->insert($data);
		} else {
			$this->model_permission->update($data, ["permission_id" => $permission_id]);
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	public function change_status_all() {
		$this->db->trans_begin();
		
		foreach ($this->model_module_button->select("mod_button_id", ["module_id" => $this->input->post("module_id")]) as $btn_key => $btn_value) {
			$permission_id = "";
			foreach ($this->model_permission->select("permission_id", ["user_id" => $this->input->post("user_id"), "mod_button_id" => $btn_value->mod_button_id]) as $key => $value) {
				$permission_id = $value->permission_id;
			}
			$data = ["status" => $this->input->post("status")];
			if($permission_id == null) {
				$data += [
					"user_id"		=> $this->input->post("user_id"),
					"mod_button_id" => $btn_value->mod_button_id
				];
				$this->model_permission->insert($data);
			} else {
				$this->model_permission->update($data, ["permission_id" => $permission_id]);
			}
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	public function is_check_all($module_id, $user_id) {
		$ret = false;
		$count_btn = 0;
		$count_allowed = 0;
		foreach ($this->model_module_button->select("mod_button_id", ["module_id" => $module_id]) as $btn_key => $btn_value) {
			$count_btn++;
			foreach ($this->model_permission->select("COUNT(*) As count", ["user_id" => $user_id, "mod_button_id" => $btn_value->mod_button_id, "status" => 1]) as $key => $value) {
				$count_allowed += $value->count;
			}
		}
		if($count_btn == $count_allowed) {
			$ret = true;
		}
		return $ret;
	}

	public function get_user_select() {
		$data = [];
		foreach ($this->model_user->select("*", [], [], ["username" => "asc"]) as $key => $value) {
			$data[] = [
				"id" 	=> $value->user_id,
				"value" => $value->username
			];
		}
		return json_encode($data);
	}
	//End


	//Modules
	public function get_modules() {
		$data = ["data" => []];
		foreach ($this->model_module->select("*", ["system_id" => $this->input->post("system_id")]) as $key => $value) {
			$id = $value->module_id;
			$url = "\"admin/Permission/delete_module\"";
			$tbl_id = "[tbl_module, tbl_permission_mod1, tbl_permission_mod2, tbl_permission_mod3, tbl_permission_mod4]";

			$data["data"][] = [
				'<input type="text" value="'.$value->module_name.'" class="form-control input-sm module_name" style="width: 100%;">',
				"<div class='text-center btn-group'>
					<button class='btn btn-xs' onclick='edit_module(this, $id)' title='Save changes'><span class='fa fa-check'></span></button>
					<button class='btn btn-xs' onclick='delete_this($url, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>",
				$this->get_module_buttons($id)
			];
		}
		echo json_encode($data);
	}

	public function get_module_buttons($module_id) {
		$tbl = "<div class='form_mod_btn' style='margin-left: 3px;'>
					<input type='text' placeholder='Button Name' name='button_name' class='form-control input-sm button_name'>
					<input type='text' placeholder='Button Code' name='button_code' class='form-control input-sm button_code' onkeypress='add_button_enter(this, $module_id)'>
					<button class='btn btn-xs' id='mod_btn_btnAdd' style='margin-left: 3px;' onclick='add_button(this, $module_id)' title='Add'><span class='fa fa-plus'></span></button>
				</div>
				<table>";
		foreach ($this->model_module_button->select("*", ["module_id" => $module_id]) as $key => $value) {
			$id = $value->mod_button_id;
			$url = "\"admin/Permission/delete_mod_button\"";
			$tbl_id = "[tbl_module, tbl_permission_mod1, tbl_permission_mod2, tbl_permission_mod3, tbl_permission_mod4]";

			$tbl .= "<tr>
						<td style='padding: 3px;'>
							<input type='text' value='$value->button_name' class='form-control input-sm button_name' style='width: 49%;'>
							<input type='text' value='$value->button_code' class='form-control input-sm button_code' style='width: 50%;'>
						</td>
						<td style='padding: 3px;'>
							<div class='text-center btn-group'>
								<button class='btn btn-xs' onclick='edit_mod_button(this, $id)' title='Save changes'><span class='fa fa-check'></span></button>
								<button class='btn btn-xs' onclick='delete_this($url, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
							</div>
						</td>
					</tr>";
		}
		$tbl .= "</table>";
		return $tbl;
	}

	public function insert_module() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$module_id = $this->input->post("module_id");
		$data = ["module_name" => $this->input->post("module_name")];

		if($module_id == null) {
			$data += [
				"system_id"		=> $this->input->post("system_id")
			];
			$this->model_module->insert($data);

			$module_id = $this->db->insert_id();

			$mod_btn_data = [
				"button_name" 	=> "Add",
				"button_code"	=> "btn_add",
				"module_id"		=> $module_id
			];
			$this->model_module_button->insert($mod_btn_data);

			$mod_btn_data = [
				"button_name" 	=> "Edit",
				"button_code"	=> "btn_edit",
				"module_id"		=> $module_id
			];
			$this->model_module_button->insert($mod_btn_data);

			$mod_btn_data = [
				"button_name" 	=> "Delete",
				"button_code"	=> "btn_delete",
				"module_id"		=> $module_id
			];
			$this->model_module_button->insert($mod_btn_data);

			$mod_btn_data = [
				"button_name" 	=> "View",
				"button_code"	=> "view_page",
				"module_id"		=> $module_id
			];
			$this->model_module_button->insert($mod_btn_data);

		} else {
			$this->model_module->update($data, ["module_id" => $module_id]);
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];
		}
		echo json_encode($ret);
	}

	public function delete_module() {
		$this->db->trans_begin();

		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$this->model_module_button->delete(["module_id" => $this->input->post("value")]);
		$this->model_module->delete(["module_id" => $this->input->post("value")]);

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];
		}
		echo json_encode($ret);
	}

	public function insert_module_btn() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$mod_button_id = $this->input->post("mod_button_id");
		$data = [
				"button_name" => $this->input->post("button_name"),
				"button_code" => $this->input->post("button_code")
			];

		if($mod_button_id == null) {
			$data += [
				"module_id"		=> $this->input->post("module_id")
			];
			$this->model_module_button->insert($data);
		} else {
			$this->model_module_button->update($data, ["mod_button_id" => $mod_button_id]);
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];
		}
		echo json_encode($ret);
	}

	public function delete_mod_button() {
		$this->db->trans_begin();

		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];


		$this->model_permission->delete(["mod_button_id" => $this->input->post("value")]);
		$this->model_module_button->delete(["mod_button_id" => $this->input->post("value")]);

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$ret = [
				"success" 	=> false,
				"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
			];
		}
		else {
		    $this->db->trans_commit();
		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];
		}
		echo json_encode($ret);
	}
	//End

}

/* End of file Permission.php */
/* Location: ./application/controllers/permission/Permission.php */