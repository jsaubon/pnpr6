<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Memo_and_update extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model("model_memo_and_update");
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Memo and Updates",
			"content_title"	=> "<strong>Memo and Updates</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Memo and Updates"),
			"content_data"	=> [$this->load->view("interface/admin/Memo_and_update", [
								// "badge_numbers"	=> $this->get_badge_number_select()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_memos()
	{
		$data = ["data" => []];
		
		$where = [
			"file_group_name" => 1
		];

		foreach ($this->model_memo_and_update->select("*", $where, [], ["memo_and_update_id" => "desc"]) as $key => $value) {

			$id = $value->memo_and_update_id;

			$url_delete = "\"delete_memo_and_update\"";
			$url_edit = "\"get_info_memo_update\"";
			$form_id = "\"form_memo\"";
			$tbl_id = "[tbl_memo]";
			$modal = "modal_memo";

			$exploded = explode(".", $value->location_path);
			$file_type = $exploded[1];

			$file = '';
			if ($file_type == "pdf") {
				$file = '<object width="120" height="120" data="'.base_url().$value->location_path.'"></object>';
			} else {
				$file = '<img src="'.base_url().$value->location_path.'" class="img-thumbnail" style="width:120px;">';
			}

			$data["data"][] = [
				$file,
				$value->title,
				$file_type,
				"<div class='text-center'>
					<button class='btn btn-primary btn-circle' name='btn_preview' data-toggle='modal' href='#modal_memo_and_update_preview' onclick='get_memo_and_update_preview($id)' title='Preview'><span class='fa fa-eye'></span></button>
					<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
					<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>"
			];
		}
		echo json_encode($data);
	}

	function get_updates()
	{
		$data = ["data" => []];

		$where = [
			"file_group_name" => 2
		];

		foreach ($this->model_memo_and_update->select("*", $where, [], ["memo_and_update_id" => "desc"]) as $key => $value) {
			$id = $value->memo_and_update_id;
			$url_delete = "\"delete_memo_and_update\"";
			$url_edit = "\"get_info_memo_update\"";
			$form_id = "\"form_update\"";
			$tbl_id = "[tbl_update]";
			$modal = "modal_update";

			$exploded = explode(".", $value->location_path);
			$file_type = $exploded[1];

			$file = '';
			if ($file_type == "pdf") {
				$file = '<object width="120" height="120" data="'.base_url().$value->location_path.'"></object>';
			} else {
				$file = '<img src="'.base_url().$value->location_path.'" class="img-thumbnail" style="width:120px;">';
			}

			$data["data"][] = [
				$file,
				$value->title,
				$file_type,
				"<div class='text-center'>
					<button class='btn btn-primary btn-circle' name='btn_preview' data-toggle='modal' href='#modal_memo_and_update_preview' onclick='get_memo_and_update_preview($id)' title='Preview'><span class='fa fa-eye'></span></button>
					<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
					<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>"
			];
		}
		echo json_encode($data);
	}

	function insert_memo()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$memo_and_update_id = $this->input->post("memo_and_update_id");
		$data = [
			"title" 			=> $this->input->post("title"),
			"file_group_name" 	=> 1,
			'status'			=> 1
		];

		if($memo_and_update_id == null) {
			if(!empty($_FILES["file_memo"]["name"])) {
		    	$allowed_types = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];
				if(in_array($_FILES["file_memo"]["type"], $allowed_types)) {
					$data  += [
						"created_by"	=> $this->session->login_id,
						"date_created"	=> $this->now()
					];

					$input_name = "file_memo";
					$upload_path = "../pnpr6/assets/uploads/memo_and_update/memo";
					$file_name = (!empty($_FILES[$input_name]["name"]) ? basename($_FILES[$input_name]["name"]) : "");

					if (pathinfo($file_name)["extension"] == "pdf") {
						$data  += [
							"file_type"	=> 1
						];
					} else {
						$data  += [
							"file_type"	=> 2
						];
					}

					$file_name = "fm_".date("YmdHis").".".pathinfo($file_name)["extension"];
					$path = $this->do_upload($input_name, $upload_path, $file_name);
					$path = "assets/uploads/memo_and_update/memo/".$path;

					$data  += [
						"location_path"	=> $path
					];

					$this->model_memo_and_update->insert($data);

				    $this->user_log("Inserted file memo '".$this->input->post("title")."'");

					$ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Success"
					];
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Invalid file type"
					];
				}
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Please add memo file"
				];
			}
		} 
		else {
			$data += [
				"modified_by"	=> $this->session->login_id,
				"date_modified"	=> $this->now()
			];

			$this->user_log("Updated file memo '".$this->input->post("title")."'");

			$ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Updated"
			];

			if(!empty($_FILES["file_memo"]["name"])) {
		    	$allowed_types = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];
				if(in_array($_FILES["file_memo"]["type"], $allowed_types)) {
			    	$exist = false;
			    	$file_memo = "";
				    foreach ($this->model_memo_and_update->select("location_path", ["memo_and_update_id" => $memo_and_update_id]) as $key => $value) {
				    	$file_memo = $value->location_path;
				    	$exist = true;
				    	break;
				    }

				    $input_name = "file_memo";
					$upload_path = "../pnpr6/assets/uploads/memo_and_update/memo";
					$file_name = (!empty($_FILES[$input_name]["name"]) ? basename($_FILES[$input_name]["name"]) : "");

					if (pathinfo($file_name)["extension"] == "pdf") {
						$data  += [
							"file_type"	=> 1
						];
					} else {
						$data  += [
							"file_type"	=> 2
						];
					}

					$file_name = "fm_".date("YmdHis").".".pathinfo($file_name)["extension"];
					$path = $this->do_upload($input_name, $upload_path, $file_name);
					$path = "assets/uploads/memo_and_update/memo/".$path;

				    if($exist == true) {
				    	if($path != $file_memo) {
				    		$data += [
				    			"location_path" => $path
				    		];
				    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
				    	}
				    } else {
				    	$data += [
			    			"location_path" => $path
			    		];
			    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
				    }
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Invalid file type"
					];
				}
			} else {
	    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
			}
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	function insert_update()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$memo_and_update_id = $this->input->post("memo_and_update_id");
		$data = [
			"title" 			=> $this->input->post("title"),
			"file_group_name" 	=> 2,
			'status'			=> 1
		];

		if($memo_and_update_id == null) {
			if(!empty($_FILES["file_update"]["name"])) {
		    	$allowed_types = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];
				if(in_array($_FILES["file_update"]["type"], $allowed_types)) {
					$data  += [
						"created_by"	=> $this->session->login_id,
						"date_created"	=> $this->now()
					];

					$input_name = "file_update";
					$upload_path = "../pnpr6/assets/uploads/memo_and_update/update";
					$file_name = (!empty($_FILES[$input_name]["name"]) ? basename($_FILES[$input_name]["name"]) : "");

					if (pathinfo($file_name)["extension"] == "pdf") {
						$data  += [
							"file_type"	=> 1
						];
					} else {
						$data  += [
							"file_type"	=> 2
						];
					}

					$file_name = "fu_".date("YmdHis").".".pathinfo($file_name)["extension"];
					$path = $this->do_upload($input_name, $upload_path, $file_name);
					$path = "assets/uploads/memo_and_update/update/".$path;

					$data  += [
						"location_path"	=> $path
					];

					$this->model_memo_and_update->insert($data);

				    $this->user_log("Inserted file update '".$this->input->post("title")."'");

					$ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Success"
					];
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Invalid file type"
					];
				}
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Please add update file"
				];
			}
		} 
		else {
			$data += [
				"modified_by"	=> $this->session->login_id,
				"date_modified"	=> $this->now()
			];

			$this->user_log("Updated file update '".$this->input->post("title")."'");

			$ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Updated"
			];

			if(!empty($_FILES["file_update"]["name"])) {
		    	$allowed_types = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];
				if(in_array($_FILES["file_update"]["type"], $allowed_types)) {
			    	$exist = false;
			    	$file_update = "";
				    foreach ($this->model_memo_and_update->select("location_path", ["memo_and_update_id" => $memo_and_update_id]) as $key => $value) {
				    	$file_update = $value->location_path;
				    	$exist = true;
				    	break;
				    }

				    $input_name = "file_update";
					$upload_path = "../pnpr6/assets/uploads/memo_and_update/update";
					$file_name = (!empty($_FILES[$input_name]["name"]) ? basename($_FILES[$input_name]["name"]) : "");

					if (pathinfo($file_name)["extension"] == "pdf") {
						$data  += [
							"file_type"	=> 1
						];
					} else {
						$data  += [
							"file_type"	=> 2
						];
					}

					$file_name = "fu_".date("YmdHis").".".pathinfo($file_name)["extension"];
					$path = $this->do_upload($input_name, $upload_path, $file_name);
					$path = "/assets/uploads/memo_and_update/update/".$path;

				    if($exist == true) {
				    	if($path != $file_update) {
				    		$data += [
				    			"location_path" => $path
				    		];
				    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
				    	}
				    } else {
				    	$data += [
			    			"location_path" => $path
			    		];
			    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
				    }
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Invalid file type"
					];
				}
			} else {
	    		$this->model_memo_and_update->update($data, ["memo_and_update_id" => $memo_and_update_id]);
			}
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	function get_info_memo_update()
	{
		$data = [];
		foreach ($this->model_memo_and_update->select("*", ["memo_and_update_id" => $this->input->post("value")]) as $key => $value) {
			$data = [
				"memo_and_update_id" 	=> $value->memo_and_update_id,
				"title"					=> $value->title
			];
		}
		echo json_encode($data);
	}

	function delete_memo_and_update() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$this->user_log("Delete memo / update '".$this->get_memo_and_update_title($this->input->post("value"))."'");
		
		if ($this->model_memo_and_update->delete(["memo_and_update_id" => $this->input->post("value")])) {

		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];			
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}
	function get_memo_and_update_title($memo_and_update_id) {
		$ret = "";
		foreach ($this->model_memo_and_update->select("title", ["memo_and_update_id" => $memo_and_update_id]) as $key => $value) {
			$ret = $value->title;
			break;
		}
		return $ret;
	}

	function get_memo_and_update_preview()
	{
		$data = "";
		foreach ($this->model_memo_and_update->select("*", ["memo_and_update_id" => $this->input->post("value")]) as $key => $value) {

			$exploded = explode(".", $value->location_path);
			$file_type = $exploded[1];

			$file = '';
			if ($file_type == "pdf") {
				$file = '<object width="100%" height="500px" data="'.base_url().$value->location_path.'"></object>';
			} else {
				$file = '<img src="'.base_url().$value->location_path.'" class="img-thumbnail" style="width:100%;">';
			}

			$data = '<div class="ibox-content text-center">
                        <h1>'.$value->title.'</h1>
                        <div class="m-b-sm">
                                '.$file.'
                        </div>
                    </div>';
		}
		echo json_encode($data);
	}

}

/* End of file Add_memo_and_update.php */
/* Location: ./application/controllers/admin/Add_memo_and_update.php */