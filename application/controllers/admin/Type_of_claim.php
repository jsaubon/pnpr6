<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_of_claim extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_type_of_claim');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Type of claims",
			"content_title"	=> "<strong>Type of claims</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Type Of Claims"),
			"content_data"	=> [$this->load->view("interface/admin/Type_of_claim", [
								// "badge_numbers"	=> $this->get_badge_number()
							], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_type_of_claims() {
		$data = ["data" => []];
		foreach ($this->model_type_of_claim->select() as $key => $value) {
			$id = $value->type_of_claim_id;
			$url_delete = "\"delete_type_of_claim\"";
			$url_edit = "\"get_info_type_of_claim\"";
			$form_id = "\"form_type_of_claim\"";
			$tbl_id = "[tbl_type_of_claim]";
			$modal = "modal_type_of_claim";

			$data["data"][] = [
				$value->type_of_claim,
				"<div class='text-center'>
					<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
					<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>"
			];
		}
		echo json_encode($data);
	}

	function insert_type_of_claim()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$type_of_claim_id 	= $this->input->post("type_of_claim_id");
		$type_of_claim 	= $this->input->post("type_of_claim");
		$data = [
			"type_of_claim" => $this->input->post("type_of_claim")
		];

		if($type_of_claim_id == null) {
			if ($this->check_type_of_claim($type_of_claim) == 0) {
				$data += [
					"created_by"	=> $this->session->login_id,
					"date_created"	=> $this->now()
				];
				if ($this->model_type_of_claim->insert($data)) {

					$this->user_log("Insert type of claims '".$this->input->post("type_of_claim")."'");
				    $ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Success"
					];
				}				
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Ranked is already exist"
				];
			}
		} else {
			if ($this->get_type_of_claim($type_of_claim_id) == $type_of_claim) {
				$data += [
					"modified_by"	=> $this->session->login_id,
					"date_modified"	=> $this->now()
				];
				if ($this->model_type_of_claim->update($data, ["type_of_claim_id" => $type_of_claim_id])) {

					$this->user_log("Updated type of claims '".$this->input->post("type_of_claim")."'");

				    $ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Updated"
					];
				}				
			} else {
				if ($this->check_type_of_claim($type_of_claim) == 0) {
					$data += [
						"modified_by"	=> $this->session->login_id,
						"date_modified"	=> $this->now()
					];
					if ($this->model_type_of_claim->update($data, ["type_of_claim_id" => $type_of_claim_id])) {

						$this->user_log("Updated type of claims '".$this->input->post("type_of_claim")."'");
					    $ret = [
							"success" 	=> true,
							"msg"		=> "<span class='fa fa-check'></span> Success"
						];
					}				
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Ranked is already exist"
					];
				}
			}
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}
	function check_type_of_claim($type_of_claim)
	{
		$count = 0;
		foreach ($this->model_type_of_claim->select("COUNT(type_of_claim) AS count", ["type_of_claim" => $type_of_claim]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}

	function get_info_type_of_claim() {
		$data = [];
		foreach ($this->model_type_of_claim->select("*", ["type_of_claim_id" => $this->input->post("value")]) as $key => $value) {
			$data = [
				"type_of_claim_id" 	=> $value->type_of_claim_id,
				"type_of_claim"	=> $value->type_of_claim,
			];
		}
		echo json_encode($data);
	}

	function delete_type_of_claim() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$this->user_log("Delete type of claims '".$this->get_type_of_claim($this->input->post("value"))."'");
		
		if ($this->model_type_of_claim->delete(["type_of_claim_id" => $this->input->post("value")])) {

		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];			
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	// other
	function get_type_of_claim($type_of_claim_id)
	{
		$data = "";
		foreach ($this->model_type_of_claim->select("type_of_claim", ["type_of_claim_id" => $type_of_claim_id]) as $key => $value) {
			$data = $value->type_of_claim;
			break;
		}
		return $data;
	}

}

/* End of file Type_of_claim.php */
/* Location: ./application/controllers/admin/Type_of_claim.php */