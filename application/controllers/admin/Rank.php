<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rank extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model('model_rank');
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Ranked",
			"content_title"	=> "<strong>Ranked</strong> Page",
			"permission"	=> $this->check_user_permission("Admin Ranks"),
			"content_data"	=> [$this->load->view("interface/admin/Rank", [], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_ranks() {
		$data = ["data" => []];
		foreach ($this->model_rank->select() as $key => $value) {
			$id = $value->rank_id;
			$url_delete = "\"delete_rank\"";
			$url_edit = "\"get_info_rank\"";
			$form_id = "\"form_rank\"";
			$tbl_id = "[tbl_rank]";
			$modal = "modal_rank";

			$data["data"][] = [
				$value->rank_name,
				"<div class='text-center'>
					<button class='btn btn-success btn-circle' name='btn_edit' data-toggle='modal' href='#$modal' onclick='get_info($url_edit, $id, $form_id)' title='Edit'><span class='fa fa-edit'></span></button>
					<button class='btn btn-danger btn-circle' name='btn_delete' onclick='delete_this($url_delete, $id, $tbl_id)' title='Delete'><span class='fa fa-trash'></span></button>
				</div>"
			];
		}
		echo json_encode($data);
	}

	function insert_rank()
	{
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$rank_id 	= $this->input->post("rank_id");
		$rank_name 	= $this->input->post("rank_name");
		$data = [
			"rank_name" => $this->input->post("rank_name")
		];

		if($rank_id == null) {
			if ($this->check_rank($rank_name) == 0) {
				$data += [
					"created_by"	=> $this->session->login_id,
					"date_created"	=> $this->now()
				];
				if ($this->model_rank->insert($data)) {

					$this->user_log("Insert rank name '".$this->input->post("rank_name")."'");
				    $ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Success"
					];
				}				
			} else {
				$ret = [
					"success" 	=> false,
					"msg"		=> "<span class='fa fa-warning'></span> Ranked is already exist"
				];
			}
		} else {
			if ($this->get_rank($rank_id) == $rank_name) {
				$data += [
					"modified_by"	=> $this->session->login_id,
					"date_modified"	=> $this->now()
				];
				if ($this->model_rank->update($data, ["rank_id" => $rank_id])) {

					$this->user_log("Updated rank name '".$this->input->post("rank_name")."'");

				    $ret = [
						"success" 	=> true,
						"msg"		=> "<span class='fa fa-check'></span> Updated"
					];
				}				
			} else {
				if ($this->check_rank($rank_name) == 0) {
					$data += [
						"modified_by"	=> $this->session->login_id,
						"date_modified"	=> $this->now()
					];
					if ($this->model_rank->update($data, ["rank_id" => $rank_id])) {

						$this->user_log("Updated rank name '".$this->input->post("rank_name")."'");
					    $ret = [
							"success" 	=> true,
							"msg"		=> "<span class='fa fa-check'></span> Success"
						];
					}				
				} else {
					$ret = [
						"success" 	=> false,
						"msg"		=> "<span class='fa fa-warning'></span> Ranked is already exist"
					];
				}
			}
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}
	function check_rank($rank_name)
	{
		$count = 0;
		foreach ($this->model_rank->select("COUNT(rank_name) AS count", ["rank_name" => $rank_name]) as $key => $value) {
			$count = $value->count;
		}
		return $count;
	}

	function get_info_rank() {
		$data = [];
		foreach ($this->model_rank->select("*", ["rank_id" => $this->input->post("value")]) as $key => $value) {
			$data = [
				"rank_id" 	=> $value->rank_id,
				"rank_name"	=> $value->rank_name,
			];
		}
		echo json_encode($data);
	}

	function delete_rank() {
		$this->db->trans_begin();
		$ret = [
			"success" 	=> false,
			"msg"		=> "<span class='fa fa-warning'></span> Something went wrong"
		];

		$this->user_log("Delete rank name '".$this->get_rank($this->input->post("value"))."'");
		
		if ($this->model_rank->delete(["rank_id" => $this->input->post("value")])) {

		    $ret = [
				"success" 	=> true,
				"msg"		=> "<span class='fa fa-check'></span> Success"
			];			
		}

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
		}
		else {
		    $this->db->trans_commit();
		}
		echo json_encode($ret);
	}

	// other
	function get_rank($rank_id)
	{
		$data = "";
		foreach ($this->model_rank->select("rank_name", ["rank_id" => $rank_id]) as $key => $value) {
			$data = $value->rank_name;
			break;
		}
		return $data;
	}

}

/* End of file Rank.php */
/* Location: ./application/controllers/admin/Rank.php */