<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Core_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->redirect();
		$this->load->model("model_claim_cdp");
		$this->load->model("model_claim_cip");
		$this->load->model("model_claim_other");
		$this->load->model("model_memo_and_update");
	}

	public function index()
	{
		$page_data = $this->system();
		$page_data += [
			"page_title"	=> "Dashboard",
			"content_title"	=> "Dashboard",
			"permission"	=> $this->check_user_permission(""),
			"content_data"	=> [$this->load->view("interface/dashboard/Dashboard", [
			], TRUE)]
		];
		$this->create_page($page_data);
	}

	function get_memo_and_updates()
	{
		$data = [];

		foreach ($this->model_memo_and_update->select("*", ['status' => 1], [], ["memo_and_update_id" => "desc"]) as $key => $value) {
			$id = $value->memo_and_update_id;

			$exploded = explode(".", $value->location_path);
			$file_type = $exploded[1];

			$file_group_name = "";
			if ($value->file_group_name == 1) {
				$file_group_name = "Memo";
			} else {
				$file_group_name = "Updates";
			}

			$file = '';
			if ($file_type == "pdf") {
				$file = '<object width="120" height="120" data="'.base_url().$value->location_path.'"></object>';
			} else {
				$file = '<img src="'.base_url().$value->location_path.'" class="img-thumbnail" style="width:120px;">';
			}

			$data_ibox = '<div class="col-md-6">
							<div class="panel panel-info">
								<div class="panel-body">
		                            <div class="ibox-content text-center">
		                                <h1>'.$value->title.'</h1>
		                                <div class="m-b-sm">
		                                        '.$file.'
		                                </div>
		                                        <p class="font-bold">'.$file_group_name.'</p>

		                                <div class="text-center">
		                                    <a class="btn btn-xs btn-white" href="#modal_memo_and_update_preview" data-toggle="modal" onclick="get_memo_and_update_preview('.$id.')"><i class="fa fa-eye"></i> Large Preview </a>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
                        </div>';

			$data[] = [
				'data_ibox'	=> $data_ibox
			];
		}
		echo json_encode($data);
	}

	function get_memo_and_update_preview()
	{
		$data = "";
		foreach ($this->model_memo_and_update->select("*", ["memo_and_update_id" => $this->input->post("value")]) as $key => $value) {

			$exploded = explode(".", $value->location_path);
			$file_type = $exploded[1];

			$file = '';
			if ($file_type == "pdf") {
				$file = '<object width="100%" height="500px" data="'.base_url().$value->location_path.'"></object>';
			} else {
				$file = '<img src="'.base_url().$value->location_path.'" class="img-thumbnail" style="width:100%;">';
			}

			$data = '<div class="ibox-content text-center">
                        <h1>'.$value->title.'</h1>
                        <div class="m-b-sm">
                                '.$file.'
                        </div>
                    </div>';
		}
		echo json_encode($data);
	}

	function get_claim_counts()
	{
		$data = [];
		$count_cdp = 0;
		$count_cip = 0;
		$count_other = 0;

		$where = [ "user_id" => $this->session->login_id, "status" => 1 ];

		foreach ($this->model_claim_cdp->select("COUNT(user_id) AS count", $where) as $key => $value) {
			$count_cdp = $value->count;
		}
		foreach ($this->model_claim_cip->select("COUNT(user_id) AS count", $where) as $key => $value) {
			$count_cip = $value->count;
		}
		foreach ($this->model_claim_other->select("COUNT(user_id) AS count", $where) as $key => $value) {
			$count_other = $value->count;
		}

		$data = [
			'count_cdp' => $count_cdp,
			'count_cip' => $count_cip,
			'count_other' => $count_other,
		];
		echo json_encode($data);
	}
	
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */