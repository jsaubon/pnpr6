<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Controller extends CI_Controller {

	public function system() {
		$data = [
			"system_title" 	=> "PNP RCD PRO 13",
			"system_logo"	=> base_url("assets/custom/images/pnp_logo.png")
		];
		return $data;
	}

	public function create_page($data = []) {
		$data += [
			"user_name" 	=> $this->full_name($this->user_ids()["user_id"]), 
			"images" 		=> $this->user_ids()["location_path"]
		];
		return $this->load->view('interface/system/layout/Page', $data, false);
	}

	public function get_ip() {
		$ip = "";
		if(!empty($_SERVER["HTTP_CLIENT_IP"])) {
			$ip = $_SERVER["HTTP_CLIENT_IP"];
		} elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif(!empty($_SERVER["HTTP_X_FORWARDED"])) {
			$ip = $_SERVER["HTTP_X_FORWARDED"];
		} elseif(!empty($_SERVER["REMOTE_ADDR"])){
			$ip = $_SERVER["REMOTE_ADDR"];
		}
		if($ip == "::1") {
			$ip = "127.0.0.1";
		}
		return $ip;
	}

	public function full_name($user_id) {
		$fn = ""; $mn = ""; $ln = ""; $ext = "";
		foreach ($this->model_user->select("firstname, middlename, lastname, name_ext", ["user_id" => $user_id]) as $key => $value) {
			$fn = $value->firstname;
			$mn = $value->middlename;
			$ln = $value->lastname;
			$ext = $value->name_ext;
		}
		if(!empty($mn)) {
			$mn = $mn[0].". ";
		}
		return ucfirst($ln).", ".ucfirst($fn)." ".ucfirst($mn).ucfirst($ext);
	}

	public function redirect() {
		if(!$this->session->login_alias) {
			redirect('/');
		}  
	}

	public function redirect_home() {
		if(isset($this->session->login_alias) && $this->uri->segment(1) == "" || $this->uri->segment(1) == "main") {
			redirect('dashboard');
		}
	}

	public function user_ids() {
		$ids = [
					"user_id" 			=> $this->session->login_id, 
					"user_role_id" 		=> "",
                    "account_number"    => "",
					"location_path"		=> ""
				];
		foreach ($this->model_user->select("SUBSTRING_INDEX(SUBSTRING_INDEX(account_number,'æ',2),'æ',-1) AS SubStr, user_role_id", ["user_id" => $this->session->login_id]) as $key => $value) {
			$ids["user_role_id"] = $value->user_role_id;
            $ids["account_number"] = $value->SubStr;
			break;
		}
		foreach ($this->model_user_image->select("location_path", ["status" => 1, "user_id" => $ids["user_id"]]) as $key => $value) {
			$ids["location_path"] = $value->location_path;
		}
		return $ids;
	}

	public function now() {
		date_default_timezone_set("Asia/Manila");
		$now = date("Y-m-d H:i:s");
		return $now;
	}

	public function do_upload($input_name, $upload_path, $file_name) {
		$path = "";
		// $num = mt_rand(1, 1000000);

		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= 'pdf|docx|xls|ppt|jpg|png|jpeg|txt|mp4';
		$config['max_size']     	= '10000000';
		$config['overwrite'] 		= true;
		$config['file_name'] 		= $file_name;
		// $config['max_width'] 		= '5000';
		// $config['max_height'] 		= '5000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$upload = $this->upload->do_upload($input_name);
		if($upload) {
			$path = $file_name;
		}
		return $path;
    }

    public function user_log($action) {
    	$this->load->model('model_user_log');
    	$data = [
    		"date" 		=> $this->now(),
    		"action" 	=> $action,
    		"user_id"	=> $this->session->login_id
    	];
    	$this->model_user_log->insert($data);
    }

    public function check_user_permission($module_name) {
    	$this->load->model('model_permission');
    	$this->load->model('model_module_button');
    	$data = [0 => [], 1 => []];
    	foreach ($this->model_module_button->select("*", [
    			"modules.module_name" 	=> $module_name, 
    			"modules.system_id"		=> 1
    		], 
    		["modules" => "module_buttons.module_id = modules.module_id"]) as $key => $value) {

    		$status = 0;
    		foreach ($this->model_permission->select("status", [
    				"user_id" 		=> $this->session->login_id,
    				"mod_button_id"	=> $value->mod_button_id
    			]) as $key2 => $value2) {
    			$status = $value2->status;
    		}

    		$data[0][] = [
    			"button_code" 	=> $value->button_code,
    			"status"		=> $status
    		];
    	}
    	
    	foreach ($this->model_module_button->select("*", [
    				"module_buttons.button_code" => "view_page",
    				"modules.system_id"	=> 1
    			], 
    			["modules" 	=> "module_buttons.module_id = modules.module_id"]) as $key => $value) {

    		$status = 0;
    		foreach ($this->model_permission->select("status", [
    				"user_id" 		=> $this->session->login_id,
    				"mod_button_id"	=> $value->mod_button_id
    			]) as $key2 => $value2) {
    			$status = $value2->status;
    		}

    		$data[1][] = [
    			"module_name" 	=> str_replace([" ", "(", ")", "."], "", $value->module_name),
    			"status"		=> $status
    		];
    	}
    	return json_encode($data);
    }

 //    public function notify($module_name, $content, $to) {
	// 	$this->load->model('model_notification');
	// 	$this->load->model('model_module_button');
	// 	$this->load->model('model_permission');

	// 	if($to == "all") {
 //    		foreach ($this->model_module_button->select("mod_button_id", [
 //    				"module_buttons.button_code" 	=> "view_notification",
	//     			"modules.module_name" 			=> $module_name, 
	//     			"modules.system_id"				=> 1
	//     		], 
	//     		["modules" => "module_buttons.module_id = modules.module_id"]) as $key => $value) {

	//     		$status = 0;
	//     		$user_id = "";
	//     		foreach ($this->model_permission->select("status, user_id", ["mod_button_id" => $value->mod_button_id]) as $key2 => $value2) {
	//     			$status = $value2->status;
	//     			$user_id = $value2->user_id;
	//     		}

	//     		if($status == 1) {
	//     			$notif_data = [
	// 					"notification"	=> $content,
	// 					"user_id"		=> $this->session->login_id,
	// 					"is_read"		=> 0,
	// 					"date_created"	=> $this->now()
	// 				];
	// 				$this->model_notification->insert_notif($notif_data);
	//     		}
	//     	}
	// 	} else {
	// 		$notif_data = [
	// 			"notification"	=> $content,
	// 			"user_id"		=> $to,
	// 			"is_read"		=> 0,
	// 			"date_created"	=> $this->now()
	// 		];
	// 		$this->model_notification->insert_notif($notif_data);
	// 	}
	// }

	public function user_full_name($user_id) {
		$this->load->model('model_user');
		$user_id = "";
		foreach ($this->model_user->select("user_id", ["user_id" => $user_id]) as $key => $value) {
			$user_id = $value->user_id;
			break;
		}
		return $this->full_name($user_id);
	}

	// others
	public function fetchRawData($query) {
	    $query = $this->db->query($query);
	    return $query->result_array();
  	}

  	public function insertNotfication($user_id,$notif) { 
  		$date = date('Y-m-d H:i:s');
  		$this->db->query("INSERT INTO notifications (user_id,notification,`date`) VALUES ($user_id,'$notif','$date')");
  		// echo "inserted!";
  	}

  	public function insertNotficationTest() { 
  		$user_id = 1233;
  		$notif = 'testt message';
  		$date = date('Y-m-d H:i:s');
  		$this->db->query("INSERT INTO notifications (user_id,notification,`date`) VALUES ($user_id,'$notif','$date')");
  		echo "inserted!";
  	}
}

/* End of file Core_controller.php */
/* Location: ./application/core/Core_controller.php */