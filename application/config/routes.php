<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']									= 'Main';
$route['404_override'] 											= 'Main/page_not_found';
$route['translate_uri_dashes'] 									= FALSE;

$route['login'] 												= 'system/Login';
$route['request_login'] 										= 'system/Login/request_login';
$route['request_logout'] 										= 'system/Login/request_logout';
$route['signup'] 												= 'system/Signup';
$route['request_signup'] 										= 'system/Signup/request_signup';

$route['dashboard'] 											= 'Dashboard';

$route['get_memo_and_updates'] 									= 'Dashboard/get_memo_and_updates';
$route['get_memo_and_update_preview'] 							= 'Dashboard/get_memo_and_update_preview';

// links
$route['admin/claim/cdpcip']				 					= 'admin/claim/Cdp_cip';
$route['admin/claim/otherclaim']				 				= 'admin/claim/Other_claim';
$route['admin/memo_and_update']				 					= 'admin/Memo_and_update';
$route['admin/user']				 							= 'admin/User';
$route['admin/rank']				 							= 'admin/Rank';
$route['admin/type_of_claim']				 					= 'admin/Type_of_claim';

// CDP Claims
$route['get_cdp_claims']				 						= 'admin/claim/Cdp_cip/get_cdp_claims';
$route['get_info_cdp_claim']				 					= 'admin/claim/Cdp_cip/get_info_cdp_claim';
$route['get_claim_temp_cdp_claims']								= 'admin/claim/Cdp_cip/get_claim_temp_cdp_claims';
$route['delete_cdp_claim']					 					= 'admin/claim/Cdp_cip/delete_cdp_claim';
$route['insert_cdp_claim_single']				 				= 'admin/claim/Cdp_cip/insert_cdp_claim_single';
$route['insert_cdp_claim_credited']				 				= 'admin/claim/Cdp_cip/insert_cdp_claim_credited';
$route['insert_claim_temp_to_user_and_claim_cdp']				= 'admin/claim/Cdp_cip/insert_claim_temp_to_user_and_claim_cdp';
$route['cdp_change_status']										= 'admin/claim/Cdp_cip/cdp_change_status';

// CIP Claims
$route['get_cip_claims']				 						= 'admin/claim/Cdp_cip/get_cip_claims';
$route['get_info_cip_claim']				 					= 'admin/claim/Cdp_cip/get_info_cip_claim';
$route['get_claim_temp_cip_claims']								= 'admin/claim/Cdp_cip/get_claim_temp_cip_claims';
$route['delete_cip_claim']					 					= 'admin/claim/Cdp_cip/delete_cip_claim';
$route['insert_cip_claim_single']				 				= 'admin/claim/Cdp_cip/insert_cip_claim_single';
$route['insert_cip_claim_credited']				 				= 'admin/claim/Cdp_cip/insert_cip_claim_credited';
$route['insert_claim_temp_to_user_and_claim_cip']				 = 'admin/claim/Cdp_cip/insert_claim_temp_to_user_and_claim_cip';
$route['cip_change_status']										= 'admin/claim/Cdp_cip/cip_change_status';

// Other Claims
$route['get_other_claims']				 						= 'admin/claim/Other_claim/get_other_claims';
$route['get_info_other_claim']				 					= 'admin/claim/Other_claim/get_info_other_claim';
$route['get_claim_temp_other_claims']							= 'admin/claim/Other_claim/get_claim_temp_other_claims';
$route['delete_other_claim']					 				= 'admin/claim/Other_claim/delete_other_claim';
$route['insert_other_claim_single']				 				= 'admin/claim/Other_claim/insert_other_claim_single';
$route['insert_other_claim_request_for_fund']					= 'admin/claim/Other_claim/insert_other_claim_request_for_fund';
$route['insert_other_claim_credited']				 			= 'admin/claim/Other_claim/insert_other_claim_credited';
$route['insert_claim_temp_to_user_and_claim']				 	= 'admin/claim/Other_claim/insert_claim_temp_to_user_and_claim';
$route['change_status']											= 'admin/claim/Other_claim/change_status';

// user
$route['get_users']				 								= 'admin/User/get_users';
$route['get_info_user']				 							= 'admin/User/get_info_user';
$route['delete_user']				 							= 'admin/User/delete_user';
$route['insert_user']				 							= 'admin/User/insert_user';
$route['insert_multiple_user']				 					= 'admin/User/insert_multiple_user';

// Memo and updates
$route['get_memos']				 								= 'admin/Memo_and_update/get_memos';
$route['get_updates']				 							= 'admin/Memo_and_update/get_updates';
$route['delete_memo_and_update']				 				= 'admin/Memo_and_update/delete_memo_and_update';
$route['get_info_memo_update']				 					= 'admin/Memo_and_update/get_info_memo_update';
$route['insert_memo']				 							= 'admin/Memo_and_update/insert_memo';
$route['insert_update']				 							= 'admin/Memo_and_update/insert_update';
$route['get_memo_and_update_preview']				 			= 'admin/Memo_and_update/get_memo_and_update_preview';

// Rank
$route['get_ranks']				 								= 'admin/Rank/get_ranks';
$route['get_info_rank']				 							= 'admin/Rank/get_info_rank';
$route['delete_rank']				 							= 'admin/Rank/delete_rank';
$route['insert_rank']				 							= 'admin/Rank/insert_rank';

// Type of Claims
$route['get_type_of_claims']				 					= 'admin/Type_of_claim/get_type_of_claims';
$route['get_info_type_of_claim']				 				= 'admin/Type_of_claim/get_info_type_of_claim';
$route['delete_type_of_claim']				 					= 'admin/Type_of_claim/delete_type_of_claim';
$route['insert_type_of_claim']				 					= 'admin/Type_of_claim/insert_type_of_claim';

// User Claims
$route['get_user_other_claims']				 					= 'User_claim/get_user_other_claims';
$route['get_user_cdp_claims']				 					= 'User_claim/get_user_cdp_claims';
$route['get_user_cip_claims']				 					= 'User_claim/get_user_cip_claims';